/**
 *  @file
 *
 *
 *
 *
 *
 *
 *
 */

#ifndef LCFSIMDETECTOR_H
#define LCFSIMDETECTOR_H

#include "TObject.h"

class LcFsimDetector : public TObject
{
  public:
     LcFsimDetector();
      ~LcFsimDetector();

      //! enumerator for detector ID.
      enum E_detector
      { k_CSI };

      bool IsHitCsi( LcFsimGenParticle &gen_particle );

  private:

};

#endif /* LcFsimDetector.h */
