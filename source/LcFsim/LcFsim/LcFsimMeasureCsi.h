/**
 *  @file         LcFsimMeasureCsi.h
 *  @brief        An object representing a hit measured at CSI.
 *    
 *  @author       C. Lin [ chiehlin@uchicago.edu ]
 *  @version
 *  @date
 *  @copyright
 */

#ifndef LCFSIMMEASURECSI_H
#define LCFSIMMEASURECSI_H

#include "TObject.h"
#include "Math/Vector2D.h"

class LcFsimMeasureCsi : public TObject
{
  public:
     LcFsimMeasureCsi();
     ~LcFsimMeasureCsi();

     inline Double_t GetMeasureX() const { return m_xyvec.X(); }
     inline Double_t GetMeasureY() const { return m_xyvec.Y(); }
     inline Double_t GetDepE()     const { return m_depE;      }

     void SetMeasureXY( ROOT::Math::XYVector const& xyvec ){ m_xyvec = xyvec; }
     void SetDepE( const Double_t depE ){ m_depE = depE; }

  private: 
     ROOT::Math::XYVector m_xyvec;
     Double_t             m_depE;
//     Double_t             m_time;

}; 

#endif /* LcFsimMeasureCsi.h */
