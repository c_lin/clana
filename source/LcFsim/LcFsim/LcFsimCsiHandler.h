/**
 *   @file         LcFsimCsiHandler.h
 *   @brief        Perform the CSI fast simulation. 
 *
 *   @author       C. Lin [ chiehlin@uchicago.edu ]
 *   @version 
 *   @date
 *   @copyright
 *
 */


#ifndef LCFSIMCSIHANDLER_H
#define LCFSIMCSIHANDLER_H

#include "TObject.h"
#include "TRandom3.h"

//#include "TF1.h"

#include "LcFsim/LcFsimMeasureCsi.h"

class LcFsimCsiHandler : public TObject
{
  public:
     LcFsimCsiHandler( const UInt_t seed = 0);
     virtual ~LcFsimCsiHandler();

     void     SetSeed( const UInt_t seed );

     UInt_t   GetSeed( void ) const { return m_seed; }

     Double_t GetTimingResolution( const Double_t energy ) const;
     Double_t GetEnergyResolution( const Double_t energy ) const;
     Double_t GetAngleResolution( const Double_t energy ) const;
     Double_t GetTransvPosResolution( const Double_t energy ) const;

     Double_t GetSmearedEnergy( const Double_t energy ) const;
     Double_t GetSmearedAngle( const Double_t angle, const Double_t energy ) const;
     Double_t GetTransvDisplacement( const Double_t energy ) const;

  private:
     //void SetSmearFunction();

  private:
     UInt_t m_seed; 

     TRandom3 *m_random;


/*
     //! Timing smearing function.
     TF1 *m_timeSmearFunc;

     //! Energy smearing function.
     TF1 *m_energySmearFunc;

     //! Angle smearing function.
     TF1 *m_angleSmearFunc;

     //! Transverse position smearing function.
     TF1 *m_transvPosSmearFunc;
*/
};

#endif /* LcFsimCsiHandler.h */
