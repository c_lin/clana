/**
 *  @file
 *
 *
 *
 *
 *
 *
 *
 */

#ifndef LCFSIMGENPARTICLE_H
#define LCFSIMGENPARTICLE_H

#include "TObject.h"
#include "Math/Vector2D.h"
#include "Math/Vector3D.h"
//#include "Math/Vector4D.h"

class LcFsimGenParticle : public TObject
{
  public:
     LcFsimGenParticle();
      ~LcFsimGenParticle();

     //! @brief Set initial position of a generated particle.
     inline void SetInitPos( const Double_t x, const Double_t y, const Double_t z)
                 { m_initPos.SetXYZ(x, y, z); }

     //! @brief Set initial momentum of a generated particle.
     inline void SetInitMom( const Double_t px, const Double_t py, const Double_t pz )
                 { m_initMom.SetXYZ(px, py, pz); }

     bool ProjectZ( const Double_t z, ROOT::Math::XYVector &vec_xy ) const; 
 
     inline Double_t GetInitPosX( void ) const { return m_initPos.X(); }
     inline Double_t GetInitPosY( void ) const { return m_initPos.Y(); }
     inline Double_t GetInitPosZ( void ) const { return m_initPos.Z(); }

     inline Double_t GetHitPosX( void ) const { return m_hitPos.X(); }
     inline Double_t GetHitPosY( void ) const { return m_hitPos.Y(); }
     inline Double_t GetHitPosZ( void ) const { return m_hitPos.Z(); }

     inline Double_t GetInitMomX( void ) const { return m_initMom.X(); }
     inline Double_t GetInitMomY( void ) const { return m_initMom.Y(); }
     inline Double_t GetInitMomZ( void ) const { return m_initMom.Z(); }

  private:
     //! The cartesian coordinate (x, y, z) of where a particle is created.
     ROOT::Math::XYZVector  m_initPos;

     //! The initial momentum (px, py, py) of a particle.
     ROOT::Math::XYZVector  m_initMom;

     //! The generation timing of a particle.
     Double_t m_time;

     //! The cartesian coordinate (x, y, z) of where a particle hits.
     ROOT::Math::XYZVector  m_hitPos;

};

#endif /* LcFsimGenParticle.h */
