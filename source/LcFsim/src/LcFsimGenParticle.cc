#include "LcFsim/LcFsimGenParticle.h"

//
//! @brief   Basic constructor.
//
LcFsimGenParticle::LcFsimGenParticle()
{
   ;
}

//
//! @brief    Basic destructor.
//
LcFsimGenParticle::~LcFsimGenParticle()
{
   ;
}

//
//! @brief Project particle 
//!
//! @return 
//!
//
bool LcFsimGenParticle::ProjectZ( const Double_t proj_z, ROOT::Math::XYVector &vec_xy ) const
{
   Double_t disp_z = proj_z - GetInitPosZ(); 

   if( (GetInitMomZ() < 1.e-3 && disp_z > 0.) || (GetInitMomZ() > -1.e-3 && disp_z < 0.) ) 
      return false;

   Double_t ratio = disp_z / GetInitMomZ();

   vec_xy.SetX( GetInitPosX() + GetInitMomX() * ratio );
   vec_xy.SetY( GetInitPosY() + GetInitMomY() * ratio );
   
   return true;
}
