#include "LcFsim/LcFsimCsiHandler.h"

#include "TMath.h"

//
//! @brief   Basic constructor.
//
LcFsimCsiHandler::LcFsimCsiHandler( const UInt_t seed )
   : m_seed ( seed )
{
   m_random = new TRandom3(m_seed);
}

//
//! @brief   Basic destructor.
//
LcFsimCsiHandler::~LcFsimCsiHandler()
{
   delete m_random;
}

//
//! @brief     Set random seed.
//! @param[in] Seed ID.
//
void LcFsimCsiHandler::SetSeed( const UInt_t seed )
{
   m_seed = seed;
   if( m_random ) m_random->SetSeed( seed );
}

//
//! @brief      Get the timing resolution of a photon hit in CSI..
//! @param[in]  True photon energy in MeV.
//!
//! sigma_t = p0 \csum p1/sqrt(energy [GeV]) (ns)
//! p0 = 0.19 (ns)
//! p1 = 3.8  (ns)
//
Double_t LcFsimCsiHandler::GetTimingResolution( const Double_t energy ) const
{
   Double_t x = ( energy < 100 ) ? 0.1 : energy / 1000.;
   return TMath::Sqrt( TMath::Power(0.19,2.) + TMath::Power(3.8/TMath::Sqrt(x),2.) );
}
     
//
//! @brief      Get the energy fluctuation ratio to the input energy.
//! @param[in]  True photon energy in MeV.
//! 
//! sigma_E / E = p0 \csum p1/sqrt(energy [GeV])
//! p0 = 0.58\%
//! p1 = 1.67\%
//
Double_t LcFsimCsiHandler::GetEnergyResolution( const Double_t energy ) const
{
   Double_t x = ( energy < 100 ) ? 0.1 : energy / 1000.;
   return TMath::Sqrt( TMath::Power(5.8e-3,2.) + TMath::Power(1.67e-2/TMath::Sqrt(x),2.) );
}
     
//
//! @brief     Get the incident angle resolution.
//! @param[in] True photon energy in MeV.
//!
//! sigma_theta = p0 + p1/sqrt(E [GeV]) + p2/(E [GeV]) (deg)
//! p0 = 0.145 (deg)
//! p1 = -0.481 (deg)
//! p2 = 0.598 (deg)
//! 
//! This formua has the minimum at 6.2 GeV. 
//
Double_t LcFsimCsiHandler::GetAngleResolution( const Double_t energy ) const
{
   Double_t x = energy / 1000.;
   if     ( x < 0.1 ) x = 0.1;
   else if( x > 6.2 ) x = 6.2;
 
   return 0.145 - 0.481/TMath::Sqrt(x) + 0.598/x;
}

//
//! @brief     Get the transverse position resolution.
//! @param[in] True photon energy in MeV.
//!
//! sigma_y = p0 \csum p1/sqrt(x) (mm)
//! p0 = 1.765 (mm)
//! p1 = 3.696 (mm)
//! Drop p2 term because it only has value of p2 = 0.009 (mm) 
//
Double_t LcFsimCsiHandler::GetTransvPosResolution( const Double_t energy ) const
{
   Double_t x = ( energy < 100 ) ? 0.1 : energy / 1000.; 
   return TMath::Sqrt( TMath::Power(1.765,2.) + TMath::Power(3.696/TMath::Sqrt(x),2.) );;
}

//
//! @brief 
//! @param[in] 
//! 
Double_t LcFsimCsiHandler::GetSmearedEnergy( const Double_t energy ) const
{
   Double_t sigma_energy = GetEnergyResolution(energy);
   return m_random->Gaus(energy, energy*sigma_energy);
}

//
//! @brief
//! @param[in]
//
Double_t LcFsimCsiHandler::GetSmearedAngle( const Double_t angle, const Double_t energy ) const
{
   Double_t sigma_angle = GetAngleResolution(energy);
   Double_t smeared_angle = m_random->Gaus(angle, sigma_angle);
   if( smeared_angle < 1.e-3 ) smeared_angle = 0.;
   else if( smeared_angle > 90.) smeared_angle = 90.;

   return smeared_angle;
}

//
//! @brief
//! @param[in]
//
Double_t LcFsimCsiHandler::GetTransvDisplacement( const Double_t energy ) const
{
   Double_t sigma_y = GetTransvPosResolution(energy);
   return m_random->Gaus(0.,sigma_y);
}
