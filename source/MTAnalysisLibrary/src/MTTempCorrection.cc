#include <cmath>
#include <unistd.h>
#include "TString.h"
#include "MTAnalysisLibrary/MTTempCorrection.h"



MTTempCorrection::MTTempCorrection( int runID ) : baseRunID(18448), m_RunID( runID )
{
  pHandler = MTPositionHandler::GetInstance();

  Temperature = new double [100] ();
  BaseTemperature = new double [100] ();

  std::string dirName
    = std::string( std::getenv(MTBP::EnvName.c_str()) )+"/AnalysisLibrary/LegacyProjects/MTAnalysisLibrary";

  basetr = new TChain( "TempRunTree" );
  basetr->Add( Form( "%s/data/TempMon/tempmon%08d.root", dirName.c_str(),baseRunID ) );
  basetr->SetBranchAddress( "Temperature", BaseTemperature );
  basetr->GetEntry(0);
  delete basetr;

  tr = new TChain( "TempRunTree" );
  tr->Add( Form( "%s/data/TempMon/tempmon%08d.root", dirName.c_str(),runID ) );
  SetBranchAddress();
  tr->GetEntry(0);
  multipleFlag = false;
  
  runEntryMap.insert( std::map< unsigned int, unsigned int>::value_type( runID, 0 ) );

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
}

MTTempCorrection::MTTempCorrection( int startRunID, int endRunID ) : baseRunID(18448), m_RunID( startRunID )
{
  pHandler = MTPositionHandler::GetInstance();

  Temperature = new double [100] ();
  BaseTemperature = new double [100] ();

  std::string dirName
    = std::string( std::getenv(MTBP::EnvName.c_str()) )+"/AnalysisLibrary/LegacyProjects/MTAnalysisLibrary";

  basetr = new TChain( "TempRunTree" );
  basetr->Add( Form( "%s/data/TempMon/tempmon%08d.root", dirName.c_str(),baseRunID ) );
  basetr->SetBranchAddress( "Temperature", BaseTemperature );
  basetr->GetEntry(0);
  delete basetr;

  tr = new TChain( "TempRunTree" );
  for( int runID=startRunID; runID<endRunID; runID++ ){
    tr->Add( Form( "%s/data/TempMon/tempmon%08d.root", dirName.c_str(),runID ) );
  }
  SetBranchAddress();
  for( int entry=0; entry<tr->GetEntries(); entry++ ){
    tr->GetEntry( entry );
    runEntryMap.insert( std::map< unsigned int, unsigned int>::value_type( RunID, entry ) );
  }
  
  multipleFlag = true;

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

}


MTTempCorrection::~MTTempCorrection()
{
  delete tr;
  delete Temperature;
  delete BaseTemperature;

  delete m_E14BP;
}


void MTTempCorrection::SetBranchAddress( void )
{
  tr->SetBranchAddress( "RunID", &RunID );
  tr->SetBranchAddress( "TrigDataStartTime", &TrigDataStartTime );
  tr->SetBranchAddress( "TempMonStartTime", &TempMonStartTime );
  tr->SetBranchAddress( "Temperature", Temperature );
}



bool MTTempCorrection::IsExist( int run ) const
{
  if( runEntryMap.count( run)==1 ){
    return true;
  }else{
    return false;
  }
}  


bool MTTempCorrection::SetRunID( int run )
{
  if( IsExist( run ) ){
    tr->GetEntry( runEntryMap[run] );
    return true;
  }
  return false;
}


unsigned int MTTempCorrection::GetTempUpChannel( int csiId ) const
{
  double x=0, y=0, z=0;
  static const int s_userFlag = GetUserFlag( m_RunID );
  pHandler->GetCSIXYZPosition( csiId, x, y, z, s_userFlag);
  return GetTempUpChannel( x, y );
}


unsigned int MTTempCorrection::GetTempUpChannel( double x, double y ) const
{
  if( x<-750 && fabs(y)<300 ) return MTBP::CsI_Upstream_1;
  if( fabs(x+600)<150 && fabs(y)<300 ) return MTBP::CsI_Upstream_3;
  if( fabs(x+300) < 150 ){
    if( fabs(y-350)<150 ) return MTBP::CsI_Upstream_5;
    if( fabs(y)    <200 ) return MTBP::CsI_Upstream_6;
    if( fabs(y+350)<150 ) return MTBP::CsI_Upstream_7;
  }
  if( x < -300 ){
    if( 300 < y )  return MTBP::CsI_Upstream_2;
    if( y < -300 ) return MTBP::CsI_Upstream_4;
  }
  if( fabs(x) < 300 ){
    if( 500 < y )  return MTBP::CsI_Upstream_8;
    if( y < -500 ) return MTBP::CsI_Upstream_13;
  }
  if( fabs(x) < 150 ){
    if( 0 < y ) return MTBP::CsI_Upstream_9;
    if( y < 0 ) return MTBP::CsI_Upstream_12;
  }
  if( fabs(x-300) < 150 ){
    if( fabs(y-350)<150 ) return MTBP::CsI_Upstream_14;
    if( fabs(y)    <200 ) return MTBP::CsI_Upstream_15;
    if( fabs(y+350)<150 ) return MTBP::CsI_Upstream_16;
  }
  if( 300 < x ){
    if( 300 < y )  return MTBP::CsI_Upstream_17;
    if( y < -300 ) return MTBP::CsI_Upstream_19;
  }
  if( fabs(x-600)<150 && fabs(y)<300 ) return MTBP::CsI_Upstream_18;
  if( 750<x && fabs(y)<300 ) return MTBP::CsI_Upstream_20;
  
  return 9999;
}



unsigned int MTTempCorrection::GetTempDownChannel( int csiId ) const
{
  double x=0, y=0, z=0;
  static const int s_userFlag = GetUserFlag( m_RunID );
  pHandler->GetCSIXYZPosition( csiId, x, y, z, s_userFlag);
  return GetTempDownChannel( x, y);
}



unsigned int MTTempCorrection::GetTempDownChannel( double x, double y ) const
{  
  if( 750<x && fabs(y)<300 ) return MTBP::CsI_Downstream_1;
  if( fabs(x-600)<150 && fabs(y)<300 ) return MTBP::CsI_Downstream_3;
  if( 300 < x ){
    if( 300 < y )  return MTBP::CsI_Downstream_2;
    if( y < -300 ) return MTBP::CsI_Downstream_4;
  }
  if( fabs(x-300) < 150 ){
    if( fabs(y-250) < 250 ) return MTBP::CsI_Downstream_5;
    if( fabs(y+250) < 250 ) return MTBP::CsI_Downstream_6;
  }
  if( fabs(x) < 150 ){
    if( fabs(y-150) < 150 ) return MTBP::CsI_Downstream_8;
    if( fabs(y+150) < 150 ) return MTBP::CsI_Downstream_11;
  }
  if( fabs(x+300) < 150 ){
    if( fabs(y-250) < 250 ) return MTBP::CsI_Downstream_9;
    if( fabs(y+250) < 250 ) return MTBP::CsI_Downstream_13;
  }
  if( fabs(x) < 300 ){
    if( 300 < y )  return MTBP::CsI_Downstream_7;
    if( y < -300 ) return MTBP::CsI_Downstream_12;
  }
  if( x < -300 ){
    if( 300 < y )       return MTBP::CsI_Downstream_10;
    if( fabs(y) < 300 ) return MTBP::CsI_Downstream_14;
    if( y < -300 )      return MTBP::CsI_Downstream_15;
  }

  return 9999;
}


double MTTempCorrection::GetTemperature( int tempChannel ) const
{
  if( tempChannel<100 && -1<tempChannel ){
    return Temperature[tempChannel];
  }else{
    return -1;
  }
}

double MTTempCorrection::GetBaseTemperature( int tempChannel ) const
{
  if( tempChannel<100 && -1<tempChannel ){
    return BaseTemperature[tempChannel];
  }else{
    return -1;
  }
}

double MTTempCorrection::GetTemperature( int run, int tempChannel )
{
  
  if( SetRunID(run) && tempChannel<100 && -1<tempChannel ){
    return Temperature[tempChannel];
  }else{
    return -1;
  }
}


double MTTempCorrection::GetCorrectionFactor( int tempChannel ) const
{

  // for run63
  switch(tempChannel ){
  case MTBP::CsI_Upstream_12:
    tempChannel=MTBP::CsI_Upstream_9;
    break;
  case MTBP::CsI_Upstream_13:
    tempChannel=MTBP::CsI_Upstream_8;
    break;
  case MTBP::CsI_Upstream_14:
    tempChannel=MTBP::CsI_Upstream_5;
    break;
  case MTBP::CsI_Upstream_15:
    tempChannel=MTBP::CsI_Upstream_5;
    break;
  case MTBP::CsI_Upstream_16:
    tempChannel=MTBP::CsI_Upstream_7;
    break;
  case MTBP::CsI_Upstream_17:
    tempChannel=MTBP::CsI_Upstream_2;
    break;
  case MTBP::CsI_Upstream_18:
    tempChannel=MTBP::CsI_Upstream_3;
    break;
  case MTBP::CsI_Upstream_19:
    tempChannel=MTBP::CsI_Upstream_4;
    break;
  case MTBP::CsI_Upstream_20:
    tempChannel=MTBP::CsI_Upstream_1;
    break;    
  }
    
  if( tempChannel<100 && -1<tempChannel ){
    //std::cout<<"temp correction:"<<tempChannel<<" "<<Temperature[tempChannel]<<" "<<1. / exp( MTBP::CSITempSlope * (Temperature[tempChannel]-BaseTemperature[tempChannel]))<<std::endl;
    //std::getchar();

    return  1. / exp( MTBP::CSITempSlope * (Temperature[tempChannel]-BaseTemperature[tempChannel]) );
  }else{
    return 0;
  }
}


double MTTempCorrection::GetCorrectionFactor( int run, int tempChannel )
{
  double temp = GetTemperature( run, tempChannel );
  if( temp != -1 ){
    return 1. / exp( MTBP::CSITempSlope * (temp-BaseTemperature[tempChannel]) );
  }else{
    return 0;
  }
}


void MTTempCorrection::Correction( double* energy )
{
  for( int ich=0; ich<MTBP::CSINChannels; ich++ ){
    energy[ich] *= GetCorrectionFactor( GetTempUpChannel( ich) );
  }
}


double MTTempCorrection::Correction( int csiID, double energy )
{
  return energy * GetCorrectionFactor( GetTempUpChannel( csiID) );
}

int MTTempCorrection::GetUserFlag( int RunID ) const
{
  m_E14BP->SetUserFlag( m_RunID );
  const int userFlag = m_E14BP->GetUserFlag();

  return userFlag;
}
