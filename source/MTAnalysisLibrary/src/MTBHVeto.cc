#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTAnalysisLibrary/MTBHVeto.h"



MTBHVeto::MTBHVeto( void )
{
  SetVetoWindow( 1000., 250. );
  m_vetoThreshold = 1.;
}

void MTBHVeto::MakeWfmArray(){
  DetSuppWfm500       = new Float_t[DetNumber][MTBP::nSample500];
  DetAreaR500         = new Float_t[DetNumber][MTBP::NMaxHitPerEvent];
  DetSuppWfmModID = new Int_t[DetNumber];
  DetSuppWfmNumber = 0;
}


MTBHVeto::~MTBHVeto(void){	  
	delete[] DetSuppWfm500;
	delete[] DetAreaR500;
}



void MTBHVeto::Initialize( void )
{
  m_eventStartZ = 0;
  m_eventStartTime = 0;
}

void MTBHVeto::SetBranchAddress( TTree* tr )
{    

  if(m_clusterTree->GetBranch(Form("%sAreaR",DetName.c_str()))!=0 ){
     m_clusterTree->SetBranchAddress(Form( "%sAreaR", DetName.c_str() ), DetAreaR500);
  }
	
  if(m_clusterTree->GetBranch(Form("%sWfm",DetName.c_str()))!=0 ){
     m_clusterTree->SetBranchAddress(Form( "%sWfmNumber", DetName.c_str() ), &DetSuppWfmNumber);
     m_clusterTree->SetBranchAddress(Form( "%sWfmModID",  DetName.c_str() ),  DetSuppWfmModID);
     m_clusterTree->SetBranchAddress(Form( "%sWfm",  DetName.c_str() ),  DetSuppWfm500);
  }

} 
void MTBHVeto::Branch( TTree* tr ){

  if(m_clusterTree->GetBranch(Form("%sAreaR",DetName.c_str()))!=0 ){
	m_recTree->Branch( Form( "%sAreaR", DetName.c_str() ), DetAreaR500, Form( "%sAreaR[%sNumber][%d]", DetName.c_str(), DetName.c_str(), MTBP::NMaxHitPerEvent  ));
  }
  if(m_clusterTree->GetBranch( Form( "%sWfm",   DetName.c_str() ))){
	m_recTree->Branch( Form( "%sWfmNumber", DetName.c_str() ), &DetSuppWfmNumber, Form( "%sWfmNumber/I",  DetName.c_str() ));
	m_recTree->Branch( Form( "%sWfmModID", DetName.c_str() ),   DetSuppWfmModID,  Form( "%sWfmModID[%sWfmNumber]/I",  DetName.c_str(), DetName.c_str() ));
	m_recTree->Branch( Form( "%sWfm",   DetName.c_str() ),      DetSuppWfm500,    Form( "%sWfm[%sWfmNumber][%d]", DetName.c_str(), DetName.c_str(), MTBP::nSample500 ));
  }
}




void MTBHVeto::SetEventStartInfo( double eventStartZ, double eventStartTime )
{
  SetEventStartZ( eventStartZ );
  SetEventStartTime( eventStartTime );
}

void MTBHVeto::SetEventStartZ( double z )
{
  m_eventStartZ = z;
}

double MTBHVeto::GetEventStartZ( void )
{
  return m_eventStartZ;
}

void MTBHVeto::SetEventStartTime( double t )
{
  m_eventStartTime = t;
}

double MTBHVeto::GetEventStartTime( void )
{
  return m_eventStartTime;
}

void MTBHVeto::SetVetoWindow( double windowLength, double windowOffset )
{
  m_vetoWindowLength = windowLength;
  m_vetoWindowOffset = windowOffset;
  m_vetoStartTime = windowOffset - windowLength/2.;
  m_vetoEndTime = windowOffset + windowLength/2.;
}

void MTBHVeto::GetVetoWindow( double& windowLength, double& windowOffset )
{
  windowLength = m_vetoWindowLength;
  windowOffset = m_vetoWindowOffset;
}

double MTBHVeto::GetVetoStartTime( void )
{
  return m_vetoStartTime;
}

double MTBHVeto::GetVetoEndTime( void )
{
  return m_vetoEndTime;
}


void MTBHVeto::SetVetoThreshold( double eThreshold )
{
  m_vetoThreshold = eThreshold;
}

void MTBHVeto::SetLooseVetoThreshold( double eThreshold )
{
  m_looseVetoThreshold = eThreshold;
}

void MTBHVeto::SetTightVetoThreshold( double eThreshold )
{
  m_tightVetoThreshold = eThreshold;
}

double MTBHVeto::GetVetoThreshold( void )
{
  return m_vetoThreshold;
}

double MTBHVeto::GetLooseVetoThreshold( void )
{
  return m_looseVetoThreshold;
}

double MTBHVeto::GetTightVetoThreshold( void )
{
  return m_tightVetoThreshold;
}


bool MTBHVeto::IsInVetoWindow( double t )
{
  if( t < m_vetoStartTime ) return false;
  if( m_vetoEndTime < t ) return false;
  return true;
}

void MTBHVeto::SetTimeOffset(double offset)
{
  for(int i=0; i<DetNumber; i++){
    for(int j=0; j<DetNHit[i]; j++){
      DetTime[i][j]=DetTime[i][j]+offset;
    }
  }
}

