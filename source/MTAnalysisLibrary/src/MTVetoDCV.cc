#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoDCV.h"


MTVetoDCV::MTVetoDCV( int userflag ): m_userFlag( userflag ) {
  DetName = "DCV"; 
  DetNumber = MTBP::DCVNModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();

  CopyDstBranchFlag = false;

  DCVScintiModID = new int[8];
  DCVScintiEne = new float[8];
  DCVScintiTime = new float[8];
  DCVScintiDeltaTime = new float[8];
  vetoHitBuffer = new pVetoHit [8]();

  DetWfmCorrupted = NULL;
  DCVSideModID = new int[16];
  DCVSideEne = new float[16];
  DCVSideTime = new float[16];
  DCVSideDeltaTime = new float[16];
  vetoHitBuffer2 = new pVetoHit [16]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  MakeWfmArray();
  Initialize();
  DetNumber = 0;
}


MTVetoDCV::~MTVetoDCV()
{
  delete m_E14BP;

  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetSmoothness;
  delete[] DetPSBit;
  delete[] vetoHitBuffer;
  delete[] vetoHitBuffer2;

  delete [] DCVScintiModID;
  delete [] DCVScintiEne;
  delete [] DCVScintiTime;
  delete [] DCVScintiDeltaTime;

  delete [] DCVSideModID;
  delete [] DCVSideEne;
  delete [] DCVSideTime;
  delete [] DCVSideDeltaTime;

}


void MTVetoDCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "DCVModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "DCVModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "DCVModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "DCVModuleEne", DetEne );
  if(m_clusterTree->GetBranch("DCVModulePSBit"))
	  m_clusterTree->SetBranchAddress( "DCVModulePSBit", DetPSBit );

  if(m_clusterTree->GetBranch("DCVModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "DCVModuleSmoothness", DetSmoothness );  
  }
  MTVeto::SetBranchAddress(tr);
}


void MTVetoDCV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "DCVModuleNumber", &DetNumber, "DCVModuleNumber/I");
  m_recTree->Branch( "DCVModuleModID", DetModID, "DCVModuleModID[DCVModuleNumber]/I");
  m_recTree->Branch( "DCVModuleEne", DetEne, "DCVModuleEne[DCVModuleNumber]/F");
  m_recTree->Branch( "DCVModuleTime", DetTime, "DCVModuleTime[DCVModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "DCVModuleSmoothness", DetSmoothness, "DCVModuleSmoothness[DCVModuleNumber]/F" );
  if(m_clusterTree->GetBranch("DCVModulePSBit"))
	  m_recTree->Branch( "DCVModulePSBit", DetPSBit, "DCVModulePSBit[DCVModuleNumber]/S");

  m_recTree->Branch( "DCVVetoModID", &DCVVetoModID,"DCVVetoModID/I");
  m_recTree->Branch( "DCVVetoEne", &DCVVetoEne,"DCVVetoEne/F");
  m_recTree->Branch( "DCVVetoTime", &DCVVetoTime,"DCVVetoTime/F");
  m_recTree->Branch( "DCVVetoDeltaTime", &DCVVetoDeltaTime, "DCVVetoDeltaTime/F");
  
  m_recTree->Branch( "DCVSideVetoModID", &DCVSideVetoModID,"DCVSideVetoModID/I");
  m_recTree->Branch( "DCVSideVetoEne", &DCVSideVetoEne,"DCVSideVetoEne/F");
  m_recTree->Branch( "DCVSideVetoTime", &DCVSideVetoTime,"DCVSideVetoTime/F");
  m_recTree->Branch( "DCVSideVetoDeltaTime", &DCVSideVetoDeltaTime, "DCVSideVetoDeltaTime/F");

  m_recTree->Branch( "DCVScintiNumber", &DCVScintiNumber, "DCVScintiNumber/I");
  m_recTree->Branch( "DCVScintiModID",  DCVScintiModID, "DCVScintiModID[DCVScintiNumber]/I");
  m_recTree->Branch( "DCVScintiEne", DCVScintiEne, "DCVScintiEne[DCVScintiNumber]/F");
  m_recTree->Branch( "DCVScintiTime", DCVScintiTime, "DCVScintiTime[DCVScintiNumber]/F");
  m_recTree->Branch( "DCVScintiDeltaTime", DCVScintiDeltaTime, "DCVScintiDeltaTime[DCVScintiNumber]/F");
  
  m_recTree->Branch( "DCVSideNumber", &DCVSideNumber, "DCVSideNumber/I");
  m_recTree->Branch( "DCVSideModID",  DCVSideModID, "DCVSideModID[DCVSideNumber]/I");
  m_recTree->Branch( "DCVSideEne", DCVSideEne, "DCVSideEne[DCVSideNumber]/F");
  m_recTree->Branch( "DCVSideTime", DCVSideTime, "DCVSideTime[DCVSideNumber]/F");
  m_recTree->Branch( "DCVSideDeltaTime", DCVSideDeltaTime, "DCVSideDeltaTime[DCVSideNumber]/F");
  
  MTVeto::Branch(tr);


}

void MTVetoDCV::BranchOriginalData( TTree* tr )
{

  if( CopyDstBranchFlag ){
    tr->Branch( "DCVNumber",       &DetDstNumber, "DCVNumber/I");
    tr->Branch( "DCVModID",         DetDstModID,  "DCVModID[DCVNumber]/I");
    tr->Branch( "DCVIntegratedADC", DetDstIntADC, "DCVIntegratedADC[DCVNumber]/F" );
    tr->Branch( "DCVEne",           DetDstEne,    "DCVEne[DCVNumber]/F" );
    tr->Branch( "DCVTime",          DetDstTime,   "DCVTime[DCVNumber]/F" );
    tr->Branch( "DCVPTime",         DetDstPTime,  "DCVPTime[DCVNumber]/F" );
	MTVeto::Branch(tr);
  }
  
  
}


void MTVetoDCV::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::DCV))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }

  for( int imod=0; imod<8; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
    vetoHitBuffer[imod].position = 0;
  }

  for( int imod=0; imod<16; imod++ ){
    vetoHitBuffer2[imod].modID = 0;
    vetoHitBuffer2[imod].energy = 0;
    vetoHitBuffer2[imod].time = 0;
    vetoHitBuffer2[imod].position = 0;
  }

  DCVScintiNumber = 0;
  for(int i=0;i<8;i++){
	  DCVScintiModID[i] = 0;
	  DCVScintiEne[i] = 0;
	  DCVScintiTime[i] = 0;
	  DCVScintiDeltaTime[i] = 0;
  }

  DCVSideNumber = 0;
  for(int i=0;i<16;i++){
	  DCVSideModID[i] = 0;
	  DCVSideEne[i] = 0;
	  DCVSideTime[i] = 0;
	  DCVSideDeltaTime[i] = 0;
  }

  int map[8] = {0};
  int ishit[8] = {0};

  int map2[16] = {0};
  int ishit2[16] = {0};



	  
  int c = 0;
  int c2 = 0;
  for(int i=0;i<DetNumber;i++){

	  int modid = DetModID[i];
	  int scnid = modid/4;
	  int sideid = 2*(modid/4)+modid%2;

	  int mppcid = modid%4;
	  int pos = -1;
	  if(mppcid>=2) pos = 1;

	  if(ishit[scnid]==0){
		  ishit[scnid]++;
		  map[scnid] = c++;
		  DCVScintiModID[map[scnid]] = scnid;
		  DCVScintiNumber++;
	  }

	  if(ishit2[sideid]==0){
		  ishit2[sideid]++;
		  map2[sideid] = c2++;
		  DCVSideModID[map2[sideid]] = sideid;
		  DCVSideNumber++;
	  }

	  DCVScintiEne[map[scnid]]  += DetEne[i];
	  DCVScintiTime[map[scnid]] += DetTime[i]/4;
	  DCVScintiDeltaTime[map[scnid]] += DetTime[i]/2*pos;

	  DCVSideEne[map2[sideid]]  += DetEne[i];
	  DCVSideTime[map2[sideid]] += DetTime[i]/2;
	  DCVSideDeltaTime[map2[sideid]] += DetTime[i]*pos;

  }

  DCVVetoModID = -1;
  DCVVetoEne = 0;
  DCVVetoTime = 9999.;
  DCVVetoDeltaTime = 9999.;

  DCVSideVetoModID = -1;
  DCVSideVetoEne = 0;
  DCVSideVetoTime = 9999.;
  DCVSideVetoDeltaTime = 9999.;

}


bool MTVetoDCV::Process( int mode )
{

	
  Initialize();

  static double DCV1pos = GetDetZPosition("DCV1");
  static double DCV2pos = GetDetZPosition("DCV2");
  
  for( int imod=0; imod<DCVSideNumber; imod++ ){
    
	double DCVpos;
	if(DCVSideModID[imod]<8) DCVpos = DCV1pos;
	else DCVpos = DCV2pos;

	DCVSideVetoTime = DCVSideTime[imod] - m_eventStartTime - (DCVpos-m_eventStartZ)/(TMath::C()/1e6);


    if( IsInVetoWindow( DCVSideVetoTime )){
      vetoHitBuffer2[imod].modID    = DCVSideModID[imod];
      vetoHitBuffer2[imod].energy   = DCVSideEne[imod];
      vetoHitBuffer2[imod].time     = DCVSideVetoTime;
      vetoHitBuffer2[imod].position = DCVSideDeltaTime[imod];
    }else{
      vetoHitBuffer2[imod].modID = DCVSideModID[imod];
      vetoHitBuffer2[imod].energy = 0;
      if(DCVSideVetoTime>-1000) vetoHitBuffer2[imod].time = DCVSideVetoTime;
	  else vetoHitBuffer2[imod].time = 9999; 
      vetoHitBuffer2[imod].position = 9999;
    }
  }

  std::sort( vetoHitBuffer2, &(vetoHitBuffer2[16]), pVetoHit::largerHit );
  DCVSideVetoModID = vetoHitBuffer2[0].modID;
  DCVSideVetoEne   = vetoHitBuffer2[0].energy;
  DCVSideVetoTime  = vetoHitBuffer2[0].time;
  DCVSideVetoDeltaTime  = vetoHitBuffer2[0].position;

  for( int imod=0; imod<DCVScintiNumber; imod++ ){
    

	double DCVpos;
	if(DCVScintiModID[imod]<4) DCVpos = DCV1pos;
	else DCVpos = DCV2pos;

	DCVVetoTime = DCVScintiTime[imod] - m_eventStartTime - (DCVpos-m_eventStartZ)/(TMath::C()/1e6);

    if( IsInVetoWindow( DCVVetoTime )){
      vetoHitBuffer[imod].modID = DCVScintiModID[imod];
      vetoHitBuffer[imod].energy = DCVScintiEne[imod];
      vetoHitBuffer[imod].time = DCVVetoTime;
      vetoHitBuffer[imod].position = DCVScintiDeltaTime[imod];
    }else{
      vetoHitBuffer[imod].modID = DCVScintiModID[imod];
      vetoHitBuffer[imod].energy = 0;
      if(DCVVetoTime>-1000) vetoHitBuffer[imod].time = DCVVetoTime;
	  else vetoHitBuffer[imod].time = 9999; 
      vetoHitBuffer[imod].position = 9999;
    }
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[8]), pVetoHit::largerHit );
  DCVVetoModID = vetoHitBuffer[0].modID;
  DCVVetoEne   = vetoHitBuffer[0].energy;
  DCVVetoTime  = vetoHitBuffer[0].time;
  DCVVetoDeltaTime  = vetoHitBuffer[0].position;

  if( DCVVetoEne > m_vetoThreshold ) return true;
  return false;

}

bool MTVetoDCV::Process2( int mode, int userFlag ){

  Initialize();

  static double DCV1pos = GetDetZPosition("DCV1");
  static double DCV2pos = GetDetZPosition("DCV2");



  for( int imod=0; imod<DCVSideNumber; imod++ ){
    
	double DCVpos;
	if(DCVSideModID[imod]<8) DCVpos = DCV1pos;
	else DCVpos = DCV2pos;

	DCVSideVetoTime = DCVSideTime[imod] - m_eventStartTime - (DCVpos-m_eventStartZ)/(TMath::C()/1e6);

    if( IsInVetoWindow( DCVSideVetoTime )){
      vetoHitBuffer2[imod].modID    = DCVSideModID[imod];
      vetoHitBuffer2[imod].energy   = DCVSideEne[imod];
      vetoHitBuffer2[imod].time     = DCVSideVetoTime;
      vetoHitBuffer2[imod].position = DCVSideDeltaTime[imod];
    }else{
      vetoHitBuffer2[imod].modID = DCVSideModID[imod];
      vetoHitBuffer2[imod].energy = 0;
      if(DCVSideVetoTime>-1000) vetoHitBuffer2[imod].time = DCVSideVetoTime;
	  else vetoHitBuffer2[imod].time = 9999; 
      vetoHitBuffer2[imod].position = 9999;
    }
  }

  std::sort( vetoHitBuffer2, &(vetoHitBuffer2[16]), pVetoHit::largerHit );
  DCVSideVetoModID = vetoHitBuffer2[0].modID;
  DCVSideVetoEne   = vetoHitBuffer2[0].energy;
  DCVSideVetoTime  = vetoHitBuffer2[0].time;
  DCVSideVetoDeltaTime  = vetoHitBuffer2[0].position;


  for( int imod=0; imod<DCVScintiNumber; imod++ ){
    
	double DCVpos;
	if(DCVScintiModID[imod]<4) DCVpos = DCV1pos;
	else DCVpos = DCV2pos;
	
	DCVVetoTime = DCVScintiTime[imod] - m_eventStartTime - (DCVpos-m_eventStartZ)/(TMath::C()/1e6);


    if( IsInVetoWindow( DCVVetoTime )){
      vetoHitBuffer[imod].modID  = DCVScintiModID[imod];
      vetoHitBuffer[imod].energy = DCVScintiEne[imod];
      vetoHitBuffer[imod].time   = DCVVetoTime;
      vetoHitBuffer[imod].position = DCVScintiDeltaTime[imod];
    }else{
      vetoHitBuffer[imod].modID  = DCVScintiModID[imod];
      vetoHitBuffer[imod].energy = 0;
      if(DCVVetoTime>-1000) vetoHitBuffer[imod].time = DCVVetoTime;
	  else vetoHitBuffer[imod].time = 9999; 
      vetoHitBuffer[imod].position = 9999;
    }
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[8]), pVetoHit::largerHit );
  DCVVetoModID = vetoHitBuffer[0].modID;
  DCVVetoEne   = vetoHitBuffer[0].energy;
  DCVVetoTime  = vetoHitBuffer[0].time;
  DCVVetoDeltaTime  = vetoHitBuffer[0].position;

  if( DCVVetoEne > m_vetoThreshold ) return true;
  return false;
  
}


double MTVetoDCV::GetDetZPosition( char* detName ){
  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();
  return DetZPosition;
}


