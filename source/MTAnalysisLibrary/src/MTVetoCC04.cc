#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoCC04.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoCC04::MTVetoCC04( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "CC04";
  DetNumber = MTBP::CC04NChannels;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();

  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
  DetWfmCorrupted = NULL;

  E391DetNumber = MTBP::CC04E391NModules;
  E391DetModID = new Int_t [E391DetNumber]();
  E391DetTime = new Float_t [E391DetNumber]();
  E391DetEne = new Float_t [E391DetNumber]();
  E391DetSmoothness = new Float_t [E391DetNumber]();
  E391DetAreaR = new Float_t [E391DetNumber]();
  E391DetWfmCorrectNumber = new short[E391DetNumber]();
  E391DetPSBit = new Short_t [E391DetNumber]();

  KTeVDetNumber = MTBP::CC04KTeVNModules;
  KTeVDetModID = new Int_t [KTeVDetNumber]();
  KTeVDetTime = new Float_t [KTeVDetNumber]();
  KTeVDetEne = new Float_t [KTeVDetNumber]();
  KTeVDetSmoothness = new Float_t [KTeVDetNumber]();
  KTeVDetAreaR = new Float_t [KTeVDetNumber]();
  KTeVDetWfmCorrectNumber = new short[KTeVDetNumber]();
  KTeVDetPSBit = new Short_t [KTeVDetNumber]();

  ScintiDetNumber = MTBP::CC04ScintiNModules;
  ScintiDetModID = new Int_t [ScintiDetNumber]();
  ScintiDetTime = new Float_t [ScintiDetNumber]();
  ScintiDetEne = new Float_t [ScintiDetNumber]();
  ScintiDetSmoothness = new Float_t [ScintiDetNumber]();
  ScintiDetAreaR = new Float_t [ScintiDetNumber]();
  ScintiDetWfmCorrectNumber = new short [ScintiDetNumber]();
  ScintiDetPSBit = new Short_t [ScintiDetNumber]();

  vetoHitBuffer = new pVetoHit [DetNumber]();
  
  MakeWfmArray();
  DetNumber = MTBP::CC04NModules;
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
}


MTVetoCC04::~MTVetoCC04()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetPSBit;
  delete[] DetSmoothness;

  delete[] E391DetModID;
  delete[] E391DetTime;
  delete[] E391DetEne;
  delete[] E391DetSmoothness;
  delete[] E391DetAreaR;
  delete[] E391DetWfmCorrectNumber;
  delete[] E391DetPSBit;

  delete[] KTeVDetModID;
  delete[] KTeVDetTime;
  delete[] KTeVDetSmoothness;
  delete[] KTeVDetEne;
  delete[] KTeVDetAreaR;
  delete[] KTeVDetWfmCorrectNumber;
  delete[] KTeVDetPSBit;

  delete[] ScintiDetModID;
  delete[] ScintiDetTime;
  delete[] ScintiDetEne;
  delete[] ScintiDetSmoothness;
  delete[] ScintiDetAreaR;
  delete[] ScintiDetWfmCorrectNumber;
  delete[] ScintiDetPSBit;
  
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoCC04::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "CC04ModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "CC04ModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "CC04ModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "CC04ModuleEne", DetEne );
  
  if(m_clusterTree->GetBranch("CC04ModulePSBit"))
	  m_clusterTree->SetBranchAddress( "CC04ModulePSBit", DetPSBit );

  if(m_clusterTree->GetBranch("CC04ModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "CC04ModuleSmoothness", DetSmoothness );  
  }

  MTVeto::SetBranchAddress(tr);
}


void MTVetoCC04::Branch( TTree* tr )
{
  m_recTree = tr;
  
  //m_recTree->Branch( "CC04ModuleNumber", &DetNumber, "CC04ModuleNumber/I");
  //m_recTree->Branch( "CC04ModuleModID", DetModID, "CC04ModuleModID[CC04ModuleNumber]/I");
  //m_recTree->Branch( "CC04ModuleEne", DetEne, "CC04ModuleEne[CC04ModuleNumber]/D");
  //m_recTree->Branch( "CC04ModuleTime", DetTime, "CC04ModuleTime[CC04ModuleNumber]/D");
  //m_recTree->Branch( "CC04ModuleSmoothness", DetSmoothness, "CC04Smoothness[CC04ModuleNumber]/F" );
  
  m_recTree->Branch( "CC04E391ModuleNumber", &E391DetNumber, "CC04E391ModuleNumber/I");
  m_recTree->Branch( "CC04E391ModuleModID", E391DetModID, "CC04E391ModuleModID[CC04E391ModuleNumber]/I");
  m_recTree->Branch( "CC04E391ModuleEne", E391DetEne, "CC04E391ModuleEne[CC04E391ModuleNumber]/F");
  m_recTree->Branch( "CC04E391ModuleTime", E391DetTime, "CC04E391ModuleTime[CC04E391ModuleNumber]/F");

  if(m_clusterTree->GetBranch("CC04ModuleAreaR")!=0 ){
	  tr->Branch( "CC04E391ModuleAreaR", E391DetAreaR, "CC04E391ModuleAreaR[CC04E391ModuleNumber]");
  }
  if(m_clusterTree->GetBranch("CC04ModuleWfmCorrectNumber")!=0 ){
	  tr->Branch( "CC04E391ModuleWfmCorrectNumber", E391DetWfmCorrectNumber, "CC04E391ModuleWfmCorrectNumber[CC04E391ModuleNumber]/S");
  }
  
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "CC04E391ModuleSmoothness", E391DetSmoothness, "CC04E391ModuleSmoothness[CC04E391ModuleNumber]/F");

  if(m_clusterTree->GetBranch("CC04ModulePSBit"))
	  m_recTree->Branch( "CC04E391ModulePSBit", E391DetPSBit, "CC04E391ModulePSBit[CC04E391ModuleNumber]/S");
  
  m_recTree->Branch( "CC04KTeVModuleNumber", &KTeVDetNumber, "CC04KTeVModuleNumber/I");
  m_recTree->Branch( "CC04KTeVModuleModID", KTeVDetModID, "CC04KTeVModuleModID[CC04KTeVModuleNumber]/I");
  m_recTree->Branch( "CC04KTeVModuleEne", KTeVDetEne, "CC04KTeVModuleEne[CC04KTeVModuleNumber]/F");
  m_recTree->Branch( "CC04KTeVModuleTime", KTeVDetTime, "CC04KTeVModuleTime[CC04KTeVModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "CC04KTeVModuleSmoothness", KTeVDetSmoothness, "CC04KTeVModuleSmoothness[CC04KTeVModuleNumber]/F");
  if(m_clusterTree->GetBranch("CC04ModulePSBit"))
	  m_recTree->Branch( "CC04KTeVModulePSBit", KTeVDetPSBit, "CC04KTeVModulePSBit[CC04KTeVModuleNumber]/S");
  if(m_clusterTree->GetBranch("CC04ModuleAreaR")!=0 ){
	  tr->Branch( "CC04KTeVModuleAreaR", KTeVDetAreaR, "CC04KTeVModuleAreaR[CC04KTeVModuleNumber]");
  }
  if(m_clusterTree->GetBranch("CC04ModuleWfmCorrectNumber")!=0 ){
	  tr->Branch( "CC04KTeVModuleWfmCorrectNumber", KTeVDetWfmCorrectNumber, "CC04KTeVModuleWfmCorrectNumber[CC04KTeVModuleNumber]/S");
  }
	  
  m_recTree->Branch( "CC04ScintiModuleNumber", &ScintiDetNumber, "CC04ScintiModuleNumber/I");
  m_recTree->Branch( "CC04ScintiModuleModID", ScintiDetModID, "CC04ScintiModuleModID[CC04ScintiModuleNumber]/I");
  m_recTree->Branch( "CC04ScintiModuleEne", ScintiDetEne, "CC04ScintiModuleEne[CC04ScintiModuleNumber]/F");
  m_recTree->Branch( "CC04ScintiModuleTime", ScintiDetTime, "CC04ScintiModuleTime[CC04ScintiModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "CC04ScintiModuleSmoothness", ScintiDetSmoothness, "CC04ScintiModuleSmoothness[CC04ScintiModuleNumber]/F");
  if(m_clusterTree->GetBranch("CC04ModulePSBit"))
	  m_recTree->Branch( "CC04ScintiModulePSBit", ScintiDetPSBit, "CC04ScintiModulePSBit[CC04ScintiModuleNumber]/S");
  if(m_clusterTree->GetBranch("CC04ModuleAreaR")!=0 ){
	  tr->Branch( "CC04ScintiModuleAreaR", ScintiDetAreaR, "CC04ScintiModuleAreaR[CC04ScintiModuleNumber]");
  }
  if(m_clusterTree->GetBranch("CC04WfmCorrectNumber")!=0 ){
	  tr->Branch( "CC04ScintiModuleWfmCorrectNumber", ScintiDetWfmCorrectNumber, "CC04ScintiModuleWfmCorrectNumber[CC04ScintiModuleNumber]/S");
  }
  
  //m_recTree->Branch( "CC04VetoModID", &CC04VetoModID,"CC04VetoModID/I");
  //m_recTree->Branch( "CC04VetoEne", &CC04VetoEne,"CC04VetoEne/D");
  //m_recTree->Branch( "CC04VetoTime", &CC04VetoTime,"CC04VetoTime/D");

  m_recTree->Branch( "CC04E391VetoModID", &CC04E391VetoModID,"CC04E391VetoModID/I");
  m_recTree->Branch( "CC04E391VetoEne", &CC04E391VetoEne,"CC04E391VetoEne/F");
  m_recTree->Branch( "CC04E391VetoTime", &CC04E391VetoTime,"CC04E391VetoTime/F");

  m_recTree->Branch( "CC04KTeVVetoModID", &CC04KTeVVetoModID,"CC04KTeVVetoModID/I");
  m_recTree->Branch( "CC04KTeVVetoEne", &CC04KTeVVetoEne,"CC04KTeVVetoEne/F");
  m_recTree->Branch( "CC04KTeVVetoTime", &CC04KTeVVetoTime,"CC04KTeVVetoTime/F");

  m_recTree->Branch( "CC04ScintiVetoModID", &CC04ScintiVetoModID,"CC04ScintiVetoModID/I");
  m_recTree->Branch( "CC04ScintiVetoEne", &CC04ScintiVetoEne,"CC04ScintiVetoEne/F");
  m_recTree->Branch( "CC04ScintiVetoTime", &CC04ScintiVetoTime,"CC04ScintiVetoTime/F");
 
  MTVeto::Branch(tr);
  
}


void MTVetoCC04::Initialize( void )
{

  for( int imod=0; imod<MTBP::CC04NModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }


  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::CC04))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  E391DetNumber=0;
  KTeVDetNumber=0;
  ScintiDetNumber=0;


      
  for(int i=0; i<DetNumber; i++){
    if(DetModID[i]<42){
      E391DetModID[E391DetNumber]=DetModID[i];
      E391DetEne[E391DetNumber]=DetEne[i];
      E391DetTime[E391DetNumber]=DetTime[i];
      E391DetSmoothness[E391DetNumber]=DetSmoothness[i];
      E391DetAreaR[E391DetNumber]=DetAreaR[i];
      E391DetWfmCorrectNumber[E391DetNumber]=DetWfmCorrectNumber[i];
      E391DetPSBit[E391DetNumber]=DetPSBit[i];
      E391DetNumber+=1;
    }
    else if( 42<= DetModID[i] && DetModID[i]< 60){
      KTeVDetModID[KTeVDetNumber]=DetModID[i];
      KTeVDetEne[KTeVDetNumber]=DetEne[i];
      KTeVDetTime[KTeVDetNumber]=DetTime[i];
      KTeVDetSmoothness[KTeVDetNumber]=DetSmoothness[i];
      KTeVDetAreaR[KTeVDetNumber]=DetAreaR[i];
      KTeVDetWfmCorrectNumber[KTeVDetNumber]=DetWfmCorrectNumber[i];
      KTeVDetPSBit[KTeVDetNumber]=DetPSBit[i];
      KTeVDetNumber+=1;
    }
    else {
      ScintiDetModID[ScintiDetNumber]=DetModID[i];
      ScintiDetEne[ScintiDetNumber]=DetEne[i];
      ScintiDetTime[ScintiDetNumber]=DetTime[i];
      ScintiDetSmoothness[ScintiDetNumber]=DetSmoothness[i];
      ScintiDetAreaR[ScintiDetNumber]=DetAreaR[i];
      ScintiDetWfmCorrectNumber[ScintiDetNumber]=DetWfmCorrectNumber[i];
      ScintiDetPSBit[ScintiDetNumber]=DetPSBit[i];
      ScintiDetNumber+=1;
    }
  }


  CC04VetoModID = -1;
  CC04VetoEne = 0;
  CC04VetoTime = 9999.;

  CC04E391VetoModID = -1;
  CC04E391VetoEne = 0;
  CC04E391VetoTime = 9999.;

  CC04KTeVVetoModID = -1;
  CC04KTeVVetoEne = 0;
  CC04KTeVVetoTime = 9999.;

  CC04ScintiVetoModID = -1;
  CC04ScintiVetoEne = 0;
  CC04ScintiVetoTime = 9999.;
  
}


bool MTVetoCC04::Process( int mode ){

    Initialize();

    static const double s_CC04ZPosition = GetDetZPosition("CC04");
   
  int CC04ModuleNumber= 42+16+4;
  
  for( int imod=0; imod<E391DetNumber; imod++ ){
    vetoTime = E391DetTime[imod] - m_eventStartTime-(s_CC04ZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = E391DetModID[imod];
      vetoHitBuffer[imod].energy = E391DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
    }else{
      vetoHitBuffer[imod].modID = E391DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }

  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[E391DetNumber]), pVetoHit::largerHit );

  if( vetoHitBuffer[0].energy>0 && E391DetNumber>0){  
    CC04E391VetoModID = vetoHitBuffer[0].modID;
    CC04E391VetoEne   = vetoHitBuffer[0].energy;
    CC04E391VetoTime  = vetoHitBuffer[0].time;
  } else{
    CC04E391VetoModID = -1;
    CC04E391VetoEne   = 0;
    CC04E391VetoTime  = -9999.;
  }

  for( int imod=0; imod<KTeVDetNumber; imod++ ){
    vetoTime = KTeVDetTime[imod] - m_eventStartTime-(s_CC04ZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[E391DetNumber+imod].modID = KTeVDetModID[imod];
      vetoHitBuffer[E391DetNumber+imod].energy = KTeVDetEne[imod];
      vetoHitBuffer[E391DetNumber+imod].time = vetoTime;
    }else{
      vetoHitBuffer[E391DetNumber+imod].modID = KTeVDetModID[imod];
      vetoHitBuffer[E391DetNumber+imod].energy = 0;
      vetoHitBuffer[E391DetNumber+imod].time = vetoTime;
    }

  }
  std::sort( &(vetoHitBuffer[E391DetNumber]), &(vetoHitBuffer[E391DetNumber+KTeVDetNumber]), pVetoHit::largerHit );

  if( vetoHitBuffer[E391DetNumber].energy>0 && KTeVDetNumber>0){  
    CC04KTeVVetoModID = vetoHitBuffer[E391DetNumber].modID;
    CC04KTeVVetoEne   = vetoHitBuffer[E391DetNumber].energy;
    CC04KTeVVetoTime  = vetoHitBuffer[E391DetNumber].time;
  } else{
    CC04KTeVVetoModID = -1;
    CC04KTeVVetoEne   = 0;
    CC04KTeVVetoTime  = -9999.;
  }

  int crystalnum=E391DetNumber+KTeVDetNumber;
  
  for( int imod=0; imod<ScintiDetNumber; imod++ ){
    vetoTime = ScintiDetTime[imod] - m_eventStartTime-(s_CC04ZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[crystalnum+imod].modID = ScintiDetModID[imod];
      vetoHitBuffer[crystalnum+imod].energy = ScintiDetEne[imod];
      vetoHitBuffer[crystalnum+imod].time = vetoTime;
    }else{
      vetoHitBuffer[crystalnum+imod].modID = ScintiDetModID[imod];
      vetoHitBuffer[crystalnum+imod].energy = 0;
      vetoHitBuffer[crystalnum+imod].time = vetoTime;
    }

  }
  std::sort( &(vetoHitBuffer[crystalnum]), &(vetoHitBuffer[crystalnum+ScintiDetNumber]), pVetoHit::largerHit );

  if( vetoHitBuffer[crystalnum].energy>0 && ScintiDetNumber>0){  
    CC04ScintiVetoModID = vetoHitBuffer[crystalnum].modID;
    CC04ScintiVetoEne   = vetoHitBuffer[crystalnum].energy;
    CC04ScintiVetoTime  = vetoHitBuffer[crystalnum].time;
  } else{
    CC04ScintiVetoModID = -1;
    CC04ScintiVetoEne   = 0;
    CC04ScintiVetoTime  = -9999.;
  }

  if( CC04E391VetoEne > m_vetoThreshold ) return true;
  if( CC04KTeVVetoEne > m_vetoThreshold ) return true;
  if( CC04ScintiVetoEne > 1 ) return true;


  

  return false;

}


double MTVetoCC04::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName, 0);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoCC04::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName, 0);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
