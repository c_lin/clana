#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoMBCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoMBCV::MTVetoMBCV( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "MBCV"; 
  DetNumber = MTBP::MBCVNModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();

  DetWfmCorrupted = NULL;
  vetoHitBuffer = new pVetoHit [DetNumber]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  MakeWfmArray();
  Initialize();
}


MTVetoMBCV::~MTVetoMBCV()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetPSBit;
  delete[] DetEne;
  delete[] DetSmoothness;
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoMBCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "MBCVModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "MBCVModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "MBCVModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "MBCVModuleEne", DetEne );
  if(m_clusterTree->GetBranch("MBCVModulePSBit"))
	  m_clusterTree->SetBranchAddress( "MBCVModulePSBit", DetPSBit );
  if(m_clusterTree->GetBranch("MBCVModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "MBCVModuleSmoothness", DetSmoothness );  
  }
  MTVeto::SetBranchAddress(tr);
}


void MTVetoMBCV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "MBCVModuleNumber", &DetNumber, "MBCVModuleNumber/I");
  m_recTree->Branch( "MBCVModuleModID", DetModID, "MBCVModuleModID[MBCVModuleNumber]/I");
  m_recTree->Branch( "MBCVModuleEne", DetEne, "MBCVModuleEne[MBCVModuleNumber]/F");
  m_recTree->Branch( "MBCVModuleTime", DetTime, "MBCVModuleTime[MBCVModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "MBCVModuleSmoothness", DetSmoothness, "MBCVModuleSmoothness[MBCVModuleNumber]/F" );
  if(m_clusterTree->GetBranch("MBCVModulePSBit"))
	  m_recTree->Branch( "MBCVModulePSBit", DetPSBit, "MBCVModulePSBit[MBCVModuleNumber]/S");

  m_recTree->Branch( "MBCVVetoModID", &MBCVVetoModID,"MBCVVetoModID/I");
  m_recTree->Branch( "MBCVVetoEne", &MBCVVetoEne,"MBCVVetoEne/F");
  m_recTree->Branch( "MBCVVetoTime", &MBCVVetoTime,"MBCVVetoTime/F");

  m_recTree->Branch( "MBCVCH2VetoEne",   &MBCVCH2VetoEne,  "MBCVCH2VetoEne/F");
  m_recTree->Branch( "MBCVCH2VetoTime",  &MBCVCH2VetoTime, "MBCVCH2VetoTime/F");

  MTVeto::Branch(tr);
}


void MTVetoMBCV::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::MBCV))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  for( int imod=0; imod<MTBP::MBCVNModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  MBCVVetoModID = -1;
  MBCVVetoEne = 0;
  MBCVVetoTime = 9999.;

  MBCVCH2VetoEne = 0;
  MBCVCH2VetoTime = -9999;

}


bool MTVetoMBCV::Process( int mode ){

  Initialize();

  static const double CSIZPosition = GetDetZPosition("CSI");
  static const std::vector<int> s_DeadModuleIDVec = GetDeadModuleIDVec("MBCV");  

  for( int imod=0; imod<DetNumber; imod++ ){
    //vetoTime = DetTime[imod] - m_eventStartTime;
    vetoTime = DetTime[imod] - m_eventStartTime - (CSIZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if(imod==2){
      if( IsInVetoWindow( vetoTime )){
	MBCVCH2VetoEne  = DetEne[imod];
	MBCVCH2VetoTime = vetoTime;
      }else{
	MBCVCH2VetoEne  = 0;
	MBCVCH2VetoTime = -9999.;
      }
    }

    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }

    // dead channel treatment
    Bool_t DeadFlag=false;
    for(int ideadmod=0;ideadmod<s_DeadModuleIDVec.size();ideadmod++){
      if( DetModID[imod]==s_DeadModuleIDVec.at(ideadmod) ){
	DeadFlag=true;
	break;
      }
    }
    if( DeadFlag ){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }

  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );

  if(vetoHitBuffer[0].energy>0){  
    MBCVVetoModID = vetoHitBuffer[0].modID;
    MBCVVetoEne   = vetoHitBuffer[0].energy;
    MBCVVetoTime  = vetoHitBuffer[0].time;
  } else {
    MBCVVetoModID = -1;
    MBCVVetoEne   = 0;
    MBCVVetoTime  = -9999.;
  }

  if( MBCVVetoEne > m_vetoThreshold ) return true;
  return false;

}


double MTVetoMBCV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoMBCV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
