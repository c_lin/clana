#include "MTAnalysisLibrary/MTVetoCSI.h"
#include "MTAnalysisLibrary/MTPositionHandler.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

#include "pi0/Pi0.h"
#include "gamma/Gamma.h"

MTVetoCSI::MTVetoCSI( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "CSI"; 
  DetNumber = MTBP::CSINChannels;

  CSIVetoNumber = 0;
  CSIVetoModID = new Int_t [DetNumber]();
  CSIVetoEne = new Double_t [DetNumber]();
  CSIVetoDistance = new Double_t [DetNumber]();
  CSIVetoDeltaTime = new Double_t [DetNumber]();
  
  
  DetWfmCorrupted = NULL;
  pHandler = MTPositionHandler::GetInstance();

  DetModID = new Int_t [DetNumber]();
  UTCModID = new Int_t [256]();
  
  CSISmoothness = new Float_t [DetNumber]();
  UTCSmoothness = new Float_t [256]();

  DetPSBit = new Short_t [DetNumber]();
  CSIPSModID = new Int_t[DetNumber];

  CSIWfmCorruptedModID = new Int_t[DetNumber];
  CSIWfmCorruptedSmoothness = new Float_t[DetNumber];
  UTCWfmCorruptedModID = new Int_t[256];
  UTCWfmCorruptedSmoothness = new Float_t[256];

  CSIWfmCorrptedNumber = 0;
  UTCWfmCorrptedNumber = 0;
  CSIPSModNumber = 0;

  MakeWfmArray();
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

}


MTVetoCSI::~MTVetoCSI()
{
  delete[] DetModID;
  delete[] UTCModID;
  delete[] DetPSBit;
  delete[] CSIVetoModID;
  delete[] CSIVetoEne;
  delete[] CSIVetoDistance;
  delete[] CSIVetoDeltaTime;
  delete[] CSIPSModID;
  delete[] CSISmoothness;
  delete[] UTCSmoothness;
  delete[] CSIWfmCorruptedModID;
  delete[] CSIWfmCorruptedSmoothness;
  delete[] UTCWfmCorruptedModID;
  delete[] UTCWfmCorruptedSmoothness;

  delete   m_E14BP;
}


void MTVetoCSI::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "CSINumber", &DetModNumber);
  m_clusterTree->SetBranchAddress( "CSIModID", DetModID );
 
  if(m_userFlag<20190101) return;

  pUTCNumber = (int*) m_clusterTree->GetBranch("UTCNumber")->GetAddress();
  ptrUTCModID = (int*) m_clusterTree->GetBranch("UTCModID")->GetAddress();
  if(ptrUTCModID==NULL){
	  m_clusterTree->SetBranchAddress( "UTCModID", UTCModID );
	  ptrUTCModID = UTCModID;
  }

  if(m_clusterTree->GetBranch("CSIPSBit"))
	  m_clusterTree->SetBranchAddress( "CSIPSBit", DetPSBit );
  
  
  if(m_clusterTree->GetBranch("CSISmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "CSISmoothness", CSISmoothness );  
	  m_clusterTree->SetBranchAddress( "UTCSmoothness", UTCSmoothness );  

  }
  MTVeto::SetBranchAddress(tr);
}


void MTVetoCSI::Branch( TTree* tr )
{
  m_recTree = tr;

  m_recTree->Branch( "CSIVetoNumber", &CSIVetoNumber, "CSIVetoNumber/I" );
  m_recTree->Branch( "CSIVetoModID", CSIVetoModID, "CSIVetoModID[CSIVetoNumber]/I" );
  m_recTree->Branch( "CSIVetoEne", CSIVetoEne, "CSIVetoEne[CSIVetoNumber]/D" );
  m_recTree->Branch( "CSIVetoDistance", CSIVetoDistance, "CSIVetoDistance[CSIVetoNumber]/D" );
  m_recTree->Branch( "CSIVetoDeltaTime", CSIVetoDeltaTime, "CSIVetoDeltaTime[CSIVetoNumber]/D" );


  if(m_userFlag<20190101) return;
  if(m_clusterTree->GetBranch("CSIPSBit")){
	  m_recTree->Branch( "CSIPSModNumber", &CSIPSModNumber, "CSIPSModNumber/I" );
	  m_recTree->Branch( "CSIPSModID", CSIPSModID, "CSIPSModID[CSIPSModNumber]/I" );
  }
 
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit")){
	  m_recTree->Branch( "CSIWfmCorruptedNumber",    &CSIWfmCorrptedNumber, "CSIWfmCorruptedNumber/I" );
	  m_recTree->Branch( "CSIWfmCorruptedModID",      CSIWfmCorruptedModID, "CSIWfmCorruptedModID[CSIWfmCorruptedNumber]/I" );
	  m_recTree->Branch( "CSIWfmCorruptedSmoothness", CSIWfmCorruptedSmoothness, "CSIWfmCorruptedSmoothness[CSIWfmCorruptedNumber]" );
	  m_recTree->Branch( "UTCWfmCorruptedNumber",    &UTCWfmCorrptedNumber, "UTCWfmCorruptedNumber/I" );
	  m_recTree->Branch( "UTCWfmCorruptedModID",      UTCWfmCorruptedModID, "UTCWfmCorruptedModID[UTCWfmCorruptedNumber]/I" );
	  m_recTree->Branch( "UTCWfmCorruptedSmoothness", UTCWfmCorruptedSmoothness, "UTCWfmCorruptedSmoothness[UTCWfmCorruptedNumber]" );
  }
  
  MTVeto::Branch(tr);
  
}


void MTVetoCSI::SetKlInfo( Klong* kl )
{
  m_kl = kl;
}


void MTVetoCSI::SetPi0Info( Pi0* pi0 )
{
  m_pi0 = pi0;
}

void MTVetoCSI::FillWfmCorruptedInfo(void){

 
	if(m_clusterTree->GetBranch("DetectorWFMCorruptBit")==NULL)return;

    const double CSIWfm_Smoothnessthreshold = 80;
    const double UTCWfm_Smoothnessthreshold = 50;

	CSIWfmCorrptedNumber = 0;
	UTCWfmCorrptedNumber = 0;

	for(int i=0;i<DetModNumber;i++){
		double smth = CSISmoothness[i];
		if(fabs(smth)>CSIWfm_Smoothnessthreshold){
			int modid = DetModID[i];
			CSIWfmCorruptedModID[CSIWfmCorrptedNumber] = modid;
			CSIWfmCorruptedSmoothness[CSIWfmCorrptedNumber] = smth; 
			CSIWfmCorrptedNumber++; 
		}
	}

	for(int i=0;i<(*pUTCNumber);i++){
		double smth = UTCSmoothness[i];
		if(fabs(smth)>UTCWfm_Smoothnessthreshold){
			int modid = ptrUTCModID[i];
			UTCWfmCorruptedModID[UTCWfmCorrptedNumber] = modid;
			UTCWfmCorruptedSmoothness[UTCWfmCorrptedNumber] = smth; 
			UTCWfmCorrptedNumber++; 
		}
	}
}





void MTVetoCSI::SetSuppressedIDs(void){
	if(!m_clusterTree->GetBranch("CSIPSBit")) return;

	CSIPSModNumber = 0;
	for(int i=0;i<DetModNumber;i++){
		if(DetPSBit[i]==0){
			CSIPSModID[CSIPSModNumber++] = DetModID[i];
		}
	}
}

bool MTVetoCSI::Process( int mode )
{


  if(m_userFlag>=20190101){
	  SetSuppressedIDs();
	  FillWfmCorruptedInfo();
  }

  CSIVetoNumber = 0;

  int nGammas = 0;
  if( mode==4 || mode==6 ){
    nGammas = m_kl->pi0().size() * 2;
  }else if( mode==2 ){
    nGammas = 2;
  }else if( mode==3){
	  nGammas = 3;
  }

  // cluster position and timing
  double cx[6]={0}, cy[6]={0}, ct[6]={0}; // Kl reconstruction clusters
  if(mode==3 || mode==4 || mode==5 || mode==6 ){
    for( int ipi0=0; ipi0<nGammas/2; ipi0++ ){
      Pi0 const& pi0 = m_kl->pi0().at(ipi0);
      for( int igamma=0; igamma<2; igamma++ ){
	Gamma const &g = (igamma==0) ? pi0.g1() : pi0.g2();
	cx[ipi0*2+igamma] = g.x();
	cy[ipi0*2+igamma] = g.y();
	ct[ipi0*2+igamma] = g.t();
      }
    }
  }else if( mode==2 ){
    for( int igamma=0; igamma<2; igamma++ ){
      Gamma const &g = (igamma==0) ? m_pi0->g1() : m_pi0->g2();
      cx[igamma] = g.x();
      cy[igamma] = g.y();
      ct[igamma] = g.t();
    }
  }
  
  if(mode==3){
	  Gamma const&g = *(m_kl->gamma().begin());
	  cx[2] = g.x();
	  cy[2] = g.y();
	  ct[2] = g.t();
  }else if(mode==5){

	  int ng = 0;
	  for( std::list<Gamma>::const_iterator it = m_kl->gamma().begin(); it!=m_kl->gamma().end() ; it++ ){
	  	  Gamma const &g = *it;
		  cx[4+ng] = g.x();
		  cy[4+ng] = g.y();
		  ct[4+ng] = g.t();
	  	  ng++;
  	  }
	  nGammas = 4+ng; 
  }
  

  // Get Csi crystal list
  std::map< int, int> IDIndexMap;
  std::list< int > IDList;
  for( int icsi=0; icsi<m_data->CsiNumber; icsi++ ){
    IDIndexMap.insert( std::map< int, int>::value_type( m_data->CsiModID[icsi], icsi) );
    IDList.push_back( m_data->CsiModID[icsi] );
  }
  
  // Remove csi id which belong to clusters from list
  for( int iclus=0; iclus<m_data->ClusterNumber; iclus++ ){
    for( int icsi=0; icsi<m_data->ClusterSize[iclus]; icsi++ ){
      IDList.remove( m_data->ClusterCsiId[iclus][icsi] );
    }
  }
  

  // Get CSI veto information
  double x=0, y=0, z=0;
  for( std::list<int>::iterator it=IDList.begin(); it!=IDList.end(); it++ ){
    CSIVetoModID[CSIVetoNumber] = *it;
    CSIVetoEne[CSIVetoNumber]   = m_data->CsiEne[ IDIndexMap[*it]];
    
    pHandler->GetCSIXYZPosition( *it, x, y, z, m_userFlag);
    CSIVetoDistance[CSIVetoNumber]  = sqrt( pow( x-cx[0], 2) + pow( y-cy[0], 2) );
    CSIVetoDeltaTime[CSIVetoNumber] = m_data->CsiTime[IDIndexMap[*it]] - ct[0];
    for( int igamma=1; igamma<nGammas; igamma++ ){
      double tmpdist = sqrt( pow( x-cx[igamma], 2) + pow( y-cy[igamma], 2) );
      if( CSIVetoDistance[CSIVetoNumber] > tmpdist ){
	CSIVetoDistance[CSIVetoNumber]  = tmpdist;
	CSIVetoDeltaTime[CSIVetoNumber] = m_data->CsiTime[IDIndexMap[*it]] - ct[igamma];
      }
    }
  
    CSIVetoNumber++;
  }
    


  // Veto
  if( mode==3 || mode==4 || mode==2 || mode==5 || mode==6 ){
    for( int icsi=0; icsi<CSIVetoNumber; icsi++ ){
      if( fabs(CSIVetoDeltaTime[icsi]) > 10. ) continue;

      if( CSIVetoDistance[icsi] < 200. ){
	//if( CSIVetoEne[icsi] > (23.4-0.034*CSIVetoDistance[icsi]) ){
	if( CSIVetoEne[icsi] > 10 ){	  // change by shiomi at 2015/05/09
	  return true;
	}
      }else if( 200<CSIVetoDistance[icsi] && CSIVetoDistance[icsi] < 600. ){
	if( CSIVetoEne[icsi] > 10-(CSIVetoDistance[icsi]-200)/400.*7){
	  return true;
	}
      }else{
	if( CSIVetoEne[icsi] > 3. ){
	  return true;
	}
      }
    }
  }
  return false;
}


bool  MTVetoCSI::Process( int mode, bool& vetoFlag, bool& looseVetoFlag, bool& tightVetoFlag )
{

  if(m_userFlag>=20190101){
	  SetSuppressedIDs();
	  FillWfmCorruptedInfo();
  }  
  
  CSIVetoNumber = 0;

  int nGammas = 0;
  if( mode==4 || mode==6 ){
    nGammas = m_kl->pi0().size() * 2;
  }else if( mode==2 ){
    nGammas = 2;
  }


  // cluster position and timing
  double cx[6]={0}, cy[6]={0}, ct[6]={0}; // Kl reconstruction clusters
  if( mode==4 || mode==6 ){
    for( int ipi0=0; ipi0<nGammas/2; ipi0++ ){
      Pi0 const& pi0 = m_kl->pi0().at(ipi0);
      for( int igamma=0; igamma<2; igamma++ ){
	Gamma const &g = (igamma==0) ? pi0.g1() : pi0.g2();
	cx[ipi0*2+igamma] = g.x();
	cy[ipi0*2+igamma] = g.y();
	ct[ipi0*2+igamma] = g.t();
      }
    }
  }else if( mode==2 ){
    for( int igamma=0; igamma<2; igamma++ ){
      Gamma const &g = (igamma==0) ? m_pi0->g1() : m_pi0->g2();
      cx[igamma] = g.x();
      cy[igamma] = g.y();
      ct[igamma] = g.t();
    }
  }
  
  // Get Csi crystal list
  std::map< int, int> IDIndexMap;
  std::list< int > IDList;
  for( int icsi=0; icsi<m_data->CsiNumber; icsi++ ){
    IDIndexMap.insert( std::map< int, int>::value_type( m_data->CsiModID[icsi], icsi) );
    IDList.push_back( m_data->CsiModID[icsi] );
  }
  
  // Remove csi id which belong to clusters from list
  for( int iclus=0; iclus<m_data->ClusterNumber; iclus++ ){
    for( int icsi=0; icsi<m_data->ClusterSize[iclus]; icsi++ ){
      IDList.remove( m_data->ClusterCsiId[iclus][icsi] );
    }
  }

  // Get CSI veto information
  double x=0, y=0, z=0;
  for( std::list<int>::iterator it=IDList.begin(); it!=IDList.end(); it++ ){
    CSIVetoModID[CSIVetoNumber] = *it;
    CSIVetoEne[CSIVetoNumber]   = m_data->CsiEne[ IDIndexMap[*it]];
    
    pHandler->GetCSIXYZPosition( *it, x, y, z, m_userFlag);
    CSIVetoDistance[CSIVetoNumber]  = sqrt( pow( x-cx[0], 2) + pow( y-cy[0], 2) );
    CSIVetoDeltaTime[CSIVetoNumber] = m_data->CsiTime[IDIndexMap[*it]] - ct[0];
    for( int igamma=1; igamma<nGammas; igamma++ ){
      double tmpdist = sqrt( pow( x-cx[igamma], 2) + pow( y-cy[igamma], 2) );
      if( CSIVetoDistance[CSIVetoNumber] > tmpdist ){
	CSIVetoDistance[CSIVetoNumber]  = tmpdist;
	CSIVetoDeltaTime[CSIVetoNumber] = m_data->CsiTime[IDIndexMap[*it]] - ct[igamma];
      }
    }
  
    CSIVetoNumber++;
  }
    
  // Veto
  vetoFlag = false;
  looseVetoFlag = false;
  tightVetoFlag = false;
  if( mode==4 || mode==2 || mode==6 ){
    for( int icsi=0; icsi<CSIVetoNumber; icsi++ ){
      if( fabs(CSIVetoDeltaTime[icsi]) > 10. ) continue;

      if( CSIVetoDistance[icsi] < 100. ){
	continue;
      }else if( CSIVetoDistance[icsi] < 600. ){
	if( CSIVetoEne[icsi] > (23.4-0.034*CSIVetoDistance[icsi]) ){
	  vetoFlag = true;
	}
	if( CSIVetoEne[icsi] > (23.4-0.034*CSIVetoDistance[icsi])*1.1 ){
	  looseVetoFlag = true;
	}
	if( CSIVetoEne[icsi] > (23.4-0.034*CSIVetoDistance[icsi])/1.1 ){
	  tightVetoFlag = true;
	}
      }else{
	if( CSIVetoEne[icsi] > 3. ){
	  vetoFlag = true;
	}
	if( CSIVetoEne[icsi] > 3.*1.1 ){
	  looseVetoFlag = true;
	}
	if( CSIVetoEne[icsi] > 3./1.1 ){
	  tightVetoFlag = true;
	}
      }
    }
  }

  return vetoFlag;
}

double MTVetoCSI::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoCSI::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
