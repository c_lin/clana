#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoFBAR.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoFBAR::MTVetoFBAR( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "FBAR"; 
  DetNumber = MTBP::FBARNModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();

  vetoHitBuffer = new pVetoHit [DetNumber]();

  DetWfmCorrupted = NULL;
  CopyDstBranchFlag = false;
  DetDstNumber   = MTBP::FBARNChannels;
  DetDstModID    = new Int_t  [DetDstNumber];
  DetDstIntADC   = new Float_t[DetDstNumber];
  DetDstEne      = new Float_t[DetDstNumber];
  DetDstTime     = new Float_t[DetDstNumber];
  DetDstPTime    = new Float_t[DetDstNumber];

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  MakeWfmArray();
  Initialize();
}


MTVetoFBAR::~MTVetoFBAR()
{
  delete[] DetModID;
  delete[] DetPSBit;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetSmoothness;
  delete[] vetoHitBuffer;

  delete[] DetDstModID;
  delete[] DetDstIntADC;
  delete[] DetDstEne;
  delete[] DetDstTime;
  delete[] DetDstPTime;
  delete   m_E14BP;
}


void MTVetoFBAR::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "FBARModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "FBARModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "FBARModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "FBARModuleEne", DetEne );
  if(m_clusterTree->GetBranch("FBARModulePSBit"))
	  m_clusterTree->SetBranchAddress( "FBARModulePSBit", DetPSBit );

  if(m_clusterTree->GetBranch("FBARModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "FBARModuleSmoothness", DetSmoothness );  
  }

  if( m_clusterTree->GetBranch("FBARNumber") ){
    CopyDstBranchFlag = true;
    m_clusterTree->SetBranchAddress( "FBARNumber",       &DetDstNumber);
    m_clusterTree->SetBranchAddress( "FBARModID",         DetDstModID );

    if(m_clusterTree->GetBranch("FBARIntegratedADC"))
    	m_clusterTree->SetBranchAddress( "FBARIntegratedADC", DetDstIntADC );
    m_clusterTree->SetBranchAddress( "FBAREne",           DetDstEne );
    m_clusterTree->SetBranchAddress( "FBARTime",          DetDstTime );
    m_clusterTree->SetBranchAddress( "FBARPTime",         DetDstPTime );
  }
  
  MTVeto::SetBranchAddress(tr);

}


void MTVetoFBAR::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "FBARModuleNumber", &DetNumber, "FBARModuleNumber/I");
  m_recTree->Branch( "FBARModuleModID", DetModID, "FBARModuleModID[FBARModuleNumber]/I");
  m_recTree->Branch( "FBARModuleEne", DetEne, "FBARModuleEne[FBARModuleNumber]/F");
  m_recTree->Branch( "FBARModuleTime", DetTime, "FBARModuleTime[FBARModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "FBARModuleSmoothness", DetSmoothness, "FBARModuleSmoothness[FBARModuleNumber]/F" );
  
  if(m_clusterTree->GetBranch("FBARModulePSBit"))
	  m_recTree->Branch( "FBARModulePSBit", DetPSBit, "FBARModulePSBit[FBARModuleNumber]/S");

  m_recTree->Branch( "FBARVetoModID", &FBARVetoModID,"FBARVetoModID/I");
  m_recTree->Branch( "FBARVetoEne", &FBARVetoEne,"FBARVetoEne/F");
  m_recTree->Branch( "FBARVetoTime", &FBARVetoTime,"FBARVetoTime/F");
  
  MTVeto::Branch(tr);
}

void MTVetoFBAR::BranchOriginalData( TTree* tr )
{

  if( CopyDstBranchFlag ){
    tr->Branch( "FBARNumber",       &DetDstNumber, "FBARNumber/I");
    tr->Branch( "FBARModID",         DetDstModID,  "FBARModID[FBARNumber]/I");
    if(m_recTree->GetBranch("FBARIntegratedADC"))
		tr->Branch( "FBARIntegratedADC", DetDstIntADC, "FBARIntegratedADC[FBARNumber]/F" );
    tr->Branch( "FBAREne",           DetDstEne,    "FBAREne[FBARNumber]/F" );
    tr->Branch( "FBARTime",          DetDstTime,   "FBARTime[FBARNumber]/F" );
    tr->Branch( "FBARPTime",         DetDstPTime,  "FBARPTime[FBARNumber]/F" );
  }
  
}


void MTVetoFBAR::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::FBAR))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }

  for( int imod=0; imod<MTBP::FBARNModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  FBARVetoModID = -1;
  FBARVetoEne = 0;
  FBARVetoTime = 9999.;

}


bool MTVetoFBAR::Process( int mode ){

  Initialize();

  static const double s_CSIZPosition  = GetDetZPosition("CSI");
  static const double s_FBARZPosition = GetDetZPosition("FBAR");
  static const std::vector<int> s_DeadModuleIDVec = GetDeadModuleIDVec("FBAR");

  // Front
  for( int imod=0; imod<DetNumber; imod++ ){

    //vetoTime = DetTime[imod] - m_eventStartTime;
    vetoTime = DetTime[imod] - m_eventStartTime - (s_CSIZPosition - m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }

    // dead channel treatment
    Bool_t DeadFlag=false;
    for(int ideadmod=0;ideadmod<s_DeadModuleIDVec.size();ideadmod++){
      if( DetModID[imod]==s_DeadModuleIDVec.at(ideadmod) ){
	DeadFlag=true;
	break;
      }
    }
    if( DeadFlag ){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }

  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );

  if(vetoHitBuffer[0].energy>0){
    FBARVetoModID = vetoHitBuffer[0].modID;
    FBARVetoEne   = vetoHitBuffer[0].energy;
    FBARVetoTime  = vetoHitBuffer[0].time;
  } else{
    FBARVetoModID = -1;
    FBARVetoEne   = 0;
    FBARVetoTime  = -9999.;
  }
  
  if( FBARVetoEne > m_vetoThreshold ) return true;
  return false;

}

double MTVetoFBAR::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoFBAR::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
