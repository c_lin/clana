#include <cmath>
#include <unistd.h>
#include "TString.h"
#include "MTAnalysisLibrary/MTClockShiftCorrection.h"

MTClockShiftCorrection::MTClockShiftCorrection(int RunID) 
{ 
  std::string dirName
    = std::string( std::getenv(MTBP::EnvName.c_str()) )+"/AnalysisLibrary/LegacyProjects/MTAnalysisLibrary";
  tr = new TChain("Tree");
  //tr->Add(Form(Form("%s/data/obsolete/clockshift_run49/TimeShiftCorrect%d.root",dirName.c_str(),RunID));
  tr->Add(Form("%s/data/clockshift_run62/TimeShiftCorrect%d.root",dirName.c_str(),RunID));
  
  SetBranchAddress();

  Nevent = tr->GetEntries();
  tr->GetEntry(0);
  
  for(int i=0; i<2716; i++) CorrectFlag[i]=0;
  for(int i=0; i<NCH; i++){
    CorrectFlag[CHID[i]]=1;
  }
  
  ClockShift = new double [2716];
  for( int i=0; i<2716; i++) ClockShift[i]=0;

  AddCorrect = new double [2716];  
  for(int i=0; i<2716; i++) AddCorrect[i]=0;
  //AddCorrect[137]=1;   //wrong sign was corrected at 18th Jul, 2014 (by MY)
  //AddCorrect[185]=-0.5;//wrong sign was corrected at 18th Jul, 2014 (by MY)
  //AddCorrect[1282]=1;  //wrong sign was corrected at 18th Jul, 2014 (by MY)  
}

MTClockShiftCorrection::~MTClockShiftCorrection()
{
  delete tr;
  delete ClockShift;
  delete AddCorrect;
}

void MTClockShiftCorrection::SetBranchAddress()
{
  tr->SetBranchAddress( "NCH", &NCH);
  tr->SetBranchAddress( "CHID", CHID );
  tr->SetBranchAddress( "TimeShift", ShiftVal );
}

double MTClockShiftCorrection::GetCorrection( int csiID ) const
{
  if( CorrectFlag[csiID]==1 )
    return ClockShift[csiID];
  else
    return 0;
}

double MTClockShiftCorrection::Correction( int csiID, double time )
{
  return time - GetCorrection(csiID) + AddCorrect[csiID];
}

bool MTClockShiftCorrection::SetCorrectionFactor( int spillNo )
{
  if(spillNo<Nevent){
    tr->GetEntry(spillNo);
    for(int i=0; i<NCH; i++)
      ClockShift[CHID[i]] = ShiftVal[i];
  }else{
    return false;
  }
  return true;
}
