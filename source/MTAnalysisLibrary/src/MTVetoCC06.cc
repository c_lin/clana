#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoCC06.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoCC06::MTVetoCC06( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "CC06"; 
  DetNumber = MTBP::CC06NModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();

  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 


  E391DetNumber = MTBP::CC06E391NModules;
  E391DetModID = new Int_t [E391DetNumber]();
  E391DetTime = new Float_t [E391DetNumber]();
  E391DetEne = new Float_t [E391DetNumber]();
  E391DetSmoothness = new Float_t [E391DetNumber]();
  E391DetAreaR = new Float_t [E391DetNumber]();
  E391DetWfmCorrectNumber = new short[E391DetNumber]();
  E391DetPSBit = new Short_t [E391DetNumber]();

  ScintiDetNumber = MTBP::CC06ScintiNModules;
  ScintiDetModID = new Int_t [ScintiDetNumber]();
  ScintiDetTime = new Float_t [ScintiDetNumber]();
  ScintiDetEne = new Float_t [ScintiDetNumber]();
  ScintiDetSmoothness = new Float_t [ScintiDetNumber]();
  ScintiDetAreaR = new Float_t [ScintiDetNumber]();
  ScintiDetWfmCorrectNumber = new short [ScintiDetNumber]();
  ScintiDetPSBit = new Short_t [ScintiDetNumber]();
  
  vetoHitBuffer = new pVetoHit [DetNumber]();

  DetWfmCorrupted = NULL;
  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  MakeWfmArray();

  //Initialize();
}


MTVetoCC06::~MTVetoCC06()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetPSBit;
  delete[] DetSmoothness;

  delete[] E391DetModID;
  delete[] E391DetTime;
  delete[] E391DetEne;
  delete[] E391DetSmoothness;
  delete[] E391DetAreaR;
  delete[] E391DetWfmCorrectNumber;
  delete[] E391DetPSBit;

  delete[] ScintiDetModID;
  delete[] ScintiDetTime;
  delete[] ScintiDetEne;
  delete[] ScintiDetSmoothness;
  delete[] ScintiDetAreaR;
  delete[] ScintiDetWfmCorrectNumber;
  delete[] ScintiDetPSBit;
  
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoCC06::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "CC06ModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "CC06ModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "CC06ModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "CC06ModuleEne", DetEne );
  if(m_clusterTree->GetBranch("CC06ModulePSBit"))
	  m_clusterTree->SetBranchAddress( "CC06ModulePSBit", DetPSBit );

  if(m_clusterTree->GetBranch("CC06ModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "CC06ModuleSmoothness", DetSmoothness );  
  }
  MTVeto::SetBranchAddress(tr);
}


void MTVetoCC06::Branch( TTree* tr )
{
  m_recTree = tr;
  
  //m_recTree->Branch( "CC06ModuleNumber", &DetNumber, "CC06ModuleNumber/I");
  //m_recTree->Branch( "CC06ModuleModID", DetModID, "CC06ModuleModID[CC06ModuleNumber]/I");
  //m_recTree->Branch( "CC06ModuleEne", DetEne, "CC06ModuleEne[CC06ModuleNumber]/D");
  //m_recTree->Branch( "CC06ModuleTime", DetTime, "CC06ModuleTime[CC06ModuleNumber]/D");

  m_recTree->Branch( "CC06E391ModuleNumber", &E391DetNumber, "CC06E391ModuleNumber/I");
  m_recTree->Branch( "CC06E391ModuleModID", E391DetModID, "CC06E391ModuleModID[CC06E391ModuleNumber]/I");
  m_recTree->Branch( "CC06E391ModuleEne", E391DetEne, "CC06E391ModuleEne[CC06E391ModuleNumber]/F");
  m_recTree->Branch( "CC06E391ModuleTime", E391DetTime, "CC06E391ModuleTime[CC06E391ModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "CC06E391ModuleSmoothness", E391DetSmoothness, "CC06E391ModuleSmoothness[CC06E391ModuleNumber]/F");
  if(m_clusterTree->GetBranch("CC06ModulePSBit"))
	  m_recTree->Branch( "CC06E391ModulePSBit", E391DetPSBit, "CC06E391ModulePSBit[CC06E391ModuleNumber]/S");
  if(m_clusterTree->GetBranch("CC06ModuleAreaR")!=0 ){
	  tr->Branch( "CC06E391ModuleAreaR", E391DetAreaR, "CC06E391ModuleAreaR[CC06E391ModuleNumber]");
  }
  if(m_clusterTree->GetBranch("CC06ModuleWfmCorrectNumber")!=0 ){
	  tr->Branch( "CC06E391ModuleWfmCorrectNumber", E391DetWfmCorrectNumber, "CC06E391ModuleWfmCorrectNumber[CC06E391ModuleNumber]/S");
  }


  m_recTree->Branch( "CC06ScintiModuleNumber", &ScintiDetNumber, "CC06ScintiModuleNumber/I");
  m_recTree->Branch( "CC06ScintiModuleModID", ScintiDetModID, "CC06ScintiModuleModID[CC06ScintiModuleNumber]/I");
  m_recTree->Branch( "CC06ScintiModuleEne", ScintiDetEne, "CC06ScintiModuleEne[CC06ScintiModuleNumber]/F");
  m_recTree->Branch( "CC06ScintiModuleTime", ScintiDetTime, "CC06ScintiModuleTime[CC06ScintiModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "CC06ScintiModuleSmoothness", ScintiDetSmoothness, "CC06ScintiModuleSmoothness[CC06ScintiModuleNumber]/F");
  if(m_clusterTree->GetBranch("CC06ModulePSBit"))
	  m_recTree->Branch( "CC06ScintiModulePSBit", ScintiDetPSBit, "CC06ScintiModulePSBit[CC06ScintiModuleNumber]/S");
  if(m_clusterTree->GetBranch("CC06ModuleAreaR")!=0 ){
	  tr->Branch( "CC06ScintiModuleAreaR", ScintiDetAreaR, "CC06ScintiModuleAreaR[CC06ScintiModuleNumber]");
  }
  if(m_clusterTree->GetBranch("CC06WfmCorrectNumber")!=0 ){
	  tr->Branch( "CC06ScintiModuleWfmCorrectNumber", ScintiDetWfmCorrectNumber, "CC06ScintiModuleWfmCorrectNumber[CC06ScintiModuleNumber]/S");
  }
  
  //m_recTree->Branch( "CC06VetoModID", &CC06VetoModID,"CC06VetoModID/I");
  //m_recTree->Branch( "CC06VetoEne", &CC06VetoEne,"CC06VetoEne/D");
  //m_recTree->Branch( "CC06VetoTime", &CC06VetoTime,"CC06VetoTime/D");

  m_recTree->Branch( "CC06E391VetoModID", &CC06E391VetoModID,"CC06E391VetoModID/I");
  m_recTree->Branch( "CC06E391VetoEne", &CC06E391VetoEne,"CC06E391VetoEne/F");
  m_recTree->Branch( "CC06E391VetoTime", &CC06E391VetoTime,"CC06E391VetoTime/F");

  m_recTree->Branch( "CC06ScintiVetoModID", &CC06ScintiVetoModID,"CC06ScintiVetoModID/I");
  m_recTree->Branch( "CC06ScintiVetoEne", &CC06ScintiVetoEne,"CC06ScintiVetoEne/F");
  m_recTree->Branch( "CC06ScintiVetoTime", &CC06ScintiVetoTime,"CC06ScintiVetoTime/F");
  MTVeto::Branch(tr);
  
}


void MTVetoCC06::Initialize( void )
{

  E391DetNumber=0;
  ScintiDetNumber=0;
 
  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::CC06))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  } 
  for( int imod=0; imod<MTBP::CC06NModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  for(int i=0; i<DetNumber; i++){
    if(DetModID[i]<60){
      E391DetModID[E391DetNumber]=DetModID[i];
      E391DetEne[E391DetNumber]=DetEne[i];
      E391DetTime[E391DetNumber]=DetTime[i];
      E391DetPSBit[E391DetNumber]=DetPSBit[i];
      E391DetSmoothness[E391DetNumber]=DetSmoothness[i];
      E391DetAreaR[E391DetNumber]=DetAreaR[i];
      E391DetWfmCorrectNumber[E391DetNumber]=DetWfmCorrectNumber[i];
      E391DetNumber+=1;
    }
    else {
      ScintiDetModID[ScintiDetNumber]=DetModID[i];
      ScintiDetEne[ScintiDetNumber]=DetEne[i];
      ScintiDetTime[ScintiDetNumber]=DetTime[i];
      ScintiDetSmoothness[ScintiDetNumber]=DetSmoothness[i];
      ScintiDetAreaR[ScintiDetNumber]=DetAreaR[i];
      ScintiDetWfmCorrectNumber[ScintiDetNumber]=DetWfmCorrectNumber[i];
      ScintiDetPSBit[ScintiDetNumber]=DetPSBit[i];
      ScintiDetNumber+=1;
    }
  }
  
  CC06VetoModID = -1;
  CC06VetoEne = 0;
  CC06VetoTime = 9999.;

  CC06E391VetoModID = -1;
  CC06E391VetoEne = 0;
  CC06E391VetoTime = 9999.;

  CC06ScintiVetoModID = -1;
  CC06ScintiVetoEne = 0;
  CC06ScintiVetoTime = 9999.;
}


bool MTVetoCC06::Process( int mode ){

  Initialize();

  static const double s_CC06ZPosition = GetDetZPosition("CC06");
     
  for( int imod=0; imod<E391DetNumber; imod++ ){
    vetoTime = E391DetTime[imod] - m_eventStartTime-(s_CC06ZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = E391DetModID[imod];
      vetoHitBuffer[imod].energy = E391DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
    }else{
      vetoHitBuffer[imod].modID = E391DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }
    //std::cout<<imod<<" "<<E391DetModID[imod]<<" "<<E391DetEne[imod]<<" "<<vetoTime<<std::endl;
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[E391DetNumber]), pVetoHit::largerHit );

  if( vetoHitBuffer[0].energy>0 && E391DetNumber>0){  
    CC06E391VetoModID = vetoHitBuffer[0].modID;
    CC06E391VetoEne   = vetoHitBuffer[0].energy;
    CC06E391VetoTime  = vetoHitBuffer[0].time;
  } else{
    CC06E391VetoModID = -1;
    CC06E391VetoEne   = 0;
    CC06E391VetoTime  = -9999.;
  }
  
  int crystalnum=E391DetNumber;
  
  for( int imod=0; imod<ScintiDetNumber; imod++ ){
    vetoTime = ScintiDetTime[imod] - m_eventStartTime-(s_CC06ZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[crystalnum+imod].modID = ScintiDetModID[imod];
      vetoHitBuffer[crystalnum+imod].energy = ScintiDetEne[imod];
      vetoHitBuffer[crystalnum+imod].time = vetoTime;
    }else{
      vetoHitBuffer[crystalnum+imod].modID = ScintiDetModID[imod];
      vetoHitBuffer[crystalnum+imod].energy = 0;
      vetoHitBuffer[crystalnum+imod].time = vetoTime;
    }
    //std::cout<<imod<<" "<<ScintiDetModID[imod]<<" "<<ScintiDetEne[imod]<<" "<<vetoTime<<std::endl;
  }
  std::sort( &(vetoHitBuffer[crystalnum]), &(vetoHitBuffer[crystalnum+ScintiDetNumber]), pVetoHit::largerHit );

  if( vetoHitBuffer[crystalnum].energy>0 && ScintiDetNumber>0){  
    CC06ScintiVetoModID = vetoHitBuffer[crystalnum].modID;
    CC06ScintiVetoEne   = vetoHitBuffer[crystalnum].energy;
    CC06ScintiVetoTime  = vetoHitBuffer[crystalnum].time;
  } else{
    CC06ScintiVetoModID = -1;
    CC06ScintiVetoEne   = 0;
    CC06ScintiVetoTime  = -9999.;
  }
  

  //std::cout<<"CC06 :"<<vetoHitBuffer[0].modID<<" "<<vetoHitBuffer[0].energy<<" "<<vetoHitBuffer[0].time<<std::endl;
  //std::cout<<"CC06 :"<<vetoHitBuffer[crystalnum].modID<<" "<<vetoHitBuffer[crystalnum].energy<<" "<<vetoHitBuffer[crystalnum].time<<std::endl;  
  //std::getchar();

  if( CC06E391VetoEne > m_vetoThreshold ) return true;
  if( CC06ScintiVetoEne > 1 ) return true;
  
  return false;

}


double MTVetoCC06::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoCC06::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
