#include <sstream>

#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "MTAnalysisLibrary/MTVeto.h"




MTVeto::MTVeto( void )
{
  SetVetoWindow( 1000., 250. );
  m_vetoThreshold = 1.;

}


void MTVeto::MakeWfmArray(){
  DetSuppWfm125       = new Float_t[DetNumber][MTBP::nSample125];
  DetSuppWfmModID = new Int_t[DetNumber];
  DetSuppWfmNumber = 0;
  DetAreaR = new Float_t[DetNumber];
  DetWfmCorrectNumber = new short[DetNumber];
}


MTVeto::~MTVeto( void ){	  
	delete[] DetSuppWfm125;
	delete[] DetAreaR;
	delete[] DetWfmCorrectNumber;
}

void MTVeto::Initialize( void )
{
  m_eventStartZ = 0;
  m_eventStartTime = 0;
}

void MTVeto::SetBranchAddress( TTree* tr )
{    

  if(m_clusterTree->GetBranch(Form("%sAreaR",DetName.c_str()))!=0 ){
     m_clusterTree->SetBranchAddress(Form( "%sAreaR", DetName.c_str() ), DetAreaR);
  }else if(m_clusterTree->GetBranch(Form("%sModuleAreaR",DetName.c_str()))!=0 ){
     m_clusterTree->SetBranchAddress(Form( "%sModuleAreaR", DetName.c_str() ), DetAreaR);
  }

  if(m_clusterTree->GetBranch(Form("%sWfmCorrectNumber",DetName.c_str()))!=0 ){
	 m_clusterTree->SetBranchAddress(Form( "%sWfmCorrectNumber", DetName.c_str() ), DetWfmCorrectNumber);
  }else if(m_clusterTree->GetBranch(Form("%sModuleWfmCorrectNumber",DetName.c_str()))!=0 ){
     m_clusterTree->SetBranchAddress(Form( "%sModuleWfmCorrectNumber", DetName.c_str() ), DetWfmCorrectNumber);
  }


  if(m_clusterTree->GetBranch(Form("%sWfm",DetName.c_str()))!=0 ){
     m_clusterTree->SetBranchAddress(Form( "%sWfmNumber", DetName.c_str() ), &DetSuppWfmNumber);
     m_clusterTree->SetBranchAddress(Form( "%sWfmModID",  DetName.c_str() ),  DetSuppWfmModID);
     m_clusterTree->SetBranchAddress(Form( "%sWfm",  DetName.c_str() ),  DetSuppWfm125);
  }

} 
void MTVeto::Branch( TTree* tr ){

	
  if(DetName!="CC05" && DetName!="CC06" && DetName!="CC04"){
  	if(m_clusterTree->GetBranch(Form("%sModuleAreaR",DetName.c_str()))!=0 ){
  	    tr->Branch( Form( "%sModuleAreaR", DetName.c_str() ), DetAreaR, Form( "%sModuleAreaR[%sModuleNumber]",  DetName.c_str(), DetName.c_str() ));
  	}else if(m_clusterTree->GetBranch(Form("%sAreaR",DetName.c_str()))!=0 ){
  	    tr->Branch( Form( "%sAreaR", DetName.c_str() ), DetAreaR, Form( "%sAreaR[%sNumber]",  DetName.c_str(), DetName.c_str() ));
  	}

	if(m_clusterTree->GetBranch(Form("%sModuleWfmCorrectNumber",DetName.c_str()))!=0 ){
  	    tr->Branch( Form( "%sModuleWfmCorrectNumber", DetName.c_str() ), DetWfmCorrectNumber, Form( "%sModuleWfmCorrectNumber[%sModuleNumber]/S",  DetName.c_str(), DetName.c_str() ));
  	}else if(m_clusterTree->GetBranch(Form("%sWfmCorrectNumber",DetName.c_str()))!=0 ){
  	    tr->Branch( Form( "%sWfmCorrectNumber", DetName.c_str() ), DetWfmCorrectNumber, Form( "%sWfmCorrectNumber[%sNumber]/S",  DetName.c_str(), DetName.c_str() ));
  	}

  }

  if(m_clusterTree->GetBranch( Form( "%sWfm",   DetName.c_str() ))){
    tr->Branch( Form( "%sWfmNumber", DetName.c_str() ), &DetSuppWfmNumber, Form( "%sWfmNumber/I",  DetName.c_str() ));
	tr->Branch( Form( "%sWfmModID", DetName.c_str() ),   DetSuppWfmModID,  Form( "%sWfmModID[%sWfmNumber]/I",  DetName.c_str(), DetName.c_str() ));
	tr->Branch( Form( "%sWfm",   DetName.c_str() ),      DetSuppWfm125,    Form( "%sWfm[%sWfmNumber][%d]", DetName.c_str(), DetName.c_str(), MTBP::nSample125 ));
  }
}


void MTVeto::SetEventStartInfo( double eventStartZ, double eventStartTime )
{
  SetEventStartZ( eventStartZ );
  SetEventStartTime( eventStartTime );
}

void MTVeto::SetEventStartZ( double z )
{
  m_eventStartZ = z;
}

double MTVeto::GetEventStartZ( void )
{
  return m_eventStartZ;
}

void MTVeto::SetEventStartTime( double t )
{
  m_eventStartTime = t;
}

double MTVeto::GetEventStartTime( void )
{
  return m_eventStartTime;
}

void MTVeto::SetVetoWindow( double windowLength, double windowOffset )
{
  m_vetoWindowLength = windowLength;
  m_vetoWindowOffset = windowOffset;
  m_vetoStartTime = windowOffset - windowLength/2.;
  m_vetoEndTime = windowOffset + windowLength/2.;
}

void MTVeto::GetVetoWindow( double& windowLength, double& windowOffset )
{
  windowLength = m_vetoWindowLength;
  windowOffset = m_vetoWindowOffset;
}

double MTVeto::GetVetoStartTime( void )
{
  return m_vetoStartTime;
}

double MTVeto::GetVetoEndTime( void )
{
  return m_vetoEndTime;
}


void MTVeto::SetVetoThreshold( double eThreshold )
{
  m_vetoThreshold = eThreshold;
}

void MTVeto::SetLooseVetoThreshold( double eThreshold )
{
  m_looseVetoThreshold = eThreshold;
}

void MTVeto::SetTightVetoThreshold( double eThreshold )
{
  m_tightVetoThreshold = eThreshold;
}

double MTVeto::GetVetoThreshold( void )
{
  return m_vetoThreshold;
}

double MTVeto::GetLooseVetoThreshold( void )
{
  return m_looseVetoThreshold;
}

double MTVeto::GetTightVetoThreshold( void )
{
  return m_tightVetoThreshold;
}


bool MTVeto::IsInVetoWindow( double t )
{
  if( t < m_vetoStartTime ) return false;
  if( m_vetoEndTime < t ) return false;
  return true;
}

void MTVeto::SetTimeOffset(double offset)
{
  for(int i=0; i<DetNumber; i++){
    DetTime[i]=DetTime[i]+offset;
  }
}

