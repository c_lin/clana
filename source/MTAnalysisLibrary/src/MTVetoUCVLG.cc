#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoUCVLG.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoUCVLG::MTVetoUCVLG( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "UCVLG"; 
  DetNumber = MTBP::UCVLGNModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();

  DetWfmCorrupted = NULL;
  vetoHitBuffer = new pVetoHit [DetNumber]();

  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  MakeWfmArray();
  Initialize();
}


MTVetoUCVLG::~MTVetoUCVLG()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetPSBit;
  delete[] DetSmoothness;
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoUCVLG::SetBranchAddress( TTree* tr )
{    
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "UCVLGModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "UCVLGModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "UCVLGModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "UCVLGModuleEne", DetEne );
  
  
  if(m_clusterTree->GetBranch("UCVLGModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "UCVLGModuleSmoothness", DetSmoothness );  
  }
  if(m_clusterTree->GetBranch("UCVLGModulePSBit"))
	  m_clusterTree->SetBranchAddress( "UCVLGModulePSBit", DetPSBit );

  MTVeto::SetBranchAddress(tr);
}


void MTVetoUCVLG::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "UCVLGModuleNumber", &DetNumber, "UCVLGModuleNumber/I");
  m_recTree->Branch( "UCVLGModuleModID", DetModID, "UCVLGModuleModID[UCVLGModuleNumber]/I");
  m_recTree->Branch( "UCVLGModuleEne", DetEne, "UCVLGModuleEne[UCVLGModuleNumber]/F");
  m_recTree->Branch( "UCVLGModuleTime", DetTime, "UCVLGModuleTime[UCVLGModuleNumber]/F");
  
    
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "UCVLGModuleSmoothness", DetSmoothness, "UCVLGSmoothness[UCVLGModuleNumber]/F" );
  if(m_clusterTree->GetBranch("UCVLGModulePSBit"))
	  m_recTree->Branch( "UCVLGModulePSBit", DetPSBit, "UCVLGModulePSBit[UCVLGModuleNumber]/S");

  m_recTree->Branch( "UCVLGVetoModID", &UCVLGVetoModID,"UCVLGVetoModID/I");
  m_recTree->Branch( "UCVLGVetoEne", &UCVLGVetoEne,"UCVLGVetoEne/F");
  m_recTree->Branch( "UCVLGVetoTime", &UCVLGVetoTime,"UCVLGVetoTime/F");

  MTVeto::Branch(tr);
}


void MTVetoUCVLG::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::UCVLG))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  for( int imod=0; imod<MTBP::UCVLGNModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  UCVLGVetoModID = -1;
  UCVLGVetoEne = 0;
  UCVLGVetoTime = 9999.;
  UCVLGTotalVetoEne=0;
}


bool MTVetoUCVLG::Process( int mode ){

    Initialize();

    static const double s_UCVLGZPosition = GetDetZPosition("UCVLG");
	static const double s_CSIZPosition  = GetDetZPosition("CSI");

  // Front
  for( int imod=0; imod<DetNumber; imod++ ){
    vetoTime = DetTime[imod] - m_eventStartTime - (s_CSIZPosition - m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
      UCVLGTotalVetoEne+=DetEne[imod];
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }

  }
  
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );
  if(vetoHitBuffer[0].energy>0){
    UCVLGVetoModID = vetoHitBuffer[0].modID;
    UCVLGVetoEne   = vetoHitBuffer[0].energy;
    UCVLGVetoTime  = vetoHitBuffer[0].time;
  } else {
    UCVLGVetoModID = -1;
    UCVLGVetoEne   = 0;
    UCVLGVetoTime  = -9999.;
  }


  


  if( UCVLGVetoEne > m_vetoThreshold ) return true;

  return false;

}


double MTVetoUCVLG::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoUCVLG::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
