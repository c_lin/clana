#include "MTAnalysisLibrary/MTPulseGenerator.h"
#include <cmath>
#include <algorithm>


double* MTPulseGenerator::originalPulseShape = new double[MTPulseGenerator::nPoints]();

const double MTPulseGenerator::peakConstantDiff = 30.6098; // [ns]
const double MTPulseGenerator::timePitch = 0.01; // [ns]


MTPulseGenerator::MTPulseGenerator()
{

  nQuantizedPoints = (int)(nPoints*timePitch/8);

  GenerateOriginalPulseShape();
  pulseShape = new double[nPoints]();
  quantizedPulseShape = new int[nQuantizedPoints]();
}

MTPulseGenerator::~MTPulseGenerator()
{
  delete [] pulseShape;
  delete [] quantizedPulseShape;
}


void MTPulseGenerator::GenerateOriginalPulseShape( void )
{

  double height = 1;
  double mean = 0;
  double sigma = 27.896;
  double asymmetric = 0.06202;
  double offset = 0;
  for( int ipoint=0; ipoint<nPoints; ipoint++ ){
    double x = (ipoint-nPoints/2) * timePitch;
    x -= peakConstantDiff;
    originalPulseShape[ipoint] = height * exp( -0.5 * pow( (x-mean)/(asymmetric*(x-mean)+sigma), 2) ) + offset;
  }
}


void MTPulseGenerator::SetEnergyTime( int n, double* e, double* t )
{
  m_etVec.clear();
  
  pEnergyTime et;
  for( int index=0; index<n; index++ ){
    et.energy = e[index];
    et.time   = t[index];
    m_etVec.push_back( et );
  }
}


void MTPulseGenerator::SetEnergyTime( int n, pEnergyTime* et )
{
  m_etVec.clear();

  for( int index=0; index<n; index++ ){
    m_etVec.push_back( et[index] );
  }
}


void MTPulseGenerator::SetEnergyTime( std::vector< pEnergyTime > etVec )
{
  m_etVec = etVec;
}


double MTPulseGenerator::GetPseudoIntegratedEnergy( double minTime, double maxTime )
{
  std::sort( m_etVec.begin(), m_etVec.end(), pEnergyTime::fasterHit );

  double integratedEnergy = 0;
  for( unsigned int index=0; index<m_etVec.size(); index++ ){
    if( maxTime > minTime ){
      if( minTime < m_etVec[index].time && m_etVec[index].time < maxTime ){
	integratedEnergy += m_etVec[index].energy;
      }
    }else{
      integratedEnergy += m_etVec[index].energy;
    }
  }

  return integratedEnergy;
}


double MTPulseGenerator::GetPseudoConstantFractionTime( double minTime, double maxTime )
{
  double threshold = GetPseudoIntegratedEnergy( minTime, maxTime ) / 2.;

  double tempEnergy = 0;
  for( unsigned int index=0; index<m_etVec.size(); index++ ){
    if( maxTime > minTime ){
      if( minTime < m_etVec[index].time && m_etVec[index].time < maxTime ){
	tempEnergy += m_etVec[index].energy;
      }
    }else{
      tempEnergy += m_etVec[index].energy;
    }

    if( tempEnergy >= threshold ){
      return m_etVec[index].time;
    }
  }

  return 0;
}

  

void MTPulseGenerator::GeneratePulseShape( void )
{
  // Initialize
  for( int ipoint=0; ipoint<nPoints; ipoint++ ){
    pulseShape[ipoint] = 0;
  }

  for( unsigned int ihit=0; ihit<m_etVec.size(); ihit++ ){
    int timeOffsetPoint = (int)floor(m_etVec[ihit].time / timePitch); // [ns] -> [point]
    for( int ipoint=0; ipoint<nPoints; ipoint++ ){
      if( -1<ipoint-timeOffsetPoint && ipoint-timeOffsetPoint<nPoints ){
	pulseShape[ipoint] += originalPulseShape[ipoint-timeOffsetPoint] * m_etVec[ihit].energy;
      }
    }
  }

  
  // Quantized pulse shape
  for( int ipoint=0; ipoint<nQuantizedPoints; ipoint++ ){
    quantizedPulseShape[ipoint] = pulseShape[ipoint*(nPoints/nQuantizedPoints)];
  }

}
		   


double MTPulseGenerator::GetPeakTime( void ) const
{
  int peakPoint = 0;
  double peakHeight = pulseShape[peakPoint];

  for( int ipoint=0; ipoint<nPoints; ipoint++ ){
    if( peakHeight < pulseShape[ipoint] ){
      peakPoint = ipoint;
      peakHeight = pulseShape[ipoint];
    }
  }

  return (peakPoint-nPoints/2) * timePitch; // [ns]
}


double MTPulseGenerator::GetConstantFractionTime( void ) const
{
  int peakPoint = 0;
  double peakHeight = pulseShape[peakPoint];

  for( int ipoint=0; ipoint<nPoints; ipoint++ ){
    if( peakHeight < pulseShape[ipoint] ){
      peakPoint = ipoint;
      peakHeight = pulseShape[ipoint];
    }
  }

  // reverse search from highest point
  double threshold = peakHeight / 2.;
  for( int ipoint=peakPoint; ipoint>0; ipoint-- ){
    if( pulseShape[ipoint-1]<threshold && threshold<=pulseShape[ipoint] ){
      return ( (threshold-pulseShape[ipoint-1]) / (pulseShape[ipoint]-pulseShape[ipoint-1] ) +ipoint-1-nPoints/2 ) * timePitch;
    }
  }

  return -1;
}



double MTPulseGenerator::GetPeakHeight( void ) const
{
  double peakHeight = pulseShape[0];

  for( int ipoint=0; ipoint<nPoints; ipoint++ ){
    if( peakHeight < pulseShape[ipoint] ){
      peakHeight = pulseShape[ipoint];
    }
  }
  return peakHeight;
}


double MTPulseGenerator::GetIntegrated( void ) const
{
  double area = 0;
  for( int ipoint=0; ipoint<nPoints; ipoint++ ){
    area += pulseShape[ipoint];
  }
  return area / 7074.77;
}



double MTPulseGenerator::GetQuantizedPeakTime( void ) const
{
  int peakPoint = 0;
  double peakHeight = quantizedPulseShape[peakPoint];

  for( int ipoint=0; ipoint<nQuantizedPoints; ipoint++ ){
    if( peakHeight < quantizedPulseShape[ipoint] ){
      peakPoint = ipoint;
      peakHeight = quantizedPulseShape[ipoint];
    }
  }

  return (peakPoint-nQuantizedPoints/2) * 8.; // [ns]

}



double MTPulseGenerator::GetQuantizedConstantFractionTime( void ) const
{
  int peakPoint = 0;
  double peakHeight = quantizedPulseShape[peakPoint];

  for( int ipoint=0; ipoint<nQuantizedPoints; ipoint++ ){
    if( peakHeight < quantizedPulseShape[ipoint] ){
      peakPoint = ipoint;
      peakHeight = quantizedPulseShape[ipoint];
    }
  }

  // reverse search from highest point
  double threshold = peakHeight / 2.;
  for( int ipoint=peakPoint; ipoint>0; ipoint-- ){
    if( quantizedPulseShape[ipoint-1]<threshold && threshold<=quantizedPulseShape[ipoint] ){
      return ( (threshold-quantizedPulseShape[ipoint-1]) / (quantizedPulseShape[ipoint]-quantizedPulseShape[ipoint-1] ) +ipoint-1-nQuantizedPoints/2 ) * 8;
    }
  }

  return -1;

}




double MTPulseGenerator::GetQuantizedPeakHeight( void ) const
{
  double peakHeight = quantizedPulseShape[0];

  for( int ipoint=0; ipoint<nQuantizedPoints; ipoint++ ){
    if( peakHeight < quantizedPulseShape[ipoint] ){
      peakHeight = quantizedPulseShape[ipoint];
    }
  }

  return peakHeight;

}



double MTPulseGenerator::GetQuantizedIntegrated( void ) const
{
  double area = 0;

  for( int ipoint=0; ipoint<nQuantizedPoints; ipoint++ ){
    area += quantizedPulseShape[ipoint];
  }

  return area/7074.77 * nPoints/nQuantizedPoints;

}
