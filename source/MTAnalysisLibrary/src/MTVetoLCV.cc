#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoLCV.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoLCV::MTVetoLCV( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "LCV"; 
  DetNumber = MTBP::LCVNModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();

  DetWfmCorrupted = NULL;
  vetoHitBuffer = new pVetoHit [DetNumber]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  Initialize();
  MakeWfmArray();
}


MTVetoLCV::~MTVetoLCV()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetSmoothness;
  delete[] DetPSBit;
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoLCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "LCVModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "LCVModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "LCVModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "LCVModuleEne", DetEne );
  if(m_clusterTree->GetBranch("LCVModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "LCVModuleSmoothness", DetSmoothness );  
  }
  if(m_clusterTree->GetBranch("LCVModulePSBit"))
	  m_clusterTree->SetBranchAddress( "LCVModulePSBit", DetPSBit );
  MTVeto::SetBranchAddress(tr);
}


void MTVetoLCV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "LCVModuleNumber", &DetNumber, "LCVModuleNumber/I");
  m_recTree->Branch( "LCVModuleModID", DetModID, "LCVModuleModID[LCVModuleNumber]/I");
  m_recTree->Branch( "LCVModuleEne", DetEne, "LCVModuleEne[LCVModuleNumber]/F");
  m_recTree->Branch( "LCVModuleTime", DetTime, "LCVModuleTime[LCVModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "LCVModuleSmoothness", DetSmoothness, "LCVModuleSmoothness[LCVModuleNumber]/F" );
  if(m_clusterTree->GetBranch("LCVModulePSBit"))
	  m_recTree->Branch( "LCVModulePSBit", DetPSBit, "LCVModulePSBit[LCVModuleNumber]/S");

  m_recTree->Branch( "LCVVetoModID", &LCVVetoModID,"LCVVetoModID/I");
  m_recTree->Branch( "LCVVetoEne", &LCVVetoEne,"LCVVetoEne/F");
  m_recTree->Branch( "LCVVetoTime", &LCVVetoTime,"LCVVetoTime/F");
  
  MTVeto::Branch(tr);
}


void MTVetoLCV::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::LCV))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  for( int imod=0; imod<MTBP::LCVNModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  LCVVetoModID = -1;
  LCVVetoEne = 0;
  LCVVetoTime = 9999.;

}
  

bool MTVetoLCV::Process( int mode ){

  Initialize();

  static const double s_LCVZPosition = GetDetZPosition("LCV");

  // Front
  for( int imod=0; imod<DetNumber; imod++ ){
    //vetoTime = DetTime[imod] - m_eventStartTime;
    vetoTime = DetTime[imod] - m_eventStartTime-(s_LCVZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }
    //std::cout<<imod<<" "<<DetModID[imod]<<" "<<DetEne[imod]<<" "<<vetoTime<<std::endl;
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );

  if(vetoHitBuffer[0].energy>0){  
    LCVVetoModID = vetoHitBuffer[0].modID;
    LCVVetoEne   = vetoHitBuffer[0].energy;
    LCVVetoTime  = vetoHitBuffer[0].time;
  } else {
    LCVVetoModID = -1;
    LCVVetoEne   = 0;
    LCVVetoTime  = -9999.;
  }

  //std::cout<<"LCV :"<<vetoHitBuffer[0].modID<<" "<<vetoHitBuffer[0].energy<<" "<<vetoHitBuffer[0].time<<std::endl;
  //std::getchar();
  
  if( LCVVetoEne > m_vetoThreshold ) return true;

  return false;

}


double MTVetoLCV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoLCV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
