#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoBHCV.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"

MTVetoBHCV::MTVetoBHCV( Int_t userFlag ) : m_userFlag( userFlag )
{

  DetName = "BHCV"; 
  DetNumber = MTBP::BHCVNChannels;
  DetModID = new Int_t[DetNumber]();
  DetNHit = new Short_t[DetNumber]();
  DetEne   = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  DetTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();

  TOFCorrection = new Float_t[DetNumber];
  for( Int_t iMod=0 ; iMod<DetNumber ; iMod++ ){
    TOFCorrection[iMod] = MTBP::BHCVDeltaZ[iMod]/(TMath::C()*1e-6);
  }
  
  
  int maxhit=MTBP::NMaxHitPerEvent;
  MaxBufferNum= DetNumber*maxhit;  
  vetoHitBuffer = new pVetoHit [MaxBufferNum]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  MakeWfmArray();
  Initialize();
}


MTVetoBHCV::~MTVetoBHCV()
{

  delete[] DetModID;
  delete[] DetNHit;
  delete[] DetEne;  
  delete[] DetTime;
  delete[] TOFCorrection;
  
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoBHCV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "BHCVNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "BHCVModID", DetModID );
  m_clusterTree->SetBranchAddress( "BHCVnHits", DetNHit );
  m_clusterTree->SetBranchAddress( "BHCVTime", DetTime );
  m_clusterTree->SetBranchAddress( "BHCVEne", DetEne );
  MTBHVeto::SetBranchAddress(tr);
}


void MTVetoBHCV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "BHCVNumber", &DetNumber, "BHCVNumber/I");
  m_recTree->Branch( "BHCVModID", DetModID, "BHCVModID[BHCVNumber]/I");
  m_recTree->Branch( "BHCVnHits", DetNHit, "BHCVnHits[BHCVNumber]/S");
  std::ostringstream leafstr[2];
  leafstr[0] << "BHCVEne[BHCVNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  leafstr[1] << "BHCVTime[BHCVNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  m_recTree->Branch( "BHCVEne", DetEne, leafstr[0].str().c_str() );
  m_recTree->Branch( "BHCVTime", DetTime, leafstr[1].str().c_str() );


  m_recTree->Branch( "BHCVVetoModID", &BHCVVetoModID,"BHCVVetoModID/I");
  m_recTree->Branch( "BHCVVetoEne", &BHCVVetoEne,"BHCVVetoEne/F");
  m_recTree->Branch( "BHCVVetoTime", &BHCVVetoTime,"BHCVVetoTime/F");
  
  MTBHVeto::Branch(tr);
}


void MTVetoBHCV::Initialize( void )
{

  for( int imod=0; imod<MaxBufferNum; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  BHCVVetoModID = -1;
  BHCVVetoEne = 0;
  BHCVVetoTime = MTBP::Invalid;
}


bool MTVetoBHCV::Process( int mode )
{
  
  Initialize();
  
  static const double s_BHCVZPosition = GetDetZPosition("BHCV");
  
  int HitNum=0;
  for( int imod=0; imod<DetNumber; imod++ ){
    for(int ihit=0; ihit<DetNHit[imod]; ihit++){
      //vetoTime = DetTime[imod][ihit] - m_eventStartTime;
      //vetoTime = DetTime[imod][ihit] - m_eventStartTime-(MTBP::CSIZPosition-m_eventStartZ)/(TMath::C()/1e6) - TOFCorrection[imod];
      vetoTime = DetTime[imod][ihit] - m_eventStartTime-(s_BHCVZPosition-m_eventStartZ)/(TMath::C()/1e6) - TOFCorrection[imod];
      if( IsInVetoWindow( vetoTime )){
	vetoHitBuffer[HitNum].modID = DetModID[imod];
	vetoHitBuffer[HitNum].energy = DetEne[imod][ihit];
	vetoHitBuffer[HitNum].time = vetoTime;
	HitNum+=1;
      }else{
	vetoHitBuffer[HitNum].modID = DetModID[imod];
	vetoHitBuffer[HitNum].energy = 0;
	vetoHitBuffer[HitNum].time = vetoTime;
	HitNum+=1;
      }
    }
  }
  
  std::sort( vetoHitBuffer, &(vetoHitBuffer[HitNum]), pVetoHit::largerHit );
  
  
  BHCVVetoModID = vetoHitBuffer[0].modID;
  BHCVVetoEne   = vetoHitBuffer[0].energy;
  BHCVVetoTime  = vetoHitBuffer[0].time;

  if( BHCVVetoEne > m_vetoThreshold ) return true;
  
  return false;
  
}
double MTVetoBHCV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoBHCV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
