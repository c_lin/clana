#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoBHPV.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoBHPV::MTVetoBHPV( Int_t userFlag ) : m_userFlag( userFlag )
{

  DetName = "BHPV"; 
  DetNumber = MTBP::BHPVNChannels;
  DetModID = new Int_t[DetNumber]();
  DetNHit = new Short_t[DetNumber]();
  DetEne   = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  DetTime  = new Float_t[DetNumber][MTBP::NMaxHitPerEvent]();
  
  //separated variable for coincidence (BHPVNMaxCoincidenceStore), at 29th May, 2014
  BHPVCoinModID = new Int_t[MTBP::BHPVNMaxCoincidenceStore];
  BHPVCoinnHitMod = new Int_t[MTBP::BHPVNMaxCoincidenceStore];
  BHPVCoinEne = new Float_t[MTBP::BHPVNMaxCoincidenceStore];
  BHPVCoinTime = new Float_t[MTBP::BHPVNMaxCoincidenceStore];
  BHPVCoinTimeSpread = new Float_t[MTBP::BHPVNMaxCoincidenceStore];
  
  TOFCorrection = new Float_t[DetNumber];
  
  int maxhit=MTBP::NMaxHitPerEvent;
  MaxBufferNum= DetNumber* maxhit;  
  vetoHitBuffer = new pVetoHit [MaxBufferNum]();
  
  Float_t FlightLength = 0;
  for( Int_t iMod=0 ; iMod<MTBP::nModBHPV ; iMod++ ){
    TOFCorrection[iMod] = FlightLength/(TMath::C()*1e-6);
    FlightLength += (MTBP::BHPVModZLength + MTBP::BHPVModGapZ[iMod]);
    
    //m_IDtoIndexMap[2*iMod] = 2*iMod;
    //m_IDtoIndexMap[2*iMod+1] = 2*iMod+1;
  }
  FlightLength -= MTBP::BHPVAerogelCenter;
  //TOFCorrectionBHTS = FlightLength/(TMath::C()*1e-6);
  //m_IDtoIndexMap[MTBP::BHTSModuleID] = 2*MTBP::nModBHPV;

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  MakeWfmArray();
  Initialize();
}


MTVetoBHPV::~MTVetoBHPV()
{
  
  delete[] DetModID;
  delete[] DetNHit;
  delete[] DetEne;  
  delete[] DetTime;
  
  delete[] BHPVCoinModID;
  delete[] BHPVCoinnHitMod;
  delete[] BHPVCoinEne;
  delete[] BHPVCoinTime;
  delete[] BHPVCoinTimeSpread;
  
  delete[] TOFCorrection;
  
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoBHPV::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "BHPVNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "BHPVModID", DetModID );
  m_clusterTree->SetBranchAddress( "BHPVnHits", DetNHit );
  m_clusterTree->SetBranchAddress( "BHPVTime", DetTime );
  m_clusterTree->SetBranchAddress( "BHPVEne", DetEne );
}


void MTVetoBHPV::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "BHPVNumber", &DetNumber, "BHPVNumber/I");
  m_recTree->Branch( "BHPVModID", DetModID, "BHPVModID[BHPVNumber]/I");
  m_recTree->Branch( "BHPVnHits", DetNHit, "BHPVnHits[BHPVNumber]/S");
  std::ostringstream leafstr[2];
  leafstr[0] << "BHPVEne[BHPVNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  leafstr[1] << "BHPVTime[BHPVNumber][" << MTBP::NMaxHitPerEvent << "]/F";
  m_recTree->Branch( "BHPVEne", DetEne, leafstr[0].str().c_str() );
  m_recTree->Branch( "BHPVTime", DetTime, leafstr[1].str().c_str() );
  
  m_recTree->Branch( "BHPVVetoModID", &BHPVVetoModID, "BHPVVetoModID/I");
  m_recTree->Branch( "BHPVVetonHitMod", &BHPVVetonHitMod, "BHPVVetonHitMod/I");
  m_recTree->Branch( "BHPVVetoEne", &BHPVVetoEne, "BHPVVetoEne/F");
  m_recTree->Branch( "BHPVVetoTime", &BHPVVetoTime, "BHPVVetoTime/F");

  m_recTree->Branch( "BHPVCoinNumber", &BHPVCoinNumber, "BHPVCoinNumber/I");
  m_recTree->Branch( "BHPVCoinnHitMod", BHPVCoinnHitMod, "BHPVCoinnHitMod[BHPVCoinNumber]/I");
  m_recTree->Branch( "BHPVCoinModID", BHPVCoinModID, "BHPVCoinModID[BHPVCoinNumber]/I");
  m_recTree->Branch( "BHPVCoinEne", BHPVCoinEne, "BHPVCoinEne[BHPVCoinNumber]/F");
  m_recTree->Branch( "BHPVCoinTime", BHPVCoinTime, "BHPVCoinTime[BHPVCoinNumber]/F");
  m_recTree->Branch( "BHPVCoinTimeSpread", BHPVCoinTimeSpread, "BHPVCoinTimeSpread[BHPVCoinNumber]/F");
  
}


void MTVetoBHPV::Initialize( void )
{
  for( int imod=0; imod<MaxBufferNum; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }
  
  BHPVVetoModID = -1;
  BHPVVetonHitMod = 0;
  BHPVVetoEne = 0;
  BHPVVetoTime = MTBP::Invalid;//-9999
  
  BHPVCoinNumber = 0;
  for( Int_t iHit=0 ; iHit<MTBP::BHPVNMaxCoincidenceStore ; iHit++ ){
    BHPVCoinModID[iHit] = (Int_t)MTBP::Invalid;
    BHPVCoinnHitMod[iHit] = (Int_t)MTBP::Invalid;
    BHPVCoinEne[iHit] = MTBP::Invalid;
    BHPVCoinTime[iHit] = MTBP::Invalid;
    BHPVCoinTimeSpread[iHit] = MTBP::Invalid;
  }
}


bool MTVetoBHPV::Process( int mode )
{
  Initialize();

  static const double s_BHPVZPosition = GetDetZPosition("BHPV");
  
  Int_t TooMany = 0;
  
  Bool_t ToBeVetoed = false;
  Bool_t IsAnalyzed[MTBP::BHPVNChannels][MTBP::NMaxHitPerEvent] = {};

  Int_t   detnumber = MTBP::BHPVNChannels;
  Int_t   detmodid[MTBP::BHPVNChannels];
  Short_t detnhits[MTBP::BHPVNChannels];
  Float_t detene[MTBP::BHPVNChannels][MTBP::NMaxHitPerEvent];
  Float_t dettime[MTBP::BHPVNChannels][MTBP::NMaxHitPerEvent];
  for(int ich=0;ich<detnumber;ich++){
    detmodid[ich]=ich;
    detnhits[ich]=0;
    for(Short_t ihit=0;ihit<MTBP::NMaxHitPerEvent;ihit++){
      detene[ich][ihit]=MTBP::Invalid;
      dettime[ich][ihit]=MTBP::Invalid;
    }
  }
  for(int ich=0;ich<DetNumber;ich++){
    int inum = ( DetModID[ich] < 32 ) ? DetModID[ich] : DetModID[ich] - 40 + 32;

    detnhits[inum] = DetNHit[ich];
    for(Short_t ihit=0;ihit<DetNHit[ich];ihit++){
      detene[inum][ihit]  = DetEne[ich][ihit];
      dettime[inum][ihit] = DetTime[ich][ihit];
    }
  }


  Double_t VetoTimeLength = 0;
  Double_t VetoTimeOffset = 0;
  GetVetoWindow( VetoTimeLength, VetoTimeOffset );
  
  //hits with light yield smaller than given threshold (default:1.5p.e.) will be neglected
  for( Int_t iCh=0 ; iCh<(MTBP::BHPVNChannels-1) ; iCh++ ){
    for( Short_t iHit=0 ; iHit<detnhits[iCh] ; iHit++ ){
      if( detene[iCh][iHit]<m_vetoThreshold ) IsAnalyzed[iCh][iHit] = true;
    }
  }  
  
  for( Int_t iCh=0 ; iCh<(MTBP::BHPVNChannels-2) ; iCh++ ){
    
    Int_t iMod = (iCh/2);
    for( Short_t iHit=0 ; iHit<detnhits[iCh] ; iHit++ ){
      if( !IsAnalyzed[iCh][iHit] ){
	IsAnalyzed[iCh][iHit] = true;
	Int_t nModCoincidence = 1;
	Float_t TotalEnergy = detene[iCh][iHit];
	
	Float_t HitTime = dettime[iCh][iHit] - TOFCorrection[iMod];
	std::vector<Float_t> HitTimeVec;
	HitTimeVec.reserve( MTBP::BHPVNChannels );
	HitTimeVec.push_back( HitTime );
	
	//check hit in partner ch
	if( (iCh%2)==0 ){
	  for( Short_t iHitPartner=0 ; iHitPartner<detnhits[iCh+1] ; iHitPartner++ ){
	    Double_t HitTimeTmp = dettime[iCh+1][iHitPartner] - TOFCorrection[iMod];
	    if( !IsAnalyzed[iCh+1][iHitPartner] && TMath::Abs(HitTimeTmp-HitTime)<MTBP::BHPVCoincidenceTimeWindow ){
	      HitTimeVec.push_back( HitTimeTmp );
	      TotalEnergy += detene[iCh+1][iHitPartner];
	      IsAnalyzed[iCh+1][iHitPartner] = true;
	    }
	  }
	}
	
	//proceed to downstrem module
	for( Int_t jMod=iMod+1 ; jMod<MTBP::nModBHPV ; jMod++ ){
	  Bool_t coincidence = false;
	  
	  //north side
	  Int_t jCh = 2*jMod;
	  for( Short_t jHit=0 ; jHit<detnhits[jCh] ; jHit++ ){
	    Double_t HitTimeTmp = dettime[jCh][jHit] - TOFCorrection[jMod];
	    if( !IsAnalyzed[jCh][jHit] && TMath::Abs(HitTimeTmp-HitTime)<MTBP::BHPVCoincidenceTimeWindow ){
	      HitTimeVec.push_back( HitTimeTmp );
	      TotalEnergy += detene[jCh][jHit];
	      IsAnalyzed[jCh][jHit] = true;
	      coincidence = true;
	    }
	  }	
	  
	  //south side
	  for( Short_t jHit=0 ; jHit<detnhits[jCh+1] ; jHit++ ){
	    Double_t HitTimeTmp = dettime[jCh+1][jHit] - TOFCorrection[jMod];
	    if( !IsAnalyzed[jCh+1][jHit] && TMath::Abs(HitTimeTmp-HitTime)<MTBP::BHPVCoincidenceTimeWindow ){
	      HitTimeVec.push_back( HitTimeTmp );
	      TotalEnergy += detene[jCh+1][jHit];
	      IsAnalyzed[jCh+1][jHit] = true;
	      coincidence = true;
	    }
	  }	
	  
	  if( !coincidence ) break;
	  nModCoincidence++; 
	}
	
	if( nModCoincidence>0 ){
	  //Double_t CoincidenceTime = TMath::Mean( HitTimeVec.size(), &HitTimeVec[0] ) - m_eventStartTime - TMath::Abs(MTBP::CSIZPosition-m_eventStartZ)/(TMath::C()*1e-6);
	  Double_t CoincidenceTime = TMath::Mean( HitTimeVec.size(), &HitTimeVec[0] ) - m_eventStartTime - TMath::Abs(s_BHPVZPosition-m_eventStartZ)/(TMath::C()*1e-6);
	  Double_t CoincidenceTimeSpread = TMath::RMS( HitTimeVec.size(), &HitTimeVec[0] );
	  if( BHPVCoinNumber<MTBP::BHPVNMaxCoincidenceStore
	      && TMath::Abs( CoincidenceTime - VetoTimeOffset )<MTBP::BHPVCoincidenceStoreTimeRange ){
	    BHPVCoinModID[BHPVCoinNumber] = iCh;
	    BHPVCoinnHitMod[BHPVCoinNumber] = nModCoincidence;
	    BHPVCoinEne[BHPVCoinNumber] = TotalEnergy;
	    BHPVCoinTime[BHPVCoinNumber] = CoincidenceTime;
	    BHPVCoinTimeSpread[BHPVCoinNumber] = CoincidenceTimeSpread;
	    
	    BHPVCoinNumber++;
	  }
	  else if( BHPVCoinNumber>=MTBP::BHPVNMaxCoincidenceStore ){
	    TooMany++;
	    std::cout << "WARNING : BHPV too many coincidence in an event! : " << (BHPVCoinNumber+TooMany) << std::endl;
	  }
	}
      }//if( !IsAnalyzed[iCh][iHit] )
    }//for( Int_t iHit=0 ; iHit<detnhits[iCh] ; iHit++ )
  }//for( Int_t iCh=0 ; iCh<(MTBP::BHPVNChannels-1) ; iCh++ )
  
  for( Int_t iHit=0 ; iHit<BHPVCoinNumber ; iHit++ ){
    if( IsInVetoWindow( BHPVCoinTime[iHit] ) && BHPVCoinnHitMod[iHit]>BHPVVetonHitMod ){
      BHPVVetoModID = BHPVCoinModID[iHit];//added at 25th Jan, 2014
      BHPVVetonHitMod = BHPVCoinnHitMod[iHit];
      BHPVVetoEne = BHPVCoinEne[iHit];
      BHPVVetoTime = BHPVCoinTime[iHit];
    }
  }
  if( BHPVVetonHitMod>=MTBP::BHPVNMinModCoincidence ) ToBeVetoed = true;
  //when on-timing module coincidence (nHitMod>=3) was not found,
  //  ModID = -1
  //  nHitMod = 0
  //  Ene = 0
  //  Time = -9999
  
  return ToBeVetoed;
}
double MTVetoBHPV::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoBHPV::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
