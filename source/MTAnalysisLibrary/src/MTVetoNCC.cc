#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoNCC.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoNCC::MTVetoNCC( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "NCC"; 
  DetNumber = MTBP::NCCNModules;
  DetModID  = new Int_t  [DetNumber];
  DetTime   = new Float_t[DetNumber];
  DetEne    = new Float_t[DetNumber];
  DetSmoothness = new Float_t[DetNumber];
  DetPSBit  = new Short_t[DetNumber];
  
  DetWfmCorrupted = NULL;
  ScintiDetNumber = 4;///// HINEMOS(4)
  ScintiDetModID  = new Int_t  [ScintiDetNumber];
  ScintiDetTime   = new Float_t[ScintiDetNumber];
  ScintiDetEne    = new Float_t[ScintiDetNumber];
  ScintiDetSmoothness = new Float_t[ScintiDetNumber];
  ScintiDetPSBit  = new Short_t[ScintiDetNumber];


  IndividualDetNumber = 144;///// HINEMOS(4)
  IndividualDetModID  = new Int_t  [IndividualDetNumber];
  IndividualDetTime   = new Float_t[IndividualDetNumber];
  IndividualDetEne    = new Float_t[IndividualDetNumber];
  IndividualDetSmoothness = new Float_t[IndividualDetNumber];
  IndividualDetPSBit  = new Short_t[IndividualDetNumber];

  DetWfmCorrupted = NULL;

  vetoHitBuffer = new pVetoHit [DetNumber]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();
  
  DetNumber = MTBP::NCCNChannels;
  MakeWfmArray();
  DetNumber = MTBP::NCCNModules;

  Initialize();
}


MTVetoNCC::~MTVetoNCC()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetSmoothness;
  delete[] DetPSBit;

  delete[] ScintiDetModID;
  delete[] ScintiDetTime;
  delete[] ScintiDetEne;
  delete[] ScintiDetSmoothness;
  delete[] ScintiDetPSBit;

  delete[] IndividualDetModID;
  delete[] IndividualDetTime;
  delete[] IndividualDetEne;
  delete[] IndividualDetSmoothness;
  delete[] IndividualDetPSBit;



  delete[] vetoHitBuffer;
  delete   m_E14BP;

  

}


void MTVetoNCC::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress("NCCModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress("NCCModuleModID",   DetModID);
  m_clusterTree->SetBranchAddress("NCCModuleHitTime", DetTime);
  m_clusterTree->SetBranchAddress("NCCModuleEne",     DetEne);
  if(m_clusterTree->GetBranch("NCCModulePSBit"))
	  m_clusterTree->SetBranchAddress("NCCModulePSBit",   DetPSBit);
  if(m_clusterTree->GetBranch("NCCModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "NCCModuleSmoothness", DetSmoothness );  
  }
  
  MTVeto::SetBranchAddress(tr);

  
}


void MTVetoNCC::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "NCCModuleNumber",&DetNumber, "NCCModuleNumber/I");
  m_recTree->Branch( "NCCModuleModID",  DetModID,  "NCCModuleModID[NCCModuleNumber]/I");
  m_recTree->Branch( "NCCModuleEne",    DetEne,    "NCCModuleEne[NCCModuleNumber]/F");
  m_recTree->Branch( "NCCModuleTime",   DetTime,   "NCCModuleTime[NCCModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "NCCModuleSmoothness", DetSmoothness, "NCCModuleSmoothness[NCCModuleNumber]/F" );
  if(m_clusterTree->GetBranch("NCCModulePSBit"))
	  m_recTree->Branch( "NCCModulePSBit",  DetPSBit,  "NCCModulePSBit[NCCModuleNumber]/S");

  m_recTree->Branch( "NCCScintiModuleNumber",&ScintiDetNumber, "NCCScintiModuleNumber/I");
  m_recTree->Branch( "NCCScintiModuleModID",  ScintiDetModID,  "NCCScintiModuleModID[NCCScintiModuleNumber]/I");
  m_recTree->Branch( "NCCScintiModuleEne",    ScintiDetEne,    "NCCScintiModuleEne[NCCScintiModuleNumber]/F");
  m_recTree->Branch( "NCCScintiModuleTime",   ScintiDetTime,   "NCCScintiModuleTime[NCCScintiModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "NCCScintiModuleSmoothness", ScintiDetSmoothness, "NCCScnitiModuleSmoothness[NCCScintiModuleNumber]/F" );
  if(m_clusterTree->GetBranch("NCCModulePSBit"))
	  m_recTree->Branch( "NCCScintiModulePSBit",  ScintiDetPSBit,  "NCCScintiModulePSBit[NCCScintiModuleNumber]/S");

  m_recTree->Branch( "NCCIndividualModuleNumber",&IndividualDetNumber, "NCCIndividualModuleNumber/I");
  m_recTree->Branch( "NCCIndividualModuleModID",  IndividualDetModID,  "NCCIndividualModuleModID[NCCIndividualModuleNumber]/I");
  m_recTree->Branch( "NCCIndividualModuleEne",    IndividualDetEne,    "NCCIndividualModuleEne[NCCIndividualModuleNumber]/F");
  m_recTree->Branch( "NCCIndividualModuleTime",   IndividualDetTime,   "NCCIndividualModuleTime[NCCIndividualModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "NCCIndividualModuleSmoothness", IndividualDetSmoothness, "NCCIndividualModuleSmoothness[NCCIndividualModuleNumber]/F" );
  if(m_clusterTree->GetBranch("NCCModulePSBit"))
	  m_recTree->Branch( "NCCIndividualModulePSBit",  IndividualDetPSBit,  "NCCIndividualModulePSBit[NCCScintiModuleNumber]/S");

  m_recTree->Branch( "NCCVetoModID",    &NCCVetoModID,   "NCCVetoModID/I");
  m_recTree->Branch( "NCCVetoEne",      &NCCVetoEne,     "NCCVetoEne/F");
  m_recTree->Branch( "NCCVetoTime",     &NCCVetoTime,    "NCCVetoTime/F");
  m_recTree->Branch( "NCCTotalVetoEne", &NCCTotalVetoEne,"NCCTotalVetoEne/F");

  m_recTree->Branch( "NCCScintiVetoModID",&NCCScintiVetoModID, "NCCScintiVetoModID/I");
  m_recTree->Branch( "NCCScintiVetoEne",  &NCCScintiVetoEne,   "NCCScintiVetoEne/F");
  m_recTree->Branch( "NCCScintiVetoTime", &NCCScintiVetoTime,  "NCCScintiVetoTime/F");

  m_recTree->Branch( "NCCIndividualVetoModID",&NCCIndividualVetoModID, "NCCIndividualVetoModID/I");
  m_recTree->Branch( "NCCIndividualVetoEne",  &NCCIndividualVetoEne,   "NCCIndividualVetoEne/F");
  m_recTree->Branch( "NCCIndividualVetoTime", &NCCIndividualVetoTime,  "NCCIndividualVetoTime/F");
  
  MTVeto::Branch(tr);

  

}

void MTVetoNCC::Initialize( void )
{
  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::NCC))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }

  for( int imod=0; imod<MTBP::NCCNModules; imod++ ){
    vetoHitBuffer[imod].modID  = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time   = 0;
  }

  NCCVetoModID    = -1;
  NCCVetoEne      = 0;
  NCCVetoTime     = 9999.;
  NCCTotalVetoEne = 0;

  NCCScintiVetoModID = -1;
  NCCScintiVetoEne   = 0;
  NCCScintiVetoTime  = 9999.;

  NCCIndividualVetoModID = -1;
  NCCIndividualVetoEne   = 0;
  NCCIndividualVetoTime  = 9999.;

}


bool MTVetoNCC::Process( int mode )
{

  Initialize();

  static const double s_CSIZPosition = GetDetZPosition("CSI");
  static const double s_NCCRZPosition = MTBP::NCCRZPosition;


  IndividualDetNumber = 0;
  for( int imod=0; imod<DetNumber; imod++ ){
    if( (DetModID[imod]%10) !=0 ){
		IndividualDetModID[IndividualDetNumber]   = DetModID[imod];
		IndividualDetEne[  IndividualDetNumber]   = DetEne[imod];
		if(DetTime[imod]<-70000) DetTime[imod] = -9999;
		IndividualDetTime[IndividualDetNumber]  = DetTime[imod];
		IndividualDetSmoothness[IndividualDetNumber]  = DetSmoothness[imod];
		IndividualDetPSBit[IndividualDetNumber]       = DetPSBit[imod];
		IndividualDetNumber++;
		DetEne[imod] = 0;
	}
  }

  


  ///// Copy NCCModule* information to NCCScintiModule*

  ScintiDetNumber = 0;
  for(int i=0; i<DetNumber; i++){
    if( DetModID[i]>=600 ){
      ScintiDetModID[ScintiDetNumber] = DetModID[i];
      ScintiDetEne[ScintiDetNumber]   = DetEne[i];
      ScintiDetTime[ScintiDetNumber]  = DetTime[i];
      ScintiDetSmoothness[ScintiDetNumber]  = DetSmoothness[i];
      ScintiDetPSBit[ScintiDetNumber] = DetPSBit[i];
      ++ScintiDetNumber;
    }
  }
  
  ///// for NCC (CsI crystal)   
  for( int imod=0; imod<DetNumber; imod++ ){
    //vetoTime = DetTime[imod] - m_eventStartTime;
    vetoTime = DetTime[imod] - m_eventStartTime - (s_CSIZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime ) && (DetModID[imod]%10)==0 && DetModID[imod]<600){
      vetoHitBuffer[imod].modID  = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time   = vetoTime;
      NCCTotalVetoEne           += DetEne[imod];
    }else{
      vetoHitBuffer[imod].modID  = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time   = vetoTime;
    }
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );
  
  if(vetoHitBuffer[0].energy>0){  
    NCCVetoModID = vetoHitBuffer[0].modID;
    NCCVetoEne   = vetoHitBuffer[0].energy;
    NCCVetoTime  = vetoHitBuffer[0].time;
  } else {
    NCCVetoModID = -1;
    NCCVetoEne   = 0;
    NCCVetoTime  = -9999.;
  }


  for( int imod=0; imod<MTBP::NCCNModules; imod++ ){
    vetoHitBuffer[imod].modID  = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time   = 0;
  }

  ///// for Plastic Scintillators (HINEMOS)
  for( int imod=0; imod<ScintiDetNumber; imod++ ){
    //vetoTime = ScintiDetTime[imod] - m_eventStartTime;
    vetoTime = ScintiDetTime[imod] - m_eventStartTime - (s_CSIZPosition - m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime ) ){
      vetoHitBuffer[imod].modID  = ScintiDetModID[imod];
      vetoHitBuffer[imod].energy = ScintiDetEne[imod];
      vetoHitBuffer[imod].time   = vetoTime;
    }else{
      vetoHitBuffer[imod].modID  = ScintiDetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time   = vetoTime;
    }
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[ScintiDetNumber]), pVetoHit::largerHit );

  if(vetoHitBuffer[0].energy>0){
    NCCScintiVetoModID = vetoHitBuffer[0].modID;
    NCCScintiVetoEne   = vetoHitBuffer[0].energy;
    NCCScintiVetoTime  = vetoHitBuffer[0].time;
  }else{
    NCCScintiVetoModID = -1;
    NCCScintiVetoEne   = 0;
    NCCScintiVetoTime  = -9999.;
  }


  for( int imod=0; imod<MTBP::NCCNModules; imod++ ){
    vetoHitBuffer[imod].modID  = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time   = 0;
  }

  ///// for individuals
  for( int imod=0; imod<IndividualDetNumber; imod++ ){
	
	double vetoTime2 = -9999;
	if(IndividualDetModID[imod]%10==3){
		// rear
		// the last term is an offset of two different timings
		vetoTime2 = IndividualDetTime[imod] - m_eventStartTime - (s_NCCRZPosition - m_eventStartZ)/(TMath::C()/1e6) - (25.0-10.5);
	}
    
	vetoTime = IndividualDetTime[imod] - m_eventStartTime - (s_CSIZPosition - m_eventStartZ)/(TMath::C()/1e6);

	double vetoStartTime = m_vetoWindowLength - 10.0/2.;
	double vetoEndTime   = m_vetoWindowLength + 10.0/2.;

	if( IsInVetoWindow( vetoTime ) ){
      vetoHitBuffer[imod].modID  = IndividualDetModID[imod];
      vetoHitBuffer[imod].energy = IndividualDetEne[imod];
      vetoHitBuffer[imod].time   = vetoTime;
    }else if( IsInVetoWindow( vetoTime2 ) ){
      vetoHitBuffer[imod].modID  = IndividualDetModID[imod];
      vetoHitBuffer[imod].energy = IndividualDetEne[imod];
      vetoHitBuffer[imod].time   = vetoTime2;
	}else{
      vetoHitBuffer[imod].modID  = IndividualDetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time   = vetoTime;
	}
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[IndividualDetNumber]), pVetoHit::largerHit );

  if(vetoHitBuffer[0].energy>0){
    NCCIndividualVetoModID = vetoHitBuffer[0].modID;
    NCCIndividualVetoEne   = vetoHitBuffer[0].energy;
    NCCIndividualVetoTime  = vetoHitBuffer[0].time;
  }else{
    NCCIndividualVetoModID = -1;
    NCCIndividualVetoEne   = 0;
    NCCIndividualVetoTime  = -9999.;
  }


  if( NCCVetoEne > m_vetoThreshold ) return true;
  return false;
}

double MTVetoNCC::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoNCC::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
