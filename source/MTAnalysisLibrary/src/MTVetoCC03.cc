#include <sstream>
#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoCC03.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoCC03::MTVetoCC03( Int_t userFlag ) : m_userFlag( userFlag )
{
  
  DetName = "CC03"; 
  DetNumber = MTBP::CC03NModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();

  DetWfmCorrupted = NULL;
  vetoHitBuffer = new pVetoHit [DetNumber]();

  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  DetNumber = MTBP::CC03NChannels;
  MTVeto::MakeWfmArray();
  DetNumber = MTBP::CC03NModules;

  Initialize();
}


MTVetoCC03::~MTVetoCC03()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetPSBit;
  delete[] DetSmoothness;
  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoCC03::SetBranchAddress( TTree* tr )
{    
  m_clusterTree = tr;
 
  m_clusterTree->SetBranchAddress( "CC03ModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "CC03ModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "CC03ModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "CC03ModuleEne", DetEne );
  
  if(m_clusterTree->GetBranch("CC03ModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "CC03ModuleSmoothness", DetSmoothness );  
  }

  if(m_clusterTree->GetBranch("CC03ModulePSBit"))
	  m_clusterTree->SetBranchAddress( "CC03ModulePSBit", DetPSBit );

  MTVeto::SetBranchAddress(tr);

  


  //m_clusterTree->SetBranchAddress( "CC03EtSum", DetEtSum );//added at 16th Mar, 2014
}


void MTVetoCC03::Branch( TTree* tr )
{
  m_recTree = tr;
  
  m_recTree->Branch( "CC03ModuleNumber", &DetNumber, "CC03ModuleNumber/I");
  m_recTree->Branch( "CC03ModuleModID", DetModID, "CC03ModuleModID[CC03ModuleNumber]/I");
  m_recTree->Branch( "CC03ModuleEne", DetEne, "CC03ModuleEne[CC03ModuleNumber]/F");
  m_recTree->Branch( "CC03ModuleTime", DetTime, "CC03ModuleTime[CC03ModuleNumber]/F");
  
    
  if(m_clusterTree->GetBranch("CC03ModuleSmoothness"))
	  m_recTree->Branch( "CC03ModuleSmoothness", DetSmoothness, "CC03Smoothness[CC03ModuleNumber]/F" );
  if(m_clusterTree->GetBranch("CC03ModulePSBit"))
	  m_recTree->Branch( "CC03ModulePSBit", DetPSBit, "CC03ModulePSBit[CC03ModuleNumber]/S");

  m_recTree->Branch( "CC03VetoModID", &CC03VetoModID,"CC03VetoModID/I");
  m_recTree->Branch( "CC03VetoEne", &CC03VetoEne,"CC03VetoEne/F");
  m_recTree->Branch( "CC03VetoTime", &CC03VetoTime,"CC03VetoTime/F");
  m_recTree->Branch( "CC03TotalVetoEne", &CC03TotalVetoEne,"CC03TotalVetoEne/F");

  MTVeto::Branch(tr);
    //std::ostringstream leafstr;
  //leafstr << "CC03EtSum[" << MTBP::nSample125 << "]/L";
  //m_recTree->Branch( "CC03EtSum", DetEtSum, leafstr.str().c_str() );//added at 16th Mar, 2014
}


void MTVetoCC03::Initialize( void )
{

  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<MTBP::CC03))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  for( int imod=0; imod<MTBP::CC03NModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  CC03VetoModID = -1;
  CC03VetoEne = 0;
  CC03VetoTime = 9999.;
  CC03TotalVetoEne=0;
}


bool MTVetoCC03::Process( int mode ){

    Initialize();

    static const double s_CC03ZPosition = GetDetZPosition("CC03");

  // Front
  for( int imod=0; imod<DetNumber; imod++ ){
    vetoTime = DetTime[imod] - m_eventStartTime-(s_CC03ZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
      CC03TotalVetoEne+=DetEne[imod];
    }else{
      vetoHitBuffer[imod].modID = DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }

  }
  
  std::sort( vetoHitBuffer, &(vetoHitBuffer[DetNumber]), pVetoHit::largerHit );
  if(vetoHitBuffer[0].energy>0){
    CC03VetoModID = vetoHitBuffer[0].modID;
    CC03VetoEne   = vetoHitBuffer[0].energy;
    CC03VetoTime  = vetoHitBuffer[0].time;
  } else {
    CC03VetoModID = -1;
    CC03VetoEne   = 0;
    CC03VetoTime  = -9999.;
  }


  


  if( CC03VetoEne > m_vetoThreshold ) return true;

  return false;

}


double MTVetoCC03::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoCC03::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
