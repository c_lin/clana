#include "TMath.h"
#include "MTAnalysisLibrary/MTBasicParameters.h"
#include "MTAnalysisLibrary/MTVetoDetector.h"
#include "MTAnalysisLibrary/MTVeto.h"

MTVetoDetector::MTVetoDetector( int detID )
{

  DetectorID = detID;
  DetNumber = MTBP::DetNChannels[detID];
  DetModID = new Int_t [DetNumber]();
  DetTime  = new Float_t [DetNumber]();
  DetEne   = new Float_t [DetNumber]();
  DetHitZ  = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();
 
  DetWfmCorrupted = NULL;
  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 

  DetName = MTBP::detectorName[detID].c_str();

  vetoHitBuffer = new pVetoHit [1000]();

  
  MakeWfmArray();
  

  
  Initialize();
}


MTVetoDetector::~MTVetoDetector()
{
  	
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetHitZ;
  delete[] DetPSBit;
  delete[] DetSmoothness;
  delete[] vetoHitBuffer;

}


void MTVetoDetector::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( Form("%sModuleNumber",DetName.c_str()), &DetNumber);
  m_clusterTree->SetBranchAddress( Form("%sModuleModID",DetName.c_str()), DetModID );
  m_clusterTree->SetBranchAddress( Form("%sModuleHitTime",DetName.c_str()), DetTime );
  m_clusterTree->SetBranchAddress( Form("%sModuleEne",DetName.c_str()), DetEne );  
  
  if(m_clusterTree->GetBranch( Form("%sModulePSBit",DetName.c_str()) ))
	  m_clusterTree->SetBranchAddress( Form("%sModulePSBit",DetName.c_str()), DetPSBit );  
  if(m_clusterTree->GetBranch(Form("%sModuleSmoothness",DetName.c_str()))){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( Form("%sModuleSmoothness",DetName.c_str()), DetSmoothness );
  }

  MTVeto::SetBranchAddress(tr);
  
}


void MTVetoDetector::Branch( TTree* tr )
{
  m_recTree = tr;

  m_recTree->Branch( Form("%sModuleNumber",DetName.c_str()), &DetNumber, Form("%sModuleNumber/I",DetName.c_str()) );
  m_recTree->Branch( Form("%sModuleModID",DetName.c_str()), DetModID, Form("%sModuleModID[%sModuleNumber]/I",DetName.c_str(),DetName.c_str()) );
  m_recTree->Branch( Form("%sModuleHitTime",DetName.c_str()), DetTime, Form("%sModuleHitTime[%sModuleNumber]/F",DetName.c_str(),DetName.c_str()) );
  m_recTree->Branch( Form("%sModuleEne",DetName.c_str()), DetEne, Form("%sModuleEne[%sModuleNumber]/F",DetName.c_str(),DetName.c_str()) );
  if(m_clusterTree->GetBranch( Form("%sModulePSBit",DetName.c_str()) ))
	  m_recTree->Branch( Form("%sModulePSBit",DetName.c_str()), DetPSBit, Form("%sModulePSBit[%sModuleNumber]/S",DetName.c_str(),DetName.c_str()) );
  if(m_clusterTree->GetBranch(Form("%sModuleSmoothness",DetName.c_str())))
	  m_recTree->Branch( Form("%sModuleSmoothness",DetName.c_str()), DetSmoothness, Form("%sModuleSmoothness[%sModuleNumber]",DetName.c_str(),DetName.c_str()) );
  
  MTVeto::Branch(tr);
  
}


void MTVetoDetector::Initialize( void )
{

  //for( int imod=0; imod<MTBP::DetNChannels[detID]; imod++ ){
  for( int imod=0; imod<1000; imod++ ){    
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
    vetoHitBuffer[imod].position = 0;
  }
}


bool MTVetoDetector::Process( int mode )
{
  if(DetWfmCorrupted!=NULL){
	  if(!((*DetWfmCorrupted)&(1<<DetectorID))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  return true;
  //return false;

}




