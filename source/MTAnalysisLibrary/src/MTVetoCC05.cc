#include <fstream>
#include "TMath.h"
#include "MTAnalysisLibrary/MTVetoCC05.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

MTVetoCC05::MTVetoCC05( Int_t userFlag ) : m_userFlag( userFlag )
{
  DetName = "CC05"; 
  DetNumber = MTBP::CC05NModules;
  DetModID = new Int_t [DetNumber]();
  DetTime = new Float_t [DetNumber]();
  DetEne = new Float_t [DetNumber]();
  DetPSBit = new Short_t [DetNumber]();
  DetSmoothness = new Float_t [DetNumber]();

  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 


  E391DetNumber = MTBP::CC05E391NModules;
  E391DetModID = new Int_t [E391DetNumber]();
  E391DetTime = new Float_t [E391DetNumber]();
  E391DetEne = new Float_t [E391DetNumber]();
  E391DetSmoothness = new Float_t [E391DetNumber]();
  E391DetAreaR = new Float_t [E391DetNumber]();
  E391DetWfmCorrectNumber = new short[E391DetNumber]();
  E391DetPSBit = new Short_t [E391DetNumber]();

  ScintiDetNumber = MTBP::CC05ScintiNModules;
  ScintiDetModID = new Int_t [ScintiDetNumber]();
  ScintiDetTime = new Float_t [ScintiDetNumber]();
  ScintiDetEne = new Float_t [ScintiDetNumber]();
  ScintiDetSmoothness = new Float_t [ScintiDetNumber]();
  ScintiDetAreaR = new Float_t [ScintiDetNumber]();
  ScintiDetWfmCorrectNumber = new short[ScintiDetNumber]();
  ScintiDetPSBit = new Short_t [ScintiDetNumber]();

  DetWfmCorrupted = NULL;
  
  vetoHitBuffer = new pVetoHit [DetNumber]();

  m_E14BP = new E14BasicParamManager::E14BasicParamManager();

  MakeWfmArray();
  //Initialize();
}


MTVetoCC05::~MTVetoCC05()
{
  delete[] DetModID;
  delete[] DetTime;
  delete[] DetEne;
  delete[] DetPSBit;
  delete[] DetSmoothness;

  delete[] E391DetModID;
  delete[] E391DetTime;
  delete[] E391DetEne;
  delete[] E391DetPSBit;
  delete[] E391DetSmoothness;
  delete[] E391DetAreaR;
  delete[] E391DetWfmCorrectNumber;

  delete[] ScintiDetModID;
  delete[] ScintiDetTime;
  delete[] ScintiDetEne;
  delete[] ScintiDetPSBit;
  delete[] ScintiDetSmoothness;
  delete[] ScintiDetAreaR;
  delete[] ScintiDetWfmCorrectNumber;

  delete[] vetoHitBuffer;
  delete   m_E14BP;
}


void MTVetoCC05::SetBranchAddress( TTree* tr )
{
  m_clusterTree = tr;
  m_clusterTree->SetBranchAddress( "CC05ModuleNumber", &DetNumber);
  m_clusterTree->SetBranchAddress( "CC05ModuleModID", DetModID );
  m_clusterTree->SetBranchAddress( "CC05ModuleHitTime", DetTime );
  m_clusterTree->SetBranchAddress( "CC05ModuleEne", DetEne );
  if(m_clusterTree->GetBranch("CC05ModulePSBit"))
	  m_clusterTree->SetBranchAddress( "CC05ModulePSBit", DetPSBit );

  if(m_clusterTree->GetBranch("CC05ModuleSmoothness")){
	  DetWfmCorrupted = (int*) m_clusterTree->GetBranch("DetectorWFMCorruptBit")->GetAddress();
	  m_clusterTree->SetBranchAddress( "CC05ModuleSmoothness", DetSmoothness );  
  }
  MTVeto::SetBranchAddress(tr);
}


void MTVetoCC05::Branch( TTree* tr )
{
  m_recTree = tr;
  
  //m_recTree->Branch( "CC05ModuleNumber", &DetNumber, "CC05ModuleNumber/I");
  //m_recTree->Branch( "CC05ModuleModID", DetModID, "CC05ModuleModID[CC05ModuleNumber]/I");
  //m_recTree->Branch( "CC05ModuleEne", DetEne, "CC05ModuleEne[CC05ModuleNumber]/D");
  //m_recTree->Branch( "CC05ModuleTime", DetTime, "CC05ModuleTime[CC05ModuleNumber]/D");

  m_recTree->Branch( "CC05E391ModuleNumber", &E391DetNumber, "CC05E391ModuleNumber/I");
  m_recTree->Branch( "CC05E391ModuleModID", E391DetModID, "CC05E391ModuleModID[CC05E391ModuleNumber]/I");
  m_recTree->Branch( "CC05E391ModuleEne", E391DetEne, "CC05E391ModuleEne[CC05E391ModuleNumber]/F");
  m_recTree->Branch( "CC05E391ModuleTime", E391DetTime, "CC05E391ModuleTime[CC05E391ModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "CC05E391ModuleSmoothness", E391DetSmoothness, "CC05E391ModuleSmoothness[CC05E391ModuleNumber]/F");
  if(m_clusterTree->GetBranch("CC05ModulePSBit"))
	  m_recTree->Branch( "CC05E391ModulePSBit", E391DetPSBit, "CC05E391ModulePSBit[CC05E391ModuleNumber]/S");
  if(m_clusterTree->GetBranch("CC05ModuleAreaR")!=0 ){
	  tr->Branch( "CC05E391ModuleAreaR", E391DetAreaR, "CC05E391ModuleAreaR[CC05E391ModuleNumber]");
  }
  if(m_clusterTree->GetBranch("CC05ModuleWfmCorrectNumber")!=0 ){
	  tr->Branch( "CC05E391ModuleWfmCorrectNumber", E391DetWfmCorrectNumber, "CC05E391ModuleWfmCorrectNumber[CC05E391ModuleNumber]/S");
  }

  m_recTree->Branch( "CC05ScintiModuleNumber", &ScintiDetNumber, "CC05ScintiModuleNumber/I");
  m_recTree->Branch( "CC05ScintiModuleModID", ScintiDetModID, "CC05ScintiModuleModID[CC05ScintiModuleNumber]/I");
  m_recTree->Branch( "CC05ScintiModuleEne", ScintiDetEne, "CC05ScintiModuleEne[CC05ScintiModuleNumber]/F");
  m_recTree->Branch( "CC05ScintiModuleTime", ScintiDetTime, "CC05ScintiModuleTime[CC05ScintiModuleNumber]/F");
  if(m_clusterTree->GetBranch("DetectorWFMCorruptBit"))
	  m_recTree->Branch( "CC05ScintiModuleSmoothness", ScintiDetSmoothness, "CC05ScintiModuleSmoothness[CC05ScintiModuleNumber]/F");
  if(m_clusterTree->GetBranch("CC05ModulePSBit"))
	  m_recTree->Branch( "CC05ScintiModulePSBit", ScintiDetPSBit, "CC05ScintiModulePSBit[CC05ScintiModuleNumber]/S");
  if(m_clusterTree->GetBranch("CC05ModuleAreaR")!=0 ){
	  tr->Branch( "CC05ScintiModuleAreaR", ScintiDetAreaR, "CC05ScintiModuleAreaR[CC05ScintiModuleNumber]");
  }
  if(m_clusterTree->GetBranch("CC05WfmCorrectNumber")!=0 ){
	  tr->Branch( "CC05ScintiModuleWfmCorrectNumber", ScintiDetWfmCorrectNumber, "CC05ScintiModuleWfmCorrectNumber[CC05ScintiModuleNumber]/S");
  }
  
  //m_recTree->Branch( "CC05VetoModID", &CC05VetoModID,"CC05VetoModID/I");
  //m_recTree->Branch( "CC05VetoEne", &CC05VetoEne,"CC05VetoEne/D");
  //m_recTree->Branch( "CC05VetoTime", &CC05VetoTime,"CC05VetoTime/D");

  m_recTree->Branch( "CC05E391VetoModID", &CC05E391VetoModID,"CC05E391VetoModID/I");
  m_recTree->Branch( "CC05E391VetoEne", &CC05E391VetoEne,"CC05E391VetoEne/F");
  m_recTree->Branch( "CC05E391VetoTime", &CC05E391VetoTime,"CC05E391VetoTime/F");

  m_recTree->Branch( "CC05ScintiVetoModID", &CC05ScintiVetoModID,"CC05ScintiVetoModID/I");
  m_recTree->Branch( "CC05ScintiVetoEne", &CC05ScintiVetoEne,"CC05ScintiVetoEne/F");
  m_recTree->Branch( "CC05ScintiVetoTime", &CC05ScintiVetoTime,"CC05ScintiVetoTime/F");  
  MTVeto::Branch(tr);
}


void MTVetoCC05::Initialize( void )
{

  E391DetNumber=0;
  ScintiDetNumber=0;
  
  if(DetWfmCorrupted!=NULL){
 
	  if(!((*DetWfmCorrupted)&(1<<MTBP::CC05))){
		  for(int i=0;i<DetNumber;i++) DetSmoothness[i] = 0; 
	  }
  }
  for( int imod=0; imod<MTBP::CC05NModules; imod++ ){
    vetoHitBuffer[imod].modID = 0;
    vetoHitBuffer[imod].energy = 0;
    vetoHitBuffer[imod].time = 0;
  }

  for(int i=0; i<DetNumber; i++){
    if(DetModID[i]<60){
      E391DetModID[E391DetNumber]=DetModID[i];
      E391DetEne[E391DetNumber]=DetEne[i];
      E391DetTime[E391DetNumber]=DetTime[i];
      E391DetPSBit[E391DetNumber]=DetPSBit[i];
      E391DetSmoothness[E391DetNumber]=DetSmoothness[i];
      E391DetAreaR[E391DetNumber]=DetAreaR[i];
      E391DetWfmCorrectNumber[E391DetNumber]=DetWfmCorrectNumber[i];
      E391DetNumber+=1;
    }
    else {
      ScintiDetModID[ScintiDetNumber]=DetModID[i];
      ScintiDetEne[ScintiDetNumber]=DetEne[i];
      ScintiDetTime[ScintiDetNumber]=DetTime[i];
      ScintiDetSmoothness[ScintiDetNumber]=DetSmoothness[i];
      ScintiDetAreaR[ScintiDetNumber]=DetAreaR[i];
      ScintiDetWfmCorrectNumber[ScintiDetNumber]=DetWfmCorrectNumber[i];
      ScintiDetPSBit[ScintiDetNumber]=DetPSBit[i];
      ScintiDetNumber+=1;
    }
  }

  
  CC05VetoModID = -1;
  CC05VetoEne = 0;
  CC05VetoTime = 9999.;

  CC05E391VetoModID = -1;
  CC05E391VetoEne = 0;
  CC05E391VetoTime = 9999.;

  CC05ScintiVetoModID = -1;
  CC05ScintiVetoEne = 0;
  CC05ScintiVetoTime = 9999.;
    
}


bool MTVetoCC05::Process( int mode ){

    Initialize();

    static const double s_CC05ZPosition = GetDetZPosition("CC05");
     
  for( int imod=0; imod<E391DetNumber; imod++ ){
    vetoTime = E391DetTime[imod] - m_eventStartTime-(s_CC05ZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[imod].modID = E391DetModID[imod];
      vetoHitBuffer[imod].energy = E391DetEne[imod];
      vetoHitBuffer[imod].time = vetoTime;
    }else{
      vetoHitBuffer[imod].modID = E391DetModID[imod];
      vetoHitBuffer[imod].energy = 0;
      vetoHitBuffer[imod].time = vetoTime;
    }
    //std::cout<<imod<<" "<<E391DetModID[imod]<<" "<<E391DetEne[imod]<<" "<<vetoTime<<std::endl;
  }
  std::sort( vetoHitBuffer, &(vetoHitBuffer[E391DetNumber]), pVetoHit::largerHit );

  if( vetoHitBuffer[0].energy>0 && E391DetNumber>0){  
    CC05E391VetoModID = vetoHitBuffer[0].modID;
    CC05E391VetoEne   = vetoHitBuffer[0].energy;
    CC05E391VetoTime  = vetoHitBuffer[0].time;
  } else{
    CC05E391VetoModID = -1;
    CC05E391VetoEne   = 0;
    CC05E391VetoTime  = -9999.;
  }
  int crystalnum=E391DetNumber;
  
  for( int imod=0; imod<ScintiDetNumber; imod++ ){
    vetoTime = ScintiDetTime[imod] - m_eventStartTime-(s_CC05ZPosition-m_eventStartZ)/(TMath::C()/1e6);
    if( IsInVetoWindow( vetoTime )){
      vetoHitBuffer[crystalnum+imod].modID = ScintiDetModID[imod];
      vetoHitBuffer[crystalnum+imod].energy = ScintiDetEne[imod];
      vetoHitBuffer[crystalnum+imod].time = vetoTime;
    }else{
      vetoHitBuffer[crystalnum+imod].modID = ScintiDetModID[imod];
      vetoHitBuffer[crystalnum+imod].energy = 0;
      vetoHitBuffer[crystalnum+imod].time = vetoTime;
    }
    //std::cout<<imod<<" "<<ScintiDetModID[imod]<<" "<<ScintiDetEne[imod]<<" "<<vetoTime<<std::endl;
  }
  std::sort( &(vetoHitBuffer[crystalnum]), &(vetoHitBuffer[crystalnum+ScintiDetNumber]), pVetoHit::largerHit );

  if( vetoHitBuffer[crystalnum].energy>0 && ScintiDetNumber>0){  
    CC05ScintiVetoModID = vetoHitBuffer[crystalnum].modID;
    CC05ScintiVetoEne   = vetoHitBuffer[crystalnum].energy;
    CC05ScintiVetoTime  = vetoHitBuffer[crystalnum].time;
  } else{
    CC05ScintiVetoModID = -1;
    CC05ScintiVetoEne   = 0;
    CC05ScintiVetoTime  = -9999.;
  }

  //std::cout<<"CC05 :"<<vetoHitBuffer[0].modID<<" "<<vetoHitBuffer[0].energy<<" "<<vetoHitBuffer[0].time<<std::endl;
  //std::cout<<"CC05 :"<<vetoHitBuffer[crystalnum].modID<<" "<<vetoHitBuffer[crystalnum].energy<<" "<<vetoHitBuffer[crystalnum].time<<std::endl;  
  //std::getchar();

  if( CC05E391VetoEne > m_vetoThreshold ) return true;
  if( CC05ScintiVetoEne > 1 ) return true;
  
  return false;

}


double MTVetoCC05::GetDetZPosition( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const double DetZPosition = m_E14BP->GetDetZPosition();

  return DetZPosition;
}

std::vector<int> MTVetoCC05::GetDeadModuleIDVec( char* detName ){

  m_E14BP->SetDetBasicParam(m_userFlag,detName);
  const std::vector<int> DeadModuleIDVec = m_E14BP->GetDeadModuleIDVec();

  return DeadModuleIDVec;
}
