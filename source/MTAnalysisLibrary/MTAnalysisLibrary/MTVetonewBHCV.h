#ifndef __MTVETOnewBHCV_H__
#define __MTVETOnewBHCV_H__

#include "MTBHVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

//distributed at 12th Dec, 2013 by Maeda Yosuke
// *TOF correction is added

class MTVetonewBHCV:public MTBHVeto
{
 public:
  MTVetonewBHCV( Int_t userFlag );
  ~MTVetonewBHCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  //  void UpdatedBranch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );

  //  bool IsInWideWindow(Float_t Time){    return (Time > -105.) && (Time < -75.);};
  //  bool IsInNarrowWindow(Float_t Time){  return (Time > -101.) && (Time < -76.);};

  bool IsInWideWindow(Float_t Time, Float_t TMin, Float_t TMax){    return (Time > TMin) && (Time < TMax);};
  bool IsInNarrowWindow(Float_t Time, Float_t TMin, Float_t TMax){    return (Time > TMin) && (Time < TMax);};
  
  Int_t newBHCVVetoModID;
  Float_t newBHCVVetoEne;
  Float_t newBHCVVetoTime;

  Int_t newBHCVHitCount;
  Int_t newBHCVModHitCount;
  
 private:
  Float_t* TOFCorrection;

  pVetoHit* vetoHitBuffer;
  Int_t MaxBufferNum;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOnewBHCV_H__
