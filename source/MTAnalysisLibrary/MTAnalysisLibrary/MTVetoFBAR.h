#ifndef __MTVETOFBAR_H__
#define __MTVETOFBAR_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoFBAR:public MTVeto
{
 public:
  MTVetoFBAR( Int_t userFlag );
  ~MTVetoFBAR();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void BranchOriginalData( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


 private:
  Int_t FBARVetoModID;
  float FBARVetoEne;
  float FBARVetoTime;

  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOFBAR_H__
