#ifndef __MTCLOCKSHIFTCORRECTION_H__
#define __MTCLOCKSHIFTCORRECTION_H__

#include <iostream>
#include <TROOT.h>
#include <TChain.h>
#include <TTree.h>
#include <map>

#include "MTBasicParameters.h"
#include "MTPositionHandler.h"

class MTClockShiftCorrection{
 private:
  TChain* tr;
  double* ClockShift;
  double* AddCorrect;  
  int CorrectFlag[2716];
  int Nevent;
  int NCH;
  int CHID[2716];
  int ShiftVal[2716];
    
  std::map< unsigned int, unsigned int> runEntryMap;

  void SetBranchAddress();

 public:
  MTClockShiftCorrection(int RunID);
  ~MTClockShiftCorrection();
  
  double GetCorrection( int csiID ) const;
  double Correction( int csiID, double time );
  bool SetCorrectionFactor( int spillNo);  
};

#endif // __MTCLOCKSHIFTCORRECTION_H__
