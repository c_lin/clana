#ifndef __MTVETOCBAR_H__
#define __MTVETOCBAR_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoCBAR:public MTVeto
{
 public:
  MTVetoCBAR( Int_t userFlag );
  ~MTVetoCBAR();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void BranchOriginalData( TTree* tr );
  void Initialize( void );
  bool Process( int mode );
  
  Float_t* DetHitZ;


 private:
  float* CBARModuleDeltaTime;

  Int_t CBARVetoModID;
  float CBARVetoEne;
  float CBARVetoTime;
  float CBARVetoHitZ;
  float CBARTotalVetoEne;
  Int_t CBARInnerVetoModID;
  float CBARInnerVetoEne;
  float CBARInnerVetoTime;
  float CBARInnerVetoHitZ;
  Int_t CBAROuterVetoModID;
  float CBAROuterVetoEne;
  float CBAROuterVetoTime;
  float CBAROuterVetoHitZ;

  pVetoHit* vetoHitBuffer;

  float CBARCH21Ene;
  float CBARCH21Time;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};


#endif // __MTVETOCBAR_H__
