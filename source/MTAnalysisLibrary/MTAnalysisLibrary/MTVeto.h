#ifndef __MTVETO_H__
#define __MTVETO_H__

#include <iostream>
#include "TROOT.h"
#include <string>
#include "TTree.h"

#include "klong/Klong.h"
#include "pi0/Pi0.h"
#include "gnana/E14GNAnaDataContainer.h"
#include "MTBasicParameters.h"


struct pVetoHit{
  int modID;
  float energy;
  float time;
  float position;
  static bool fasterHit( const pVetoHit& et0, const pVetoHit& et1 ){ return et0.time < et1.time; }
  static bool laterHit( const pVetoHit& et0, const pVetoHit& et1 ){ return et0.time > et1.time; }
  static bool largerHit( const pVetoHit& et0, const pVetoHit& et1 ){ return et0.energy > et1.energy; }
  static bool smallerHit( const pVetoHit& et0, const pVetoHit& et1 ){ return et0.energy < et1.energy; }
};



class MTVeto
{
 public:
  MTVeto( void );
  ~MTVeto( void );


  virtual void SetBranchAddress( TTree* tr );
  void SetDataContainer( E14GNAnaDataContainer* data ) { m_data = data; };
  virtual void Branch( TTree* tr );
  virtual void BranchOriginalData( TTree* tr ){};
  virtual void Initialize( void );
  virtual bool Process( int mode=0 ) = 0;
  virtual bool Process( int mode, bool& vetoFlag, bool& looseVetoFlag, bool& tightVetoFlag ){};

  void SetEventStartInfo( double eventStartZ, double eventStartTime );  
  void SetEventStartZ( double z );
  double GetEventStartZ( void );
  void SetEventStartTime( double t );
  double GetEventStartTime( void );
  virtual void SetKlInfo( Klong* kl ) {}; // for MTVetoCSI only so far.
  virtual void SetPi0Info( Pi0* pi0 ) {}; // for MTVetoCSI only so far.
  void SetVetoWindow( double windowLength, double windowOffset );
  void GetVetoWindow( double& windowLength, double& windowOffset );
  double GetVetoStartTime( void );
  double GetVetoEndTime( void );
  void SetVetoThreshold( double eThreshold );
  void SetLooseVetoThreshold( double eThreshold );
  void SetTightVetoThreshold( double eThreshold );
  double GetVetoThreshold( void );
  double GetLooseVetoThreshold( void );
  double GetTightVetoThreshold( void );

  void SetClusteringTree(TTree* tree) { m_clusterTree=tree;}

  void SetTimeOffset(double offset);
 
  std::string DetName; 
  Int_t* DetWfmCorrupted;
  Int_t DetNumber;
  Int_t* DetModID;
  Float_t* DetTime;
  Float_t* DetEne;
  Float_t* DetSmoothness;
  Float_t* DetAreaR;
  short  * DetWfmCorrectNumber;
  Long64_t DetEtSum[MTBP::nSample125];//added at 16th Mar, 2014

  Float_t (*DetSuppWfm125)[MTBP::nSample125];
  int      DetSuppWfmNumber;
  int     *DetSuppWfmModID;
  



  ///// only CBAR and FBAR use the variables below so far
  Bool_t   CopyDstBranchFlag;
  Int_t    DetDstNumber;
  Int_t   *DetDstModID;
  Float_t *DetDstIntADC;
  Float_t *DetDstEne;
  Float_t *DetDstTime;
  Float_t *DetDstPTime;
  Float_t *DetDstFallTime;
  Float_t *DetDstSmoothness;
  short   *DetDstWfmCorrectNumber;
  Short_t *DetPSBit;


 protected:
  TTree* m_clusterTree;
  TTree* m_recTree;
  E14GNAnaDataContainer* m_data;
  double m_eventStartZ;
  double m_eventStartTime;
  double m_vetoWindowLength;
  double m_vetoWindowOffset;
  double m_vetoStartTime;
  double m_vetoEndTime;
  double m_vetoThreshold;
  double m_looseVetoThreshold;
  double m_tightVetoThreshold;

  float vetoEne;
  float vetoTime;

  bool IsInVetoWindow( double t );

  void MakeWfmArray();

};


#endif //__MTVETO_H__
