#ifndef __MTFUNCTION_H__
#define __MTFUNCTION_H__
#include <iostream>



struct pHitInfo{
  int modID;
  float energy;
  float time;
  static bool smallerID( const pHitInfo& hitInfo0, const pHitInfo& hitInfo1 ){ return hitInfo0.modID < hitInfo1.modID; }
  static bool fasterHit( const pHitInfo& hitInfo0, const pHitInfo& hitInfo1 ){ return hitInfo0.time < hitInfo1.time; }
  static bool laterHit( const pHitInfo& hitInfo0, const pHitInfo& hitInfo1 ){ return hitInfo0.time > hitInfo1.time; }
  static bool largerHit( const pHitInfo& hitInfo0, const pHitInfo& hitInfo1 ){ return hitInfo0.energy > hitInfo1.energy; }
  static bool smallerHit( const pHitInfo& hitInfo0, const pHitInfo& hitInfo1 ){ return hitInfo0.energy < hitInfo1.energy; }
};



namespace MTFUNC{
  Double_t langau( Double_t *x, Double_t *par);
  Double_t asymgau( Double_t *x, Double_t *par);
  Double_t WeightedAverage( int n, Double_t *values, Double_t *weights );
  Double_t TResolutionOfCluster( double e ); // [ns_sigma] <- [MeV]
  Double_t Distance( double x0, double x1, double y0, double y1, double z0, double z1 );
}


#endif //__MTFUNCTION_H__
