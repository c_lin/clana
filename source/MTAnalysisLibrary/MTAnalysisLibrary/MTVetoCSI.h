#ifndef __MTVETOCSI_H__
#define __MTVETOCSI_H__

#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"
#include "MTPositionHandler.h"


class MTVetoCSI:public MTVeto
{
 public:
  MTVetoCSI( Int_t userFlag );
  ~MTVetoCSI();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  //void BranchOriginalData( TTree* tr );
  bool Process( int mode );
  bool Process( int mode, bool& vetoFlag, bool& looseVetoFlag, bool& tightVetoFlag );  

  void SetKlInfo( Klong* kl );
  void SetPi0Info( Pi0* pi0 );




 private:
  MTPositionHandler* pHandler;
  Klong* m_kl;
  Pi0* m_pi0;

  Int_t ClusterNumber;
  Int_t* ClusterSize;
  Double_t (*ClusterCsiId)[120];

  Int_t* pUTCNumber;
  Int_t* UTCModID;
  Int_t* ptrUTCModID;
  Float_t* UTCSmoothness;
  Float_t* CSISmoothness;
  


  Int_t CSIWfmCorrptedNumber;
  Int_t* CSIWfmCorruptedModID;
  Float_t* CSIWfmCorruptedSmoothness;

  Int_t UTCWfmCorrptedNumber;
  Int_t* UTCWfmCorruptedModID;
  Float_t* UTCWfmCorruptedSmoothness;

  Int_t CSIVetoNumber;
  Int_t* CSIVetoModID;
  
  Double_t* CSIVetoEne;
  Double_t* CSIVetoDistance;
  Double_t* CSIVetoDeltaTime;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );
  
  Int_t  DetModNumber;
  Int_t  CSIPSModNumber;
  Int_t* CSIPSModID;

  void SetSuppressedIDs(void);
  void FillWfmCorruptedInfo(void);

  const Int_t m_userFlag;

};




#endif //__MTVETOCSI_H__
