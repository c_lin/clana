#ifndef __MTVETOCV_H__
#define __MTVETOCV_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoCV:public MTVeto
{
 public:
  MTVetoCV( Int_t userFlag );
  ~MTVetoCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void BranchOriginalData( TTree* tr );
  void Initialize( void );
  bool Process( int mode );
  
  Float_t (*CVModuleRiseTime)[2];//added at 4th May, 2014 by Maeda Yosuke

  float* CVModuleDeltaTime;
  float CVVetoEne;
 private:

  Int_t CVVetoModID;
  float CVVetoTime;
  float CVTotalVetoEne;
  Int_t CVFrontVetoModID;
  float CVFrontVetoEne;
  float CVFrontVetoTime;
  float CVFrontTotalVetoEne;
  Int_t CVRearVetoModID;
  float CVRearVetoEne;
  float CVRearVetoTime;
  float CVRearTotalVetoEne;

  pVetoHit* vetoHitBuffer;
  
  Float_t CVMaxPulseWidth;//added at 4th May, 2014 by Maeda Yosuke
  
  // geometory for each strip
  float* x_min;
  float* x_max;
  float* x_center;
  float* y_min;
  float* y_max;
  float* y_center;
  float* z_min;
  float* z_max;
  float* z_center;
  Int_t*  quadrant;
  Int_t*  direction; // 0:Short is +x, 1:Short is +y, 2:Short is -x, 3:Short is -y



  void ReadGeometory( void );

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetCVFZPosition();
  double GetCVRZPosition();
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOCV_H__
