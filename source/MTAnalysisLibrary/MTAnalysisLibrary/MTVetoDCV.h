#ifndef __MTVETODCV_H__
#define __MTVETODCV_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoDCV:public MTVeto
{
 public:
  MTVetoDCV(int userflag);
  ~MTVetoDCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void BranchOriginalData( TTree* tr );
  void Initialize( void );
  bool Process( int mode );
  bool Process2( int mode, int userFlag );

  double GetDetZPosition( char* detName );
  E14BasicParamManager::E14BasicParamManager *m_E14BP;


 private:
  Int_t DCVVetoModID;
  float DCVVetoEne;
  float DCVVetoTime;
  float DCVVetoDeltaTime;
  
  Int_t DCVSideVetoModID;
  float DCVSideVetoEne;
  float DCVSideVetoTime;
  float DCVSideVetoDeltaTime;

  int DCVScintiNumber;
  int* DCVScintiModID;
  float* DCVScintiEne;
  float* DCVScintiTime;
  float* DCVScintiDeltaTime;

  int    DCVSideNumber;
  int*   DCVSideModID;
  float* DCVSideEne;
  float* DCVSideTime;
  float* DCVSideDeltaTime;

  pVetoHit* vetoHitBuffer;
  pVetoHit* vetoHitBuffer2;
  
  const Int_t m_userFlag;

};







#endif //__MTVETODCV_H__
