#ifndef __MTVETOCC06_H__
#define __MTVETOCC06_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoCC06:public MTVeto
{
 public:
  MTVetoCC06( Int_t userFlag );
  ~MTVetoCC06();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


 private:
  Int_t CC06VetoModID;
  float CC06VetoEne;
  float CC06VetoTime;

  Int_t CC06E391VetoModID;
  float CC06E391VetoEne;
  float CC06E391VetoTime;

  Int_t CC06ScintiVetoModID;
  float CC06ScintiVetoEne;
  float CC06ScintiVetoTime;

  int E391DetNumber;
  int* E391DetModID;
  float* E391DetEne;
  float* E391DetTime;
  float* E391DetSmoothness;
  float* E391DetAreaR;
  short* E391DetWfmCorrectNumber;
  short* E391DetPSBit;

  int ScintiDetNumber;
  int* ScintiDetModID;
  float* ScintiDetEne;
  float* ScintiDetTime;
  float* ScintiDetSmoothness;
  float* ScintiDetAreaR;
  short* ScintiDetWfmCorrectNumber;
  short* ScintiDetPSBit;
  
  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOCC06_H__
