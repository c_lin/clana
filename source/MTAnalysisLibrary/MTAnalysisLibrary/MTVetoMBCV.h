#ifndef __MTVETOMBCV_H__
#define __MTVETOMBCV_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoMBCV:public MTVeto
{
 public:
  MTVetoMBCV( Int_t userFlag );
  ~MTVetoMBCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


 private:
  Int_t MBCVVetoModID;
  float MBCVVetoEne;
  float MBCVVetoTime;

  float MBCVCH2VetoEne;
  float MBCVCH2VetoTime;

  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOMBCV_H__
