#ifndef __MTTEMPCORRECTION_H__
#define __MTTEMPCORRECTION_H__

#include <iostream>
#include <TROOT.h>
#include <TChain.h>
#include <map>

#include "MTBasicParameters.h"
#include "MTPositionHandler.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTTempCorrection{
 private:
  TChain* tr;
  TChain* basetr;
  int RunID;
  const int baseRunID;
  const Int_t m_RunID;
  unsigned int TrigDataStartTime;
  unsigned int TempMonStartTime;
  double* Temperature;
  double* BaseTemperature;
  bool multipleFlag;
  std::map< unsigned int, unsigned int> runEntryMap;

  MTPositionHandler* pHandler;

  void SetBranchAddress( void );

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  int GetUserFlag( int runID ) const;

 public:
  MTTempCorrection( int runID );
  MTTempCorrection( int startRunID, int endRunID );
  ~MTTempCorrection();

  bool IsExist( int run ) const;
  bool SetRunID( int run );

  unsigned int GetTempUpChannel( int csiId ) const;
  unsigned int GetTempDownChannel( int csiId ) const;
  unsigned int GetTempUpChannel( double x, double y ) const;
  unsigned int GetTempDownChannel( double x, double y ) const;

  double GetTemperature( int tempChannel ) const;
  double GetBaseTemperature( int tempChannel ) const;
  double GetTemperature( int run, int tempChannel );
  double GetCorrectionFactor( int tempChannel ) const;
  double GetCorrectionFactor( int run, int tempChannel );

  void Correction( double* energy );
  double Correction( int csiID, double energy );

  unsigned int GetTrigDataStartTime() const { return TrigDataStartTime;};
  unsigned int GetTempMonStartTime() const { return TempMonStartTime;};
    
};



#endif // __MTTEMPCORRECTION_H__
