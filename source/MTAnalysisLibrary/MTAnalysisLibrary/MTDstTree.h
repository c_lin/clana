#ifndef __MTDSTTREE_H__
#define __MTDSTTREE_H__


#include <TROOT.h>
#include <TChain.h>
#include <TFile.h>
#include <set>
#include "MTBasicParameters.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTDstTree{
 private:
  static const int nMaxChannels = 2716+256;
  TFile* tf;
  bool tfAllocationFlag; // If tf is new, it is set true
  TTree* T;
  int Allocation( void );

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  std::vector<int> GetDeadChannelIDVec( int userFlag, char* detName );

  std::set< int > CSIDeadChannelSet;

  int typeL2Trig;
  int typeSpillNo;

  float CSISmoothness_asModID[nMaxChannels];
  float CSIProdDer2_asModID[nMaxChannels];

 public :
  
  
  void* GetL2TrigPtr(std::string& strL2trig);
  void* GetSpillNoPtr(std::string& strSpillNo);
  int GetSpillNo();
  int GetL2TrigNo();
  
  
  Int_t           EventNo;
  Short_t         ExtTrigType;
  Short_t         DeltaTrig;
  Int_t           L1TrigNo;
  Short_t         L2TrigNo;
  Int_t           L2TrigNo2019;
  Short_t         SpillNo;
  Int_t           SpillNo2019;
  Int_t           TimeStamp;
  UInt_t          L2TimeStamp;
  Short_t         Error;
  Short_t         TrigTagMismatch;
  Short_t         L2AR;
  Short_t         COE_Et_overflow;
  Short_t         COE_L2_override;
  Int_t           COE_Esum;
  Int_t           COE_Ex;
  Int_t           COE_Ey;
  UInt_t          DetectorBit;
  UInt_t          RawTrigBit;
  UInt_t          ScaledTrigBit;
  Int_t           LeftEt;
  Int_t           LeftEt_raw;
  Int_t           RightEt;
  Int_t           RightEt_raw;


  Int_t CSIWfm_corrupted;
  Int_t UTCWfm_corrupted;
  Float_t CSIWfm_ProdDer2threshold;
  Float_t CSIWfm_Smoothnessthreshold;
  Float_t UTCWfm_ProdDer2threshold;
  Float_t UTCWfm_Smoothnessthreshold;

  int             userflag;

  Int_t           *DetNumber;
  Int_t           (*DetModID)[nMaxChannels];
  Short_t         (*DetPeak)[nMaxChannels];
  Float_t         (*DetPedestal)[nMaxChannels];
  Float_t         (*DetIntegratedADC)[nMaxChannels];
  Float_t         (*DetEne)[nMaxChannels];
  Float_t         (*DetInitialTime)[nMaxChannels];
  Float_t         (*DetTime)[nMaxChannels];
  Long64_t        (*DetEtSum)[64];
  Short_t         (*DetPSbit)[nMaxChannels];
  Float_t         (*DetSmoothness)[nMaxChannels];
  Float_t         (*DetProdDer2)[nMaxChannels];

  Int_t           CDTNum;
  Short_t         CDTData[3][64];
  Short_t         CDTBit[38][38];

  Short_t         CDTVetoData[4][64];
  Short_t         CDTVetoBitSum[13];

  bool RunMCFlag;

  MTDstTree( TTree* tr, bool runMCFlag, Int_t userFlag);
  MTDstTree( const char* filename, bool runMCFlag, Int_t userFlag );
  ~MTDstTree();

  void Initialize( Int_t userFlag );
  Int_t GetEntry( Long64_t entry = 0, int apply_PS=0);
  void SetBranchAddress( void ); 
  void SetBranchStatus( const char* bname, Bool_t status = 1);

  short GetCSIPSBit(int ModID);
  short GetUTCPSBit(int ModID){ return GetCSIPSBit(ModID+2716);}

  float GetCSISmoothness(int ModID);
  float GetUTCSmoothness(int ModID){ return GetCSISmoothness(ModID+2716);}
  
  float GetCSIProdDer2(int ModID);
  float GetUTCProdDer2(int ModID){ return GetCSIProdDer2(ModID+2716);}



  // TRIGGERTAG information
  bool IsClock( void );
  bool IsLaser( void );
  bool IsLED( void );
  bool IsMBCR( void );
  bool IsFBCR( void );
  bool IsCSICR( void );
  bool IsTRIGGERTAG( void );

  // If 0th bit of useinitialdata is set, Integrated ADC is used instead of calibrated one.
  // If 1st bit of useinitialdata is set, Initial time is used instead of calibrated one.
  void GetData( const int detID, int& number, int* modID, double* ene, double* time, int useinitialdata = 0 );
  void GetCSIData(int& number, int* modID, double* ene, double* time , int useinitialdata = 0);
  void GetCSIDataWithoutDeadChannels(int& number, int* modID, double* ene, double* time, int useinitialdata = 0 );
  void GetCSIEtSum(Long64_t* EtSum);
  Int_t CheckCSIWfmCorruption();

  int ModID2Index(int detID, int ModID);

  // by checking PS bit, apply PS
  void PedestalSuppression(int apply_PS);

};

#endif //__MTDSTTREE_H__
