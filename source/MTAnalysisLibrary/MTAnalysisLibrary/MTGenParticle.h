#ifndef __MTGENPARTICLE_H__
#define __MTGENPARTICLE_H__

#include <iostream>
#include "TROOT.h"
#include "TTree.h"





class MTGenParticle
{
 public:
  MTGenParticle( void );
  ~MTGenParticle();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );


  // GenParticle information
  static const int MaxGenParticleN = 256;
  Int_t GenParticleNumber;
  Int_t* GenParticleTrack;
  Int_t* GenParticleMother;
  Int_t* GenParticlePid;
  //Int_t* GenParticleMotherPid;
  Double_t (*GenParticleMom)[3];
  Double_t* GenParticleEk;
  Double_t* GenParticleMass;
  Double_t* GenParticleTime;
  Double_t (*GenParticlePos)[3];
  Double_t (*GenParticleEndMom)[3];
  Double_t* GenParticleEndEk;
  Double_t* GenParticleEndTime;
  Double_t (*GenParticleEndPos)[3];

 private:
  TTree* m_itr;
  TTree* m_otr;

};


#endif //__MTGENPARTICLE_H__
