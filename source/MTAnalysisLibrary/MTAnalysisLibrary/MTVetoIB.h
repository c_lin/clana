#ifndef __MTVETOIB_H__
#define __MTVETOIB_H__

#include "MTBHVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTVetoIB:public MTBHVeto
{
 public:
  MTVetoIB( Int_t userFlag );
  ~MTVetoIB();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );
  
  Int_t   IBVetoModID;
  Float_t IBTotalVetoEne;
  Float_t IBVetoEne;
  Float_t IBVetoTime;
  Float_t IBVetoHitZ;

  Int_t     ModuleNumber;
  Int_t*    ModuleModID;
  Float_t (*ModuleTime);
  Float_t (*ModuleEne);
  Float_t (*ModuleHitZ);

  Int_t   IBWideVetoModID;
  Float_t IBWideVetoEne;
  Float_t IBWideVetoTime;
  
  Float_t IBCH55VetoEne;
  Float_t IBCH55VetoTime;

 private:

  float GetHitZ( float frontTime, float rearTime );
  float GetHitTime( float frontTime, float rearTime );
  float GetEne( float frontEne,  float rearEne,
		float frontTime, float rearTime);

  float* IBModuleDeltaTime;
  Float_t* TOFCorrection;

  pVetoHit* vetoHitBuffer;
  Int_t MaxBufferNum;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};


#endif //__MTVETOIB_H__
