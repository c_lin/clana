#ifndef __MTVETOCC03_H__
#define __MTVETOCC03_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoCC03:public MTVeto
{
 public:
  MTVetoCC03( Int_t userFlag );
  ~MTVetoCC03();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


 private:
  Int_t CC03VetoModID;
  float CC03VetoEne;
  float CC03VetoTime;
  float CC03TotalVetoEne;

  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOCC03_H__
