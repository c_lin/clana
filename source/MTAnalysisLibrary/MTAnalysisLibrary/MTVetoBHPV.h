#ifndef __MTVETOBHPV_H__
#define __MTVETOBHPV_H__

#include "MTBHVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

//distributed at 12th Dec, 2013 by Maeda Yosuke
// *coincidence analysis is implemented

class MTVetoBHPV:public MTBHVeto
{
 public:
  MTVetoBHPV( Int_t userFlag );
  ~MTVetoBHPV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  //  void UpdatedBranch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );

  //coincidence info. with largest nModules in veto window
  Int_t BHPVVetoModID;//start channel ID (e14ID)
  Int_t BHPVVetonHitMod;//# of coincidence modules (when >=3, the event is vetoed.)
  Float_t BHPVVetoEne;//total light yield[p.e.]
  Float_t BHPVVetoTime;//coincidence timing (average timing for each channel after TOF correction)
  //all module coincidence information (for hits with timing in given veto center timing +/- 30ns)
  Int_t BHPVCoinNumber;
  Int_t* BHPVCoinModID;
  Int_t* BHPVCoinnHitMod;
  Float_t* BHPVCoinEne;
  Float_t* BHPVCoinTime;
  Float_t* BHPVCoinTimeSpread;//StdDev of timing for each channel in coincidence
  
  Float_t* TOFCorrection;
  
  pVetoHit* vetoHitBuffer;
  Int_t MaxBufferNum;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};

#endif //__MTVETOBHPV_H__
