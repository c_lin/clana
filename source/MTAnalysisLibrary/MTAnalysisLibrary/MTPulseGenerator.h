#ifndef __MTPULSEGENERATOR_H__
#define __MTPULSEGENERATOR_H__

#include <iostream>
#include <vector>
#include "TF1.h"

struct pEnergyTime{
  double energy;
  double time;
  static bool fasterHit(  const pEnergyTime& et0, const pEnergyTime& et1 ){ return et0.time   < et1.time; }
  static bool laterHit(   const pEnergyTime& et0, const pEnergyTime& et1 ){ return et0.time   > et1.time; }
  static bool largerHit(  const pEnergyTime& et0, const pEnergyTime& et1 ){ return et0.energy > et1.energy; }
  static bool smallerHit( const pEnergyTime& et0, const pEnergyTime& et1 ){ return et0.energy < et1.energy; }
};

class MTPulseGenerator
{
 public:

  double* pulseShape;
  int* quantizedPulseShape;  // 8ns sampling

  MTPulseGenerator();
  ~MTPulseGenerator();  
  void SetEnergyTime( int n, double* e, double* t );
  void SetEnergyTime( int n, pEnergyTime* et );
  void SetEnergyTime( std::vector< pEnergyTime > etVec );

  double GetPseudoIntegratedEnergy( double minTime=-1, double maxTime=-1 );
  double GetPseudoConstantFractionTime( double minTime=-1, double maxTime=-1 );

  void GeneratePulseShape( void );
  double GetPeakTime( void ) const;
  double GetConstantFractionTime( void ) const;
  double GetPeakHeight( void ) const;
  double GetIntegrated( void ) const;
  double GetQuantizedPeakTime( void ) const;
  double GetQuantizedConstantFractionTime( void ) const;
  double GetQuantizedPeakHeight( void ) const;
  double GetQuantizedIntegrated( void ) const;

 private:
  static const double peakConstantDiff;//defined in .cc file
  static const double timePitch;//defined in .cc file
  static const long nPoints = 200000; // = 2000 ns window
  int nQuantizedPoints;
  std::vector< pEnergyTime > m_etVec;

  static double* originalPulseShape;

  void GenerateOriginalPulseShape( void );




};






#endif //__MTPULSEGENERATOR_H__
