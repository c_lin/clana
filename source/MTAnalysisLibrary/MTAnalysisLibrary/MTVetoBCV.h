#ifndef __MTVETOBCV_H__
#define __MTVETOBCV_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"

class MTVetoBCV:public MTVeto
{
 public:
  MTVetoBCV( Int_t userFlag );
  ~MTVetoBCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );

  Float_t* DetHitZ;
  
 private:
  Int_t BCVVetoModID;
  float BCVVetoEne;
  float BCVVetoTime;
  float BCVVetoHitZ;
  float* BCVModuleDeltaTime;
  
  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOBCV_H__
