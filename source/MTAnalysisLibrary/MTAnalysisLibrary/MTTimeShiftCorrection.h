#ifndef __MTTIMESHIFTCORRECTION_H__
#define __MTTIMESHIFTCORRECTION_H__

#include <iostream>
#include <TROOT.h>
#include <TChain.h>
#include <TTree.h>
#include <map>

#include "MTBasicParameters.h"
#include "MTPositionHandler.h"



class MTTimeShiftCorrection{
 private:
  
  double* TimeShift;


 public:
  MTTimeShiftCorrection(int RunID);
  ~MTTimeShiftCorrection();

  double GetCorrection( int csiID ) const;
  double Correction( int csiID, double time );
    
};



#endif // __MTTIMESHIFTCORRECTION_H__
