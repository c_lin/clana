#ifndef __MTVETOLCV_H__
#define __MTVETOLCV_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoLCV:public MTVeto
{
 public:
  MTVetoLCV( Int_t userFlag );
  ~MTVetoLCV();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


 private:
  Int_t LCVVetoModID;
  float LCVVetoEne;
  float LCVVetoTime;

  pVetoHit* vetoHitBuffer;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

};







#endif //__MTVETOLCV_H__
