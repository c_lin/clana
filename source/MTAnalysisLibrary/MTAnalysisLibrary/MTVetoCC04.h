#ifndef __MTVETOCC04_H__
#define __MTVETOCC04_H__


#include "MTVeto.h"
#include "E14BasicParamManager/E14BasicParamManager.h"


class MTVetoCC04:public MTVeto
{
 public:
  MTVetoCC04( Int_t userFlag );
  ~MTVetoCC04();

  void SetBranchAddress( TTree* tr );
  void Branch( TTree* tr );
  void Initialize( void );
  bool Process( int mode );


 private:
  Int_t CC04VetoModID;
  float CC04VetoEne;
  float CC04VetoTime;

  Int_t CC04E391VetoModID;
  float CC04E391VetoEne;
  float CC04E391VetoTime;

  Int_t CC04KTeVVetoModID;
  float CC04KTeVVetoEne;
  float CC04KTeVVetoTime;

  Int_t CC04ScintiVetoModID;
  float CC04ScintiVetoEne;
  float CC04ScintiVetoTime;
  
  pVetoHit* vetoHitBuffer;


  int E391DetNumber;
  int* E391DetModID;
  float* E391DetEne;
  float* E391DetTime;
  float* E391DetSmoothness;
  float* E391DetAreaR;
  short* E391DetWfmCorrectNumber;
  short* E391DetPSBit;

  int KTeVDetNumber;
  int* KTeVDetModID;
  float* KTeVDetEne;
  float* KTeVDetTime;
  float* KTeVDetSmoothness;
  float* KTeVDetAreaR;
  short* KTeVDetWfmCorrectNumber;
  short* KTeVDetPSBit;

  int ScintiDetNumber;
  int* ScintiDetModID;
  float* ScintiDetEne;
  float* ScintiDetSmoothness;
  float* ScintiDetAreaR;
  short* ScintiDetWfmCorrectNumber;
  float* ScintiDetTime;
  short* ScintiDetPSBit;

  E14BasicParamManager::E14BasicParamManager *m_E14BP;
  double GetDetZPosition( char* detName );
  std::vector<int> GetDeadModuleIDVec( char* detName );

  const Int_t m_userFlag;

  
};







#endif //__MTVETOCC04_H__
