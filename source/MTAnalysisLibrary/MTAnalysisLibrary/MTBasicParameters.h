#ifndef __MTBASICPARAMETERS_H__
#define __MTBASICPARAMETERS_H__
#include <iostream>
#include <string>
#include <TMath.h>


namespace MTBP{

  typedef enum { UCV=0, UCVLG, CC03,    CC04, CC05,
		 CC06,   CBAR, FBAR,    CSI,  BHPV,
		 CV,     BCV,  BHCV,    NCC,  OEV,
		 LCV,    BPCV, newBHCV, BHGC, IB,
		 IBCV,   MBCV,
		 COSMIC, LASER,  ETC,  TRIGGERTAG, DCV } DetectorID;

  const std::string EnvName = "MY_TOP_DIR";

  const int nDetectors = 27;
  const std::string detectorName[nDetectors] =
    { "UCV", "UCVLG", "CC03",    "CC04", "CC05",
      "CC06", "CBAR", "FBAR",    "CSI",  "BHPV", 
      "CV",   "BCV",  "BHCV",    "NCC",  "OEV",
      "LCV",  "BPCV", "newBHCV", "BHGC", "IB",
      "IBCV", "MBCV",
      "COSMIC", "LASER", "ETC", "TRIGGERTAG", "DCV" };

  const int DetNChannels[nDetectors] =
    { 12,   2,    32,  64,   60,
      60,   128,  32,  2716, 34,
      184,  64,   8,   204,  44,
      4,    4,    48,  8,    64,
      64,   16,
      16,   4,    0,   6, 32 };  
  const int DetNModules[nDetectors] =
    { 12,    2,    16,  62,   58,
      58,   64,   32,  2716, 34,
      92,   32,   8,   204,  44,
      4,    4,    48,  4,    32,
      32,   16,
      16,   4,    0,   6,  32};

  const Bool_t is125MHzDetector[nDetectors] = 
    { false, true,  true,  true,  true,
      true,  true,  true,  true,  false,
      true,  true,  false, true,  true,
      true,  true,  false, false, false,
      true,  true,
      true,  true,  true,  true, true};
  const Bool_t is500MHzDetector[nDetectors] = 
    { true, false, false, false, false,
      false, false, false, false, true,
      false, false, true,  false, false,
      false, false, true,  true,  true,
      false, false,
      false, false, false, false, false };

  /*  const Float_t AccTimeShift[nDetectors] =
    { 0,     0,     9.6,    -14.6, -39.0,
      -38.5, 18.8,  19.5,   0,     -64.0,
      27.6,  22.5,  -79.0,  1.8,   10.2,
      -6.2,  0,     -78.8,  -65.0, -20.0,
      0,     0,
      0, 0, 0, 0};*/
  /* const Float_t AccTimeShift[nDetectors] =
    { 0,     0,     8.0,    -16.0, -39.0,
      -38.5, 16.5,  19.5,   0,     -64.0,
      26.5,  22.5,  -79.0,  1.8,   10.2,
      -8.5,  0,     -78.8,  -65.,  -20.0,
      0,     0,
      0, 0, 0, 0}; */
  /*  const Float_t AccTimeShift[nDetectors] =
    { 0,     0,       40.487,  15.40,   -13.1,
      -10.6, 51.34,  49.09,   30.6098, -67.3,
      65.46, 50.89,  -82,      34.059,  27.02,
      24.91, 25.44,  -81.2,   -64.7,    -20.0,
      29.29, 29.29,     
      0, 0, 0, 0}; // change by shiomi 2017/11/30*/
  const Float_t AccTimeShift[nDetectors] =
    { -40,   -40,       40.487,  15.40,   -13.1,
      -10.6, 51.34,  49.09,   30.6098, -67.3,
      65.46, 50.89,  -82,      34.059,  27.02,
      24.91, 25.44,  -81.2,   -64.7,    -29.5,
      29.99, 20.29,     
      0, 0, 0, 0, 0}; // change by shinohara 2018/05/17 for IB, IBCV, and MBCV
  
  const Float_t VetoEneThre[nDetectors] =
    { 0.05, 0.05,  3.,  3.,   3.,
      3.,  2., 2.,        0.,   2.5,
      0.2, 1., 0.3,       2.,   2.,
      0.6, 1., 884.e-6/4, 2.5,  2.,
      1.,  1.,
      0,   0,  0 , 0, 0.2};

  /*
    const Float_t VetoTiming[nDetectors] =
    { 0,     0,    33.4,   4.4,   -19.2,
      -18.5, 40.7, 35.4,   0,     -74.6,
      54.6,  40.6, -92.4,  14.9,  19.9,
      13.9,  18.,  0,      -76.,  -40.0,
      15.0,  10.0,
      0,     0,    0,      0 };
  */
  /*
  const Float_t VetoTiming[nDetectors] =
    { 0,     0,    32.2,   4.31,   -24.3,
      -21.8, 38.8, 35.08,   0,     -76.2,
      53.7,  37.9,-94.5,   9.32,   19.5,
      14.9,  21.4,  0,    -77.4,  -40.0,
      15.0,  10.0,
      0,     0,    0,      0 }; // change by shiomi 2017/11/30 for 2015 run
  */
  const Float_t VetoTiming[nDetectors] =
    { -38.0, 32.0,  32.2,   4.31,   -24.3,
      -21.8, 38.8, 35.08,   0,     -76.2,
      53.7,  37.9,-94.5,   9.32,   19.5,
      14.9,  21.4,  0,    -77.4,  -32.4,
      10.0,  19.4,
      0,     0,    0,      0,  31.66}; // change by shiomi 2017/11/30 for 2015 run. changed IB, IBCV, and MBCV by shinohara 2018/05/17

  const Float_t VetoWidth[nDetectors] =
    { 30.*2, 15.*2, 15.*2, 15.*2, 15.*2,
      15.*2, 30.*2, 20.*2, -1.,   7.5*2,
      40.*2, 30.*2, 7.5*2, 20.*2, 10.*2,
      15.*2, 12.*2, -1,    7.5*2, 30.*2,
      30.*2,  30.*2, 
      -1,    -1,    -1 ,    -1, 30*2};

  const std::string GsimE14DetName[nDetectors] =
    { "UCV", "UCVLG", "CC03",    "CC04", "CC05",
      "CC06", "CBAR", "FBAR",    "CSI",  "BHPV", 
      "CV",   "BCV",  "BHCV",    "NCC",  "OEV",
      "LCV",  "BPCV", "NewBHCV", "BHGC", "IBAR",
      "IBCV", "MBCV",
      "COSMIC", "LASER", "ETC", "TRIGGERTAG", "DCV" };

  //  const std::string CalibFileDir = "/home/had/shiomi/work2/beam2015/CheckPro3/MakeCalibFile_27kW/";//modified at 13rd June, 2016 by K. Shiomi
  //const std::string CalibFileDir = "/home/had/shiomi/work2/beam2015/CheckPro2/MakeCalibFile/";//added at 17rd Feb, 2016 by K. Shiomi
  //const std::string CalibFileDir = "/home/had/shiomi/work2/beam2015/CheckPro3/MakeCalibFile/";//modified at 13rd June, 2016 by K. Shiomi
  //const std::string CalibFileDir = "/home/had/shiomi/work2/beam2015/CheckPro3/MakeCalibFile_27kW/";//modified at 13rd June, 2016 by K. Shiomi
  //  const std::string CalibFileDir = "/sw/koto/production/run69/pro0/Fiber/calib";//modified at 26th June, 2016 by M.Togawa
  //const std::string CalibFileDir = "/home/had/togawa/work/MyAna/run69/calib/calib";//modified at 2016/08/31 by M.Togawa
  //const std::string CalibFileDir = "/home/had/shiomi/work2/beam2017_04/CheckPro0/MakeCalibFile/";//modified at 28th April, 2017 by K. Shiomi
  //const std::string CalibFileDir = "/home/had/shiomi/work2/beam2017_04/CheckPro0/MakeCalibFile_Run25596/";//modified at 25th Jun, 2017 by K. Shiomi
  //const std::string CalibFileDir = "/home/had/shiomi/work2/beam2017_04/CheckPro0/MakeCalibFile_Run25711/";//modified at 26th Jun, 2017 by K. Shiomi
  const std::string CalibFileDir = "/home/had/shiomi/work2/beam2017_04/CheckPro0/MakeCalibFile_Run25928/";//modified at 3th July, 2017 by K. Shiomi


  // Particle mass
  const double KL_MASS  = 497.614; // [MeV/c2]
  const double CPi_MASS = 139.57;
  const double Pi0_MASS = 134.9766; // [MeV/c2]
  const double ETA_MASS = 547.853;
  const double NEUTRON_MASS = 939.565379;//[MeV/c2]

  //PDG code (22nd Jun, 2014)
  const Int_t KL_PDGCode = 130; 
  const Int_t gamma_PDGCode = 22; 
  const Int_t neutron_PDGCode = 2112; 
  const Int_t pi0_PDGCode = 111; 
  const Int_t electron_PDGCode = 11;
  const Int_t nue_PDGCode = 12;
  const Int_t muon_PDGCode = 13;
  const Int_t nuu_PDGCode = 14;
  const Int_t pion_PDGCode = 211;
  
  const Int_t nSample125 = 64;
  const Int_t nSample500 = 256;
  const Float_t MCCommonTimeShift = 176.;//[ns]
  const Float_t MCStartZ = -1507;//[mm]
  
  // CC03
  const int CC03NChannels = 32;
  const int CC03NModules = 16;
  const int CC03NDeadChannels = 0;
  const double CC03ZPosition = 6148; // surface
  const double CC03Length = 500;
  const double CC03PropVelo = 88.3; // [mm/ns]
  //const double CC03PropVelo = 86.0; // [mm/ns]
  const double CC03NonUniformity = 1.535E-3; // [/mm] LY=1@z=0, LY=1.768@z=500. CC03 upstream surface is set to 1.
  
  const Double_t CC03WfmTimeShift = 30.287;//ns
  const Double_t CC03WfmSigma0 = 39.66;//ns
  const Double_t CC03WfmAsymPar = 0.1097;
  const Double_t CC03NominalPeakTime = 30.3;//[clock]
  const Double_t CC03MinPeakHeight = 10;//[FADC count]
  const Double_t CC03MinPeakHeight_2017 = 30;//[FADC count] //for 2017
  
  // CC04
  const int CC04NChannels = 64;
  const int CC04NModules = 62;
  const int CC04E391NModules = 42;
  const int CC04KTeVNModules = 16;
  const int CC04ScintiNModules = 4;
  const int CC04NDeadChannels = 4;
  const int CC04DeadChannelID[CC04NDeadChannels] = { 26, 36, 56, 57};
  const double CC04ZPosition = 7415.; // Z front surface of CsI crystal
  const double CC04PSThick = 10.; // Front plastic scintillator thickness
  const double CC04CenterPSZPosition = 7370.; // Front central scintillator front surface position.
  const double CC04UDPSZPosition = 7385.; // Front top and bottom scintillator front surface position.
  const Double_t CC04WfmTimeShift = 30.9;//ns
  const Double_t CC04WfmSigma0 = 41.2;//ns
  const Double_t CC04WfmAsymPar = 0.1449;
  //KTeV crystals have slightly large TimeShift & Sigma0 value but the common value is used
  const Double_t CC04ScintiWfmTimeShift = 29.29;
  const Double_t CC04ScintiWfmSigma0 = 35.78;
  const Double_t CC04ScintiWfmAsymPar = 0.0668;
  const Double_t CC04NominalPeakTime = 28.3;//[clock], common for E391a CsI and scinti 
  const Double_t CC04KTeVNominalPeakTime = 30.5;//[clock]
  const Double_t CC04MinPeakHeight = 10;//[FADC count], 2014, common for CsI and scinti
  const Double_t CC04MinPeakHeight_2017 = 20;//[FADC count], updated at 25th Jun, 2017, common for CsI and scinti 
  //const Double_t CC04ScintiMinPeakHeight = 0.08;//[MeV/clock]

  const Double_t TimeDiffCrystalAndScintiForCC04_CC06=1.41; //[ns]
  
  // CC05
  const int CC05NChannels = 60;
  const int CC05NModules = 58;
  const int CC05E391NModules = 54;
  const int CC05ScintiNModules = 4;
  const double CC05ZPosition = 8793.; // Z front surface of CsI crystal
  const Double_t CC05NominalPeakTime = 26.0;//[clock], common for CsI and scinti 
  const Double_t CC05MinPeakHeight = 20;//[FADC count], common for CsI and scinti
  const Double_t CC05MinPeakHeight_2017 = 17;//[FADC count], updated at 25th June, 2017, common for CsI and scinti 
  //const Double_t CC05ScintiMinPeakHeight = 0.08;//[MeV/clock]
  
  // CC06
  const int CC06NChannels = 60;
  //const int CC06NModules = 60;
  const int CC06NModules = 58;
  const int CC06E391NModules = 54;
  const int CC06ScintiNModules = 4;
  const double CC06ZPosition = 10338.; // Z front surface of CsI crystal  
  const Double_t CC06NominalPeakTime = 26.4;//[clock]
  const Double_t CC06MinPeakHeight = 15;//[FADC count]
  const Double_t CC06MinPeakHeight_2017 = 20;//[FADC count], updated at 25th June, 2017
  const Double_t CC06ScintiMinPeakHeight = 10;//[FADC count]
  
  // CSI
  const int CSINChannels = 2716;
  const double CSIZPosition = 6148; // surface
  const double CSILength = 500;

  // for run62
  //const int CSINGainCorrectChannel = 22;
  //const int CSIGainCorrectChannelID[CSINGainCorrectChannel]={2203,157,2000,1904,2173,
  //2148,248,1327,1568,341,
  //1705,1448,297,2107,179,
  //590,1929,2221,2172,1355,
  //1483,2703};

  // for run62
  //const int CSINDeadChannels = 3;
  //const int CSIDeadChannelID[CSINDeadChannels] = { 1554,2070,2229};

  // for run64
  const int CSINGainCorrectChannel = 10;
  const int CSIGainCorrectChannelID[CSINGainCorrectChannel]={248,297,341,590,1327,
							     1448,1568,1904,1929,2107};  
  const int CSINDeadChannels = 3;
  const int CSIDeadChannelID[CSINDeadChannels] = { 1554,2070,2229};

  // for run69 and run74
  //  const int CSINGainCorrectChannel = 3; //not used??
  //  const int CSIGainCorrectChannelID[CSINGainCorrectChannel]={1242,1904,2000}; //not used??
  const int CSINDeadChannels_2016 = 2;
  const int CSIDeadChannelID_2016[CSINDeadChannels_2016] = {356, 357};

  const double CSILightYield = 9.0; // [p.e./MeV]
  const double CSISPropVelo = 91.0; // [mm/ns] propagation velocity of small crystal
  const double CSILPropVelo = 95.9; // [mm/ns] propagation velocity of small crystal
  // CSI corrections
  const double CSIAbsoluteECoeff = 0.9539; // Al target peak correction.

  const double CSITempSlope = -0.02195; // exponential slope on degree unit.
  
  const double CSITimeDelayCoeff[3] = { 0.067843, -13.25397E-4, 15.91791E-7}; // pol2 coeff. over 400MeV.
  const double CSITimeResolutionCoeff[3] = { 5.00, 3.63318, 0.13}; // Iwai-san's result
  
  //Masuda-san's value (appears in Kyoto meeting slide of 15th Apr, 2013)
  const Double_t CSIWfmTimeShift = 30.6098;//ns
  const Double_t CSIWfmSigma0 = 27.869;//ns
  const Double_t CSIWfmAsymPar = 0.06202;//ns
  const Double_t CSINominalPeakTime = 31.1;//[clock]
  const Double_t CSIMinPeakHeight = 10;//[FADC count]

  // FBAR
  const int FBARNChannels = 32;
  const int FBARNModules = 32;
  const double FBARZPosition  = 0;
  const double FBARLength = 2750.;
  const double FBARAttenuationL = 2700.;
  const double FBARPropVelo = 181.; // [mm/ns]
  const int FBARNDeadChannels = 0;
  const Double_t FBARNominalPeakTime = 30.9;//[clock]
  const Double_t FBARMinPeakHeight = 10;//[FADC count]
  const Double_t FBARMinPeakHeight_2017 = 25;//[FADC count], updated at 25th June, 2017
  

  // CBAR
  const int CBARNChannels = 128;
  const int CBARNModules = 64;
  const double CBARZPosition = 1348 + 7; // upstream end
  const double CBARLength = 5500; // inside
  const double CBARPropVelo = 177. * 0.9499;
  const int CBARNDeadChannels = 0;
  const double CBARInnerInnerR = 1018.; // Inner surface R of Inner module
  const double CBAROuterInnerR = 1118.; // Inner surface R of Outer module

  // Attenuation
  /*  exp( -z / ( Lambda _ alpha * z ) ) */
  const double CBARLAMBDA = 4674. / 0.9499;
  const double CBARALPHA = 0.495;

  const Double_t CBARWfmTimeShift = 29.889;//ns
  const Double_t CBARWfmSigma0 = 37.416;//ns
  const Double_t CBARWfmAsymPar = 0.08396;
  const Double_t CBARNominalPeakTime = 30.75;//[clock]
  const Double_t CBARMinPeakHeight = 10;//[FADC count]
  const Double_t CBARMinPeakHeight_2017 = 50;//[FADC count], updated at 25th June, 2017

  // light yield for CBAR [MeV/p.e.]
  const Double_t CBARLightYieldPerMeV = 10.39;

  // IB
  const int IBNChannels = 64;
  const int IBNModules = 32;
  const Int_t  nModIB = 32;
  const int    IBNDeadChannels = 1;
  const int    IBDeadChannelID[IBNDeadChannels] = { 23 };
  const double IBZPosition = 1348 + 7 + 1639.5; // upstream end
  const double IBLength = 2676; // inside
  const double IBPropVelo = 181.;
  const double IBInnerInnerR = 758.6; // Inner surface R
  const double IBOuterInnerR = 920.; // Outer surface R 

  // Attenuation for IB
  /*  exp( -z / ( Lambda _ alpha * z ) ) */
  const double IBLAMBDA_U = 2886.;
  const double IBALPHA_U = 0.247;
  const double IBLAMBDA_D = 2591.;
  const double IBALPHA_D = 0.353;

  // Waveform parameter for IB
  const Double_t IBWfmPeakHeight_U = 0.143323;
  const Double_t IBWfmSigma0_U     = 2.98882;
  const Double_t IBWfmAsymPar_U    = 0.375717;
  const Double_t IBWfmPeakHeight_D = 0.14977;
  const Double_t IBWfmSigma0_D     = 2.80803;
  const Double_t IBWfmAsymPar_D    = 0.384307;

  // light yield for IB [MeV/p.e.]
  const Double_t IBLightYieldPerMeV_Front = 11.0905;
  const Double_t IBLightYieldPerMeV_Rear  = 10.0605;

  // timing resolution parameter for IB
  const Double_t IBTimeResPar_a = 19.01;
  const Double_t IBTimeResPar_b = 4.534;
  const Double_t IBTimeResPar_c = 0.06978;

  const Int_t IBnSampleIntegral = 25;
  const Int_t IBnSampleIntegralBeforePeak = 6;
  const Float_t IBPeakFindingThreshold = 0.3; // [MeV]

  // Followings are same as MB
  //  const Double_t IBWfmTimeShift = 29.889;//ns
  //  const Double_t IBWfmSigma0 = 37.416;//ns
  //  const Double_t IBWfmAsymPar = 0.08396;
  const Double_t IBNominalPeakTime = 30.75;//[clock]
  const Double_t IBMinPeakHeight = 10;//[FADC count]
  
  // IBCV
  const int IBCVNChannels = 64;
  const int IBCVNModules = 32;
  const int IBCVNDeadChannels = 1;
  const int IBCVDeadChannelIndex[IBCVNDeadChannels] = { 59 };
  const int IBCVDeadChannelID[IBCVNDeadChannels] = { 127 };//e14ID=127 and ChIndex=59 are the same.
  const double IBCVZPosition = IBZPosition;
  const double IBCVLength = IBLength;
  const double IBCVInnerR = 745;
  const double IBCVPropVelo = IBPropVelo;
  const double IBCVLAMBDA_U = IBLAMBDA_U;
  const double IBCVALPHA_U = IBALPHA_U;
  const double IBCVLAMBDA_D = IBLAMBDA_D;
  const double IBCVALPHA_D = IBALPHA_D;

  // Followings are same as BCV
  const Double_t IBCVWfmTimeShift = 29.29;//ns
  const Double_t IBCVWfmSigma0 = 36.64;//ns
  const Double_t IBCVWfmAsymPar = 0.06671;
  const Double_t IBCVNominalPeakTime = 30.25;//[clock]
  const Double_t IBCVMinPeakHeight = 10;//[FADC count]

  // MBCV
  const int MBCVNChannels = 16;
  const int MBCVNModules = 16;

  // Followings are same as BCV
  const Double_t MBCVWfmTimeShift = 29.29;//ns
  const Double_t MBCVWfmSigma0 = 36.64;//ns
  const Double_t MBCVWfmAsymPar = 0.06671;
  const Double_t MBCVNominalPeakTime = 30.25;//[clock]
  const Double_t MBCVMinPeakHeight = 10;//[FADC count]
  const Double_t MBCVMinPeakHeight_2017 = 20;//[FADC count]
  
  // BCV
  const int BCVNChannels = 64;
  const int BCVNModules = 32;
  const double BCVZPosition = CBARZPosition;
  const double BCVLength = CBARLength;
  const double BCVInnerR = 995; 
  const double BCVPropVelo = CBARPropVelo;
  const double BCVLAMBDA = CBARLAMBDA;
  const double BCVALPHA = CBARALPHA;
  const int BCVNDeadChannels = 0;

  const Double_t BCVWfmTimeShift = 29.29;//ns
  const Double_t BCVWfmSigma0 = 36.64;//ns
  const Double_t BCVWfmAsymPar = 0.06671;
  const Double_t BCVNominalPeakTime = 30.25;//[clock]
  const Double_t BCVMinPeakHeight = 10;//[FADC count]
  
  // NCC
  const int NCCNChannels = 204;
  const int NCCNModules = 204;
  const double NCCZPosition = 1990; // upstream surface
  const double NCCLength = 445;
  const double NCCFZPosition = 1990;
  const double NCCFLength = 148;
  const double NCCMZPosition = 2139;
  const double NCCMLength = 200;
  const double NCCRZPosition = 2340;
  const double NCCRLength = 96;

  const Int_t NCCnCommonChannel = 48;
  const Double_t NCCWfmTimeShift = 32.760;//ns
  const Double_t NCCWfmSigma0 = 43.864;//ns
  const Double_t NCCWfmAsymPar = 0.1533;
  const Double_t NCCWfmAsymParSigma = 0.02039;
  //const Double_t HinemosWfmTimeShift = 29.584;//ns
  const Double_t HinemosWfmTimeShift = 30.34;//ns  // change by shiomi 2017/11/30
  const Double_t HinemosWfmSigma0 = 37.032;//ns
  const Double_t HinemosWfmAsymPar = 0.08208;
  const Double_t HinemosWfmAsymParSigma = 0.02503;
  //NCCOuter : temporarily same as CsI by Masuda-san's value (in Kyoto meeting slide of 15th Apr, 2013)
  const Double_t NCCOuterWfmTimeShift = CSIWfmTimeShift;//ns
  const Double_t NCCOuterWfmSigma0 = CSIWfmSigma0;//ns
  const Double_t NCCOuterWfmAsymPar = CSIWfmAsymPar;//ns
  const Double_t NCCNominalPeakTime = 28.3;//[clock]
  const Double_t NCCMinPeakHeight = 50;//[FDAC count] (also for Hinemos)
  const Double_t HinemosNominalPeakTime = 34.5;//[FADC count]
  const Double_t NCCOuterMinPeakHeight = 10;//[FADC count]

  const Double_t TimeDiffCrystalAndScintiForNCC=2.82; //[ns]
  
  // OEV
  const int OEVNChannels = 44;
  const int OEVNDeadChannels = 1;
  const int OEVDeadChannelID[OEVNDeadChannels] = { 41 };
  const double OEVZPosition = 6148.; // Z front surface 
  const Double_t OEVZLength = 420;
  const Double_t OEVLightPropSpeed = 177.;//same with LCV, CV
  const Double_t OEVLightYieldPerMeV = -1;//not implemented yet
  const Double_t OEVOnePhotonSigma = 0;
  const Double_t OEVMinEnergyTimeSmearing = 1;
  const Double_t OEVTimeRes_a = 0;
  const Double_t OEVTimeRes_b = 0;
  const Double_t OEVWfmTimeShift = 17.02;//ns
  const Double_t OEVWfmSigma0 = 23.575;//ns
  const Double_t OEVWfmAsymPar = 0.1787;
  const Double_t OEVWfmAsymParSigma = 0.0301;
  const Double_t OEVNominalPeakTime = 31.5;//[clock]
  const Double_t OEVMinPeakHeight = 50;//[FADC count]  
  
  // CV
  const int CVNChannels = 184;
  const int CVNModules = 92;
  const double CVFZPosition = 5842;
  const double CVFLength = 3;
  const double CVRZPosition = 6093;
  const double CVRLength = 3;
  const double CVSigmaT = 1.28863; // [ns] for module
  const int CVNDeadChannels = 2;
  const int CVDeadChannelID[CVNDeadChannels] = { 115, 172 };
  const int CVDeadModuleID[CVNDeadChannels] = { 57, 86 };
  const Double_t CVWfmTimeShift = 36.024;//ns
  const Double_t CVWfmSigma0 = 48.30;//ns
  const Double_t CVWfmAsymPar = 0.1512;
  const Double_t CVWfmAsymParSigma = 0.01953;
  const Double_t CVLightPropSpeed = 177.;//added at 20th Dec, 2013, from Naito's analysis(in weekly meeting slide at 2nd Sep, 2013)
  const Double_t CVFrontBeamHole = 240;
  const Double_t CVRearBeamHole = 153;
  const Int_t CVFrontnModQuadrant = 12;
  const Int_t CVRearnModQuadrant = 11;
  const Double_t CVFrontMod11EdgeLength = 171.5;
  const Double_t CVFiberLiftLongSide = 25;//[mm], common in both of front and rear plane
  const Double_t CVFrontFiberLiftShortSide[CVFrontnModQuadrant] =
    { 999.9-240/2.-25, 999.9-240/2.-25, 956.4-240/2., 927.8-240/2., 899.1-240/2., 870.5-240/2.,
      821.9-240/2., 752.7-240/2., 683.6-240/2., 614.4-240/2., 481.8-240/2.,
      551.7-CVFrontMod11EdgeLength-240/2.  };//[mm]
  //FCV2-10 : 
  //FCV0, 1 :
  //FCV11   : 
  const Double_t CVNominalPeakTime = 33.5;//[clock]
  const Double_t CVMinPeakHeight = 150;//[FADC count]
  
  // LCV
  const int LCVNChannels = 4;
  const int LCVNModules = 4;
  const double LCVZPosition = 6148.; // Z front surface 
  const Double_t LCVUpstreamZ = 6095.7;
  const Double_t LCVZLength = 575;
  const Double_t LCVLightPropSpeed = 177.;//added at 27th Dec, 2013, from Naito's analysis(in weekly meeting slide at 2nd Sep, 2013)
  const Float_t LCVMinEnergyTimeSmearing = 0.1;//[MeV]
  const Float_t LCVTimeRes_a = 0.1498;//0.1421;//[clock]
  const Float_t LCVTimeRes_b = 0.2207;//0.2316;//[clock*(MeV)^0.5]
  const Float_t LCVLightYieldPerMeV[LCVNModules] = { 34.38, 30.08, 37.69, 31.03 };//[p.e./MeV]
  const Float_t LCVOnePhotonSigma = 0;//[p.e.], not applied
  
  //const Double_t LCVWfmTimeShift = 27.93;//ns
  const Double_t LCVWfmTimeShift = 29.71;//ns
  const Double_t LCVWfmSigma0 = 36.21;//ns
  const Double_t LCVWfmAsymPar = 0.06172;
  const Double_t LCVNominalPeakTime = 28.6;//[clock]
  const Double_t LCVMinPeakHeight = 50;//[FADC count]
  
  //500MHz detectors
  const Int_t NMaxHitPerEvent = 20;
  const Float_t SampleInterval500 = 2.;
  const Float_t TRFInterval = 21.06;//[ns]
  const Float_t PulseMergeTimingWindow = 10;//[ns]
  const Double_t Invalid = -9999;

  // BHCV
  const int BHCVNChannels = 8;
  const int BHCVNModules = 8;
  const double BHCVZPosition = 10757.; // Z front surface 
  
  const Float_t BHCVBHPVTimeDiff = 10.696;//[clock], add(not subtract) this value to BHCVTime
  const Float_t BHCVDeltaZ[BHCVNModules] = { 0, 0, 3., 3., -6., -6., -3., -3. };//[mm]
  const Int_t BHCVSouthModStartID = 4;
  const Float_t BHCVCentralOverlap = 9.;//[mm]
  const Float_t BHCVModSizeX = 117.;//[mm]
  const Float_t BHCVModThickness = 3.;//[mm]
  //const Float_t BHCVAttenLength = 759;//[mm], from measurement with Sr90 by Korean group ; 10% attenuation between both edge (~8cm;<-my feeling)
  const Float_t BHCVAttenLength = -60./TMath::Log(0.9);//[mm]
  //const Float_t BHCVCalibPos = 30.;//[mm], defined from edge ; just feeling
  const Float_t BHCVEdgeCalibFactor = 423./470.8;//[keV/keV]
  const Float_t BHCVCutOffTime = 10;//[ns] ; hits later by this time from the earliest hit will be ignored
  //const Float_t BirksConst = 0.131/1.023;//[mm/MeV]
  const Float_t BirksConst = 0;//[mm/MeV], set to 0
  const Float_t BHCVLightPropSpeed = 299.792458/1.58*(1.+1./1.58)/2.;//[mm/ns]
  const Float_t BHCVMIPEnergy = 0.4708;//[MeV]
  const Float_t BHCVTimeRes_a = 0.061;//[clock]
  const Float_t BHCVTimeRes_b = 0;//[clock*(MeV)^0.5]
  const Float_t BHCVTimeRes_c = 0.031;//[clock*MeV]
  const Float_t BHCVMCTimingCorrection = 0.4839;//[ns]
  //const Float_t BHCVMCTiming = 20.93;//[clock]
  const Float_t BHCVAccDataTiming = (30.439-100);//[clock]
  
  // BHPV
  const int BHPVNChannels = 34;
  const int BHPVNModules = 34;
  const double BHPVZPosition = 12077.; // Z front surface   
  
  const Int_t nModBHPV = 16;
  const Float_t BHPVModZLength = 332;//[mm], added at 4th Nov
  const Float_t BHPVModGapZ[nModBHPV] = { 6, 47, 4, 40, 3, 28, 6, 57, 5, 48, 5, 168,5,5,5,5 };//[mm], added at
  
  //gap btw ith and (i+1)th module
  //last entry is for BHTS (distance btw downstream edge of BHPV module and center of BHTS)
  const Float_t BHPVAerogelCenter = 5.+1.+15.+1.+58/2.;//[mm]
  const Float_t BHPVCoincidenceTimeWindow = 10;//[ns]
  const Float_t BHPV1peSigma = 0.45;//[p.e.]
  const Float_t BHPVTimeRes_a = 0;//[clock]
  const Float_t BHPVTimeRes_b = 0.507;//[clock*(p.e.)^0.5]
  const Float_t BHPVTimeRes_c = 0.138;//[clock*p.e.]
  const Float_t BHPVTimeRes_MinLY = 0.5;//[p.e.], timing resolution in MC does not get worse than the value with this LY
  const Int_t BHPVNMinModCoincidence = 3;
  const Int_t BHPVNMaxCoincidenceStore = 50;
  const Double_t BHPVCoincidenceStoreTimeRange = 75.;//[ns], half width
  //const Double_t BHPVCoincidenceStoreTimeRange = 30.;//[ns]
  //const Double_t BHPVCoincidenceStoreTimeRange = 10000.;//[ns], temporary changed for BHCV/BHPV rate study
  const Int_t BHTSModuleID = 40;
  const Float_t BHPVMCTimingCorrection = 2.8210;//[ns]
  //const Float_t BHPVMCTiming = 24.40;//[clock]
  const Float_t BHPVAccDataTiming = (41.069-100);//[clock]
  
  // TRIGGERTAG
  const int TRIGGERTAGNChannels = 6;
  enum { TRIG_10HzCLOCK=0, TRIG_LASER, TRIG_LED, TRIG_MBCR, TRIG_FBCR, TRIG_CSICR };


  // BPCV
  const int BPCVNChannels = 4;
  const int BPCVNModules = 4;
  //const double BPCVZPosition = 9103; // upstream end
  const double BPCVZPosition = 8793; 
  const double BPCVLength = 1000; // inside
  const double BPCVPropVelo = 177. * 0.9499;
    
  const Double_t BPCVWfmTimeShift = 22.3;//ns
  const Double_t BPCVWfmSigma0 = 36.16;//ns, added at 12th June, 2016
  const Double_t BPCVWfmAsymPar = 0.066;// added at 12th June, 2016
  const Double_t BPCVNominalPeakTime = 26.08;//[clock], added at 12th June, 2016
  const Double_t BPCVMinPeakHeight = 10;//[FADC count]

  // Attenuation
  /* exp(-z/ (Lambda - alpha * z)) */
  const double BPCVLAMBDA = 4674./ 0.9499;
  const double BPCVALPHA = 0.495;

  // Timing Resoulution
  const double BPCVSigmaT=2.02; // ns
  /* This timing resultion is obtained from comic ray run for BPCV.
     During this run, Scintillator counters was put above BPCV top module and below BPCV bottom
     module. And timing resolution was calculated from timing difference distribution between
     top module and bottom module.  (K. Shiomi 2017/11/30) */
  
	
  // DCV
  const int DCVNChannels = 32;
  const int DCVNModules = 32;
  const Double_t DCVWfmSigma0 = 50.1;
  const Double_t DCVWfmAsymPar = 0.29;
  
  // measure before installation. provided by Hongmin
  const Double_t DCV_attenuation = 2469; // in mm
  
  // measured using the time difference of both ends.
  // RMS value divided by sqrt(2) is used.
  // This is obtained using cosmic ray signal.
  // This value is temporary
  const double DCVSigmaT = 0.8; // [ns]  


  // newBHCV
  const int newBHCVNChannels = 48;
  const int newBHCVNModules = 48;
  const Double_t newBHCVZpos=(614.8+242.5+155.5+2.+19.+(7.006+0.029)*3.+1.125+(1+0.14))*10.;
  const Double_t newBHCVmoduleThickness=10.+0.05+2.8+0.05+15.;//tFrontFrame+tKapton+tG10+tKapton+tRearFrame , mm   

  // BHGC
  const int BHGCNChannels = 8;
  const int BHGCNModules = 4;
  const Int_t nModBHGC = 4;  
  const Double_t BHGCZPosition= 18195 + 40;
  const Double_t BHGCModuleInterval= 180.+183.;
  const Float_t BHGC1peSigma = 0.40;//[p.e.]
  const Float_t BHGCTimeSmear = 0.2065;//[clock]
 
  // UCV
  const int UCVNChannels = 12;
  const int UCVNModules = 12;
  const Double_t UCVWfmSigma0 = 2.1;
  const Double_t UCVWfmAsymPar = 0.01;
  
  // UCVLG
  const int UCVLGNChannels = 2;
  const int UCVLGNModules = 2;
  const Double_t UCVLGWfmSigma0 = 2.1;
  const Double_t UCVLGWfmAsymPar = 0.01;


  // Beam line
  const Float_t BeamLineLength = 21507;//[mm]
  const double T1TargetZ = -21507.;
  const double BeamExitZ = -1507.;


  // Al target
  const double ALUZPosition = CSIZPosition - 3360 + 7; // center (z=2795)
  const double ALUThick = 5.;
  const double ALUDiameter = 100.;

  const double ALDZPosition = CSIZPosition - 868.6 - 122; // center (z=5157.4)
  const double ALDThick = 5.;
  const double ALDDiameter = 100.;
  

  // Temperature monitor
  const int nTempMonChannels = 100;
  const std::string TempChannelNames[nTempMonChannels] = { "FB_Chamber", "FB_PMT_25", "FB_PMT_23", "FB_PMT_21", "FB_PMT_19",
							   "FB_PMT_27",  "FB_PMT_29", "FB_PMT_31", "FB_PMT_01", "FB_PMT_03",
							   "FB_PMT_05",  "FB_PMT_28", "FB_PMT_07", "FB_PMT_04", "FB_AIR", 
							   "FB_PMT_17", "Vacuum_Moitor_BeamLine", "Vacuum_Moitor_DecayVolume_LowVac", "Vacuum_Moitor_MB", "Vacuum_Moitor_FB",
							   "Small_PMT_South",   "Small_CW_South",       "Small_PMT_North",   "Small_CW_North",       "Small_CW_Center",
							   "Small_CW_SouthTop", "Small_CW_SouthBottom", "Small_CW_NorthTop", "Small_CW_NorthBottom", "Large_PMT_South",
							   "Large_CW_South",    "Large_PMT_North",      "Large_CW_North",    "CC03_South",           "CC03_North",
							   "OEV_Top",           "OEV_South",            "LCV_1",             "LCV_2",                "LCV_3",
							   "LCV_4",             "Water_IN_1",           "Water_IN_2",        "Small_PMT_Center",     "CV_1",
							   "CV_2",              "CV_3",                 "CV_4",              "Water_OUT_1",          "Water_OUT_2",
							   "BCV_PMT_05_Upstream_3",  "MB_PMT_11_Upstream_4", "MB_PMT_10_Upstream_5", "MB_PMT_71_Downstream_5",  "BCV_PMT_35_Downstream_6",
							   "MB_PMT_70_Downstream_7", "HighVacuum_Monitor_1", "HighVacuum_Monitor_2", "Vacuum_Monitor_Endcap_1", "Vacuum_Monitor_Endcap_2",
							   "CsI_Upstream_1",    "CsI_Upstream_2",    "CsI_Upstream_3",    "CsI_Upstream_4",    "CsI_Upstream_5",
							   "CsI_Upstream_6",    "CsI_Upstream_7",    "CsI_Upstream_8",    "CsI_Upstream_9",    "CsI_Upstream_10",
							   "CsI_Upstream_11",   "CsI_Upstream_12",   "CsI_Upstream_13",   "CsI_Upstream_14",   "CsI_Upstream_15", 
							   "CsI_Upstream_16",   "CsI_Upstream_17",   "CsI_Upstream_18",   "CsI_Upstream_19",   "CsI_Upstream_20",
							   "CsI_Downstream_1",  "CsI_Downstream_2",  "CsI_Downstream_3",  "CsI_Downstream_4",  "CsI_Downstream_5",
							   "CsI_Downstream_6",  "CsI_Downstream_7",  "CsI_Downstream_8",  "CsI_Downstream_9",  "CsI_Downstream_10",
							   "CsI_Downstream_11", "CsI_Downstream_12", "CsI_Downstream_13", "CsI_Downstream_14", "CsI_Downstream_15", 
							   "CsI_Downstream_16", "CC04_1", "CC04_2", "CC04_PMT", "Endcap_2" };

  enum { FB_Chamber=0, FB_PMT_25, FB_PMT_23, FB_PMT_21, FB_PMT_19, FB_PMT_27, FB_PMT_29, FB_PMT_31, FB_PMT_01, FB_PMT_03,
	 FB_PMT_05, FB_PMT_28, FB_PMT_07, FB_PMT_04, FB_AIR, FB_PMT_17, Vacuum_Moitor_BeamLine, Vacuum_Moitor_DecayVolume_LowVac, Vacuum_Moitor_MB, Vacuum_Moitor_FB,
	 Small_PMT_South, Small_CW_South, Small_PMT_North, Small_CW_North, Small_CW_Center, Small_CW_SouthTop, Small_CW_SouthBottom, Small_CW_NorthTop, Small_CW_NorthBottom, Large_PMT_South,
	 Large_CW_South, Large_PMT_North, Large_CW_North, CC03_South, CC03_North, OEV_Top, OEV_South, LCV_1, LCV_2, LCV_3,
	 LCV_4, Water_IN_1, Water_IN_2, Small_PMT_Center, CV_1, CV_2, CV_3, CV_4, Water_OUT_1, Water_OUT_2,
	 BCV_PMT_05_Upstream_3, MB_PMT_11_Upstream_4, MB_PMT_10_Upstream_5, MB_PMT_71_Downstream_5, BCV_PMT_35_Downstream_6, MB_PMT_70_Downstream_7, HighVacuum_Monitor_1, HighVacuum_Monitor_2, Vacuum_Monitor_Endcap_1, Vacuum_Monitor_Endcap_2,
	 CsI_Upstream_1, CsI_Upstream_2, CsI_Upstream_3, CsI_Upstream_4, CsI_Upstream_5, CsI_Upstream_6, CsI_Upstream_7, CsI_Upstream_8, CsI_Upstream_9, CsI_Upstream_10,
	 CsI_Upstream_11, CsI_Upstream_12, CsI_Upstream_13, CsI_Upstream_14, CsI_Upstream_15, CsI_Upstream_16, CsI_Upstream_17, CsI_Upstream_18, CsI_Upstream_19, CsI_Upstream_20,
	 CsI_Downstream_1, CsI_Downstream_2, CsI_Downstream_3, CsI_Downstream_4, CsI_Downstream_5, CsI_Downstream_6, CsI_Downstream_7, CsI_Downstream_8, CsI_Downstream_9, CsI_Downstream_10,
	 CsI_Downstream_11, CsI_Downstream_12, CsI_Downstream_13, CsI_Downstream_14, CsI_Downstream_15, CsI_Downstream_16, CC04_1, CC04_2, CC04_PMT, Endcap_2 };

  // etc
  const double SECPOTRatio = 2.56E9;  // SEC -> POT
  const double POTKLRatio = 2.095E-7; // POT -> KL
  const double OnlineThreshold = 550; // [MeV] for May Et
}


#endif  //__MTBASICPARAMETERS_H__
