#ifndef __MTGAINCORRECTION_H__
#define __MTGAINCORRECTION_H__

#include <iostream>
#include <TROOT.h>
#include <TChain.h>
#include <TTree.h>
#include <map>

#include "MTBasicParameters.h"
#include "MTPositionHandler.h"

class MTGainCorrection{
private:
  TChain* tr;
  int     RunID;
  double* Gain;
  double* BaseGain;
  int     CorrectFlag[2716];
  std::map< unsigned int, unsigned int> runEntryMap;
  void SetBranchAddress( void );
  
public:
  MTGainCorrection();
  ~MTGainCorrection();
  bool   IsExist( int run ) const;
  bool   SetRunID( int run );
  double GetCorrectionFactor( int csiID ) const;
  double Correction( int csiID, double energy );
};
#endif // __MTGAINCORRECTION_H__
